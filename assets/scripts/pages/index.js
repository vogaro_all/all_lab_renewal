import Swiper from 'swiper/dist/js/swiper'

{
  const mvSlider = new Swiper('#js-mv-slider', {
    init: false,
    slidesPerView: 'auto',
    centeredSlides: true,
    spaceBetween: 30,
    loop: true,
    speed: 600,
    pagination: {
      el: '.swiper-pagination'
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    breakpoints: {
      767: {
        spaceBetween: 10
      }
    },
    autoplay: {
      delay: 4000
    }
  })
  mvSlider.init()
}

// mv category
{
  const categorySlider = new Swiper('#js-mv-category', {
    init: false,
    slidesPerView: 'auto',
    resistanceRatio: 0,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  })
  categorySlider.init()
}
