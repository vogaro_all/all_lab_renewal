import 'objectFitPolyfill'
import 'what-input'
import 'intersection-observer'
import picturefill from 'picturefill'
import Scroller from '../modules/_scroller'
import Swiper from 'swiper/dist/js/swiper'

{
  const scroller = new Scroller('[data-scroll]')
  scroller.init()
}

// 375px以下でviewportを固定
{
  const minWidth = 375
  const viewportEl = document.querySelector('meta[name="viewport"]')
  const mediaQueryList = window.matchMedia(`(min-device-width: ${minWidth}px)`)

  function onChange() {
    const viewportContent = mediaQueryList.matches
      ? 'width=device-width, initial-scale=1'
      : `width=${minWidth}`

    viewportEl.setAttribute('content', viewportContent)
  }

  mediaQueryList.addListener(onChange)
  onChange()
}

// picturefill stickyfill
{
  const mql = window.matchMedia('(min-width: 765px)')
  const fill = () => {
    picturefill()
    window.objectFitPolyfill()
  }
  mql.addListener(fill)
  fill()
}

// header active
{
  const path = location.pathname.split('/')[1]
  if (path) {
    const target = document
      .getElementById('js-header-category')
      .querySelector(`[href="/${path}/"]`)

    if (target) target.classList.add('is-active')

    document.body.classList.add(`page-${path}`)
  }
}

// header category
{
  const categorySlider = new Swiper('#js-header-category', {
    init: false,
    slidesPerView: 'auto',
    resistanceRatio: 0,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    }
  })
  categorySlider.init()
}

// side ranking tab
{
  const wrapper = document.getElementById('js-side')
  const triggers = [...wrapper.querySelectorAll('[data-tab-trigger]')]
  const targets = [...wrapper.querySelectorAll('[data-tab]')]

  const onClick = event => {
    const trigger = event.currentTarget
    const target = trigger.dataset.tabTrigger

    triggers.forEach(trigger => {
      trigger.classList.remove('is-active')
    })
    targets.forEach(target => {
      target.classList.remove('is-active')
    })
    trigger.classList.add('is-active')
    wrapper.querySelector(`[data-tab="${target}"]`).classList.add('is-active')
  }

  triggers.forEach(trigger => {
    trigger.addEventListener('click', onClick)
  })
}

{
  const searchButton = document.getElementById('js-header-search')
  searchButton.addEventListener('click', event => {
    event.stopPropagation()

    if (!searchButton.classList.contains('is-active')) {
      searchButton.classList.add('is-active')
    }
  })

  document.addEventListener('click', () => {
    if (searchButton.classList.contains('is-active')) {
      searchButton.classList.remove('is-active')
    }
  })
}

{
  const target = document.getElementById('js-menu-target')
  const trigger = document.getElementById('js-menu-trigger')
  const close = document.getElementById('js-menu-close')

  target.addEventListener('click', event => {
    event.stopPropagation()
  })

  trigger.addEventListener('click', event => {
    event.stopPropagation()
    target.classList.add('is-active')
  })

  close.addEventListener('click', event => {
    target.classList.remove('is-active')
  })

  document.addEventListener('click', () => {
    if (target.classList.contains('is-active')) {
      target.classList.remove('is-active')
    }
  })
}
