import gulp from 'gulp'
import gulpLoadPlugins from 'gulp-load-plugins'
import imageminPngquant from 'imagemin-pngquant'
import imageminWebp from 'imagemin-webp'
import upath from 'upath'

import * as utils from '../utils'
import common from '../../common'
import config from '../../config'

const $ = gulpLoadPlugins()
const isDev = process.env.NODE_ENV === 'development'

export default function images() {
  const spritesFilter = $.filter(
    upath.join(
      config.distDir,
      common.dir.assets,
      common.dir.images,
      common.dir.sprites,
      '**',
      `*${common.ext.sprites}`
    ),
    {
      restore: true
    }
  )

  return gulp
    .src(common.srcPaths.images, {
      base: config.srcDir
    })
    .pipe($.changed(`${config.distDir}/${config.baseDir}`))
    .pipe(
      $.if(
        isDev,
        $.plumber({
          errorHandler: $.notify.onError()
        })
      )
    )
    .pipe(
      $.if(
        !isDev,
        $.imagemin([
          $.imagemin.gifsicle({
            optimizationLevel: 3
          }),
          $.imagemin.jpegtran(),
          $.imagemin.svgo({
            plugins: [
              {
                removeUselessDefs: false
              },
              {
                removeViewBox: false
              },
              {
                cleanupIDs: false
              }
            ]
          }),
          imageminPngquant()
        ])
      )
    )
    .pipe(utils.detectConflict())
    .pipe(gulp.dest(`${config.distDir}/${config.baseDir}`))
    .pipe(spritesFilter)
    .pipe(
      $.svgSprite({
        shape: {
          id: {
            generator(name) {
              const destRelativeName = upath.relative(
                upath.join(
                  common.dir.assets,
                  common.dir.images,
                  common.dir.sprites
                ),
                name
              )
              const directorySeparatedName = destRelativeName
                .split(upath.sep)
                .join(this.separator)

              return upath.basename(
                directorySeparatedName.replace(/\s+/g, this.whitespace),
                common.ext.sprites
              )
            }
          },
          transform: null
        },
        mode: {
          symbol: {
            dest: upath.join(common.dir.assets, common.dir.images),
            sprite: 'sprite.symbol.svg'
          }
        }
      })
    )
    .pipe(utils.detectConflict())
    .pipe(gulp.dest(`${config.distDir}/${config.baseDir}`))
    .pipe(spritesFilter.restore)
    .pipe($.if(config.webp, $.filter('**/*.+(png|jp?(e)g)')))
    .pipe(
      $.if(
        config.webp && !isDev,
        $.imagemin([
          imageminWebp({
            quality: '90',
            method: 6
          })
        ])
      )
    )
    .pipe(
      $.if(
        config.webp,
        $.rename({
          extname: '.webp'
        })
      )
    )
    .pipe($.if(config.webp, utils.detectConflict()))
    .pipe($.if(config.webp, gulp.dest(`${config.distDir}/${config.baseDir}`)))
    .pipe(common.server.stream())
}
