<?php
require_once("../lacne/ownedmedia/output/post.php");
use lacne\service\output\OwnedMediaOtherAPI;
use lacne\service\output\OwnedMediaHelper;
use lacne\core\Pager;
$ownedMediaOtherAPI = new OwnedMediaOtherAPI();

$limit 	= 12;
$params = array(
    'limit' => $limit,
);
$category_name = '';
if (isset($_GET['category']) && !empty($_GET['category']) && is_numeric($_GET['category'])) {
    $params['category'] = $_GET['category'];
    
    global $LACNE;
    $category_list = $LACNE->library['post']->get_category_list();
    $category_name = $category_list[$_GET['category']];
}

$dataList = $ownedMediaOtherAPI->load("loadArticleList", $params);

//ページャ条件設定
$page 	= $_GET['page'];
$length = $dataList['ret']['cnt'];
$pages = (OwnedMediaHelper::pager( new Pager($page, $length, $limit) ));


// side用
$params = array(
    'limit' => 5,
);
$rankingWeeklyDataList = $ownedMediaOtherAPI->load("loadRankingWeekly", $params);
$rankingTotalDataList = $ownedMediaOtherAPI->load("loadRankingTotal", $params);
$keywordList = $ownedMediaOtherAPI->load("loadKeywordList", $params);

include_once('./index_template.html');
?>