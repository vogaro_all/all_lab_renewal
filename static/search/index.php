<?php
require_once("../lacne/ownedmedia/output/post.php");
use lacne\service\output\OwnedMediaOtherAPI;
use lacne\service\output\OwnedMediaHelper;
use lacne\core\Pager;
$ownedMediaOtherAPI = new OwnedMediaOtherAPI();

$tag_list = $ownedMediaOtherAPI->tag_list();

$limit 	= 12;
$params = array(
    'limit' => $limit,
);

$keywords_title = '';
if (isset($_GET['key']) && !empty($_GET['key']) && is_numeric($_GET['key'])) {
    $params['key'] = $_GET['key'];
    $keywords_title .= fn_esc($tag_list[$_GET['key']]);
}

if (isset($_GET['keywords']) && !empty($_GET['keywords'])) {
    $params['keywords'] = $_GET['keywords'];
    $keywords_title .= fn_esc($_GET['keywords']);
}



$dataList = $ownedMediaOtherAPI->load("loadArticleList", $params);

//ページャ条件設定
$page 	= $_GET['page'];
$length = $dataList['ret']['cnt'];

$pages = OwnedMediaHelper::pager( new Pager($page, $length, $limit) ) ;

// side用
$params = array(
    'limit' => 5,
);
$rankingWeeklyDataList = $ownedMediaOtherAPI->load("loadRankingWeekly", $params);
$rankingTotalDataList = $ownedMediaOtherAPI->load("loadRankingTotal", $params);
$keywordList = $ownedMediaOtherAPI->load("loadKeywordList", $params);

include_once('./index_template.html');
?>