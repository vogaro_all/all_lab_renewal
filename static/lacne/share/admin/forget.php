<?php

/**
// ------------------------------------------------------------------------
 * パスワードリマインダ
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../include/setup.php");

// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login'));

//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のforget/以下にあるため）
$LACNE->template->setViewDir($LACNE->template->getViewDir()."/forget");

// ------------------------------------------------------------------------
// ここから送信データチェックの処理
// ------------------------------------------------------------------------
if($LACNE->action("/check")){
    
    $success = 0;
    $err = "";
    
    // 入力判定
    if(!empty($_POST["login_account"]) && !empty($_POST["email"]))
    {
        
        //アカウント存在チェック
        //存在すれば仮パスワードを発行しメール送信する
        $result = $LACNE->library["login"]->passRemind($_POST["login_account"] , $_POST["email"]);
        
        //仮パスワード発行成功
        if($result)
        {
            $success = 1;
        }
        
        //失敗
        $err = "入力された ID もしくは メールアドレスは登録がございません。";
    }
    else
    {
        $err = "ID または メールアドレスが入力されていません";
    }
    
    $render_data = array(
        "success"   => $success, 
        "err"       => $err,
        "data"      => fn_sanitize($_POST)
    );
}


// ------------------------------------------------------------------------
// 初期状態
// ------------------------------------------------------------------------
if($LACNE->action("/"))
{
    $render_data = array();
}


//Render
$LACNE->render("forget" , $render_data);

?>