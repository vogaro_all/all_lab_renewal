<?php

/**
// ------------------------------------------------------------------------
 * ログイン
 * 
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

require_once(dirname(__FILE__)."/../include/setup.php");
require_once(DIR_VENDORS."Services/JSON.php");
// ------------------------------------------------------------------------
// セットアップ
// ------------------------------------------------------------------------
$LACNE->load_library(array('login'));
$LACNE->session->sessionStart(); //Session start

//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のlogin/以下にあるため）
$LACNE->template->setViewDir($LACNE->template->getViewDir()."/login");

// ------------------------------------------------------------------------
// ログイン処理
// ------------------------------------------------------------------------
if($LACNE->post('/login_ajax'))
{
    $err = "";
    // 入力判定
    if(!empty($_POST["login_account"]) && !empty($_POST["password"]))
    {
        $result = $LACNE->library["login"]->checkLogin($_POST["login_account"] , $_POST["password"]);
        
        //認証成功
        if($result)
        {
            $mainpage_url = LACNE_MAINPAGE_URL;
            if (strpos($mainpage_url , "?") === FALSE) {
                $mainpage_url .= "?login=1";
            } else {
                $mainpage_url .= "&login=1";
            }

            $return = array(
                "status" => 1,
                "path" => $mainpage_url
            );
        }
        else
        {
            //失敗
            $err = "ID もしくは パスワードが違います";
            //接続元IPアドレス制限に引っかかったか
            if(!$LACNE->library["login"]->check_allow_ipaddress($_POST["login_account"])) {
                $err = "ID もしくは パスワードが違うか、または、接続元のIPアドレスがログインを許可されていません。";
            }
        }
        
    }
    else
    {
        $err = "ID もしくは パスワードが入力されていません";
    }
    
    if($err)
    {
        $return = array(
            "status" => 0,
            "error" => $err
        );
    }
    
    $json = new Services_JSON();
    echo $json->encode($return);
    exit;
}


// ------------------------------------------------------------------------
// 初期状態
// ------------------------------------------------------------------------
if($LACNE->action("/"))
{
    $render_data = array();
}

//Render
$LACNE->render("login" , $render_data);

?>