var MediaDrop = MediaDrop || {};

MediaDrop.config = {
    'filetype' : ['jpg', 'gif', 'png', 'pdf'],
    'maxfilesize' : 2000, //キロバイト
    'maxfilenum'  : 10
}

MediaDrop.support = function() {
    //return false;
    return window.FileReader ? true : false;
}

MediaDrop.stopPropagation = function(e) {
    e.stopPropagation();
    if (e.preventDefault) {
        return e.preventDefault();
    } else {
        return e.returnValue = false;
    }
}

MediaDrop.previewImage = function(file) {
    var self = this;
    var deferred = $.Deferred();
    var re = new RegExp('(.+).(' + self.config.filetype.join('|') + ')$');
    var filename = file.name.match(re);
    if (!filename) {
        deferred.reject('アップロードできるファイルは'+self.config.filetype.join(', ')+'のみです。');
        return deferred;
    }
    if (file.size > self.config.maxfilesize * 1000) {
        deferred.reject('アップロードできるファイルは'+self.config.maxfilesize+'キロバイトまでです。');
        return deferred;
    }
    var fileReader = new FileReader();
    fileReader.onload = function(event) {
        if ($('#image-preview tbody tr').length >= self.config.maxfilenum){
            deferred.reject('一度にアップロードできるのは'+self.config.maxfilenum+'ファイルまでです。');
        } else {
            var clone = $('#image-preview-tpl').find('tr').clone(true);
            if(filename[2] == 'pdf'){
                clone.find('td.image img').attr('src', LACNE_SHAREDATA_PATH+'/images/common/thumb_pdf.jpg');
            }else if(filename[2] == 'csv'){
                clone.find('td.image img').attr('src', LACNE_SHAREDATA_PATH+'/images/common/thumb_csv.jpg');
            }else{
                clone.find('td.image img').attr('src', fileReader.result);
            }
            clone.find('td.image input[type=hidden]').val(fileReader.result);
            clone.find('td.filename input[type=text]').val(filename[1]);
            clone.find('td.filename input[type=hidden]').val(file.name);
            $('#image-preview tbody').append(clone);
            deferred.resolve();
        }
    }
    fileReader.readAsDataURL(file);

    return deferred;
}

MediaDrop.previewImages = function(files){
    var promises = [];
    for(var i = 0; i < files.length; i++) {
        promises.push(this.previewImage(files[i]));
    }
    $.when.apply($, promises).fail(function(error){
        setTimeout(function(){
            alert(error);
        }, 0);
    });
}

MediaDrop.init = function(config) {

    if (!this.support()){
        $('#drop-upload').remove();
        return false;
    }
    $('#legacy-upload').remove();

    $.extend(this.config, config);
    var self = this;

    //ドロップエリアのイベント
    $('#image-drop').on({
        'dragenter dragover mouseover' : function(){
            $(this).addClass('active');
            return false;
        },
        'dragleave mouseout' : function(){
            $(this).removeClass('active');
        },
        'drop' : function(e){
            self.previewImages(e.originalEvent.dataTransfer.files);
            return false;
        },
        'click' : function(){
            $('#image-select').click();
        }
    });

    //ファイル選択のイベント
    $('#image-select').on({
        "change" : function(){
            self.previewImages($(this).prop('files'));
        }
    });

    $('#image-preview tbody tr').each(function(){
        var filename = $(this).find('td.filename input[type=hidden]').val();
        if(filename.match(/(.*).pdf$/)){
            $(this).find('td.image img').attr('src', LACNE_SHAREDATA_PATH+'/images/common/thumb_pdf.jpg');
        }else if(filename.match(/(.*).csv$/)){
            $(this).find('td.image img').attr('src', LACNE_SHAREDATA_PATH+'/images/common/thumb_csv.jpg');
        }else{
            $(this).find('td.image img').attr('src', $(this).find('td.image input[type=hidden]').val());
        }
    });
}