/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'placeholder', 'ja', {
	title: 'ムービー',
	toolbar: 'ムービー',
	name: '動画ファイル',
	invalidName: 'ファイルの指定を行ってください。',
	pathName: 'placeholder'
});
