<?php

    $page_setting = array(
        "title" => "アカウント管理",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css",LACNE_SHAREDATA_PATH."/css/account/regist.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .account');
    $("input[name=ipaddress]")
    .focus(function(){
        $("#ip-alert-container").fadeTo('fast' , 0.95);
    })
    .blur(function(){
        $("#ip-alert-container").fadeOut('fast');
    });
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "regist");
?>


<section class="section">
<?php
if(!isset($err) || !$err) :
?>
<div class="alert memo pie pc"><span class="icon">情報</span><p class="fl">下記の項目を入力のうえ、確認ボタンを押してください。</p></div>
<?php
else:
?>
<div class="alert error pie"><span class="icon">情報</span><p class="fl"><?=fn_output_errtxt($err)?></p></div>
<?php
endif;
?>
<form action="<?=LACNE_APP_ADMIN_PATH?>/account/register.php?action=check&<?=fn_set_urlparam($_GET , array('id') , false)?>" method="post">
<div class="input">
<p><span class="label">担当者名：</span><input type="text" name="user_name" autocomplete="off" value="<?=(isset($data_list["user_name"]))?$data_list["user_name"]:""?>" /> <span class="req">必須</span></p>
<p><span class="label">ログインID：</span><input type="text" name="login_account" autocomplete="off" value="<?=(isset($data_list["login_account"]))?$data_list["login_account"]:""?>" /> <span class="req">必須</span></p>
<p><span class="label">メールアドレス：</span><input type="text" name="email" autocomplete="off" maxlength="100" value="<?=(isset($data_list["email"]))?$data_list["email"]:""?>" /> <span class="req">必須</span></p>
<p><span class="label">パスワード：</span><input type="password" name="password" autocomplete="off" maxlength="100" value="<?=(isset($data_list["password"]))?$data_list["password"]:""?>" /><?php if(empty($_GET["id"])){ ?> <span class="req">必須</span><?php } ?></p>
<p><span class="label">パスワード確認：</span><input type="password" name="password_chk" autocomplete="off" maxlength="100" value="<?=(isset($data_list["password_chk"]))?$data_list["password_chk"]:""?>" /><?php if(empty($_GET["id"])){ ?> <span class="req">必須</span><?php } ?></p>
<?php
//ipaddressカラムがadminテーブル上にあればIP制限設定可能
if(isset($account_list[1]["ipaddress"])) {
?>
<p><span class="label">IP制限：</span><input type="text" name="ipaddress" value="<?=(isset($data_list["ipaddress"]))?$data_list["ipaddress"]:""?>" /></p>

<div id="ip-alert-container">
<div id="ip-alert">
※IPアドレスによるログイン制限をかける場合、ここにIPアドレスを入力して下さい。<b>入力が空の場合は制限は行われません</b>。<br>
※複数のIPアドレスを登録する場合は<b>カンマ区切り</b>で入力して下さい。<br>
※必ず<b>固定化されているIPアドレス</b>を登録するようにして下さい。<br>
（特にスマートフォンなどからログインされる事がある場合は、LTEや3G通信利用時にIPアドレスが変動しますのでログインできなくなる可能性があります。）
</div>
</div>
<?php
}
?>

<p><span class="label">権限タイプ：</span>
<?php
//id=1の場合は権限変更できない
if((!empty($data_list["id"]) && $data_list["id"] == "1") || (!empty($_GET["id"]) && $_GET["id"] == "1")) :
?>
<?=!empty($authority_list[$data_list["authority"]])?$authority_list[$data_list["authority"]]:""?>
<input type="hidden" name="authority" value="<?=$data_list["authority"]?>">
<?php
else:
?>
<select name="authority">
<?=fn_output_html_select($authority_list,$data_list["authority"])?>
</select>
 <span class="req">必須</span>
<?php
endif;
?>
</p>
<!-- .input // --></div>

<div class="btn">
<p class="btn-type01 pie"><input type="submit" name="conf" value="確認" class="pie" /></p>
<p class="btn-type02 pie"><a href="#"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<br>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>