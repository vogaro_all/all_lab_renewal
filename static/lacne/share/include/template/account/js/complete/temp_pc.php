<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $("#Pc #Side").remove();
    $('#Main').width($('#Content').width());
    $parent = $(window.parent.document);
    $('#Content').css({width:$parent.find('#box-regist iframe').width()});
    $parent.find('#box-regist iframe').css({height:$('#Main .section').height()});
    window.parent.$.library.modalAdjust();
    
    //完了ボタンクリックでリストリロード
    $("#Main .section .btn-type02 a").click(function(){
            window.parent.$.library.modalClose('reload');
            return false;
    });


});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    


});
// -----------------------------------------------
</script>
