<?php

    $page_setting = array(
        "title" => "メイン",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/main.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");    
?>

<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('#MainMenu .main');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "main");
?>


<section class="section pc">
<h1 class="head-line01">メインメニュー</h1>
<p class="load">メニューから目的の操作を選択してください。</p>
<nav class="menu">
<?php
include_once(DIR_CONFIG.'admin_menu.php');
if(!empty($_SETTING_ADMIN_MENU) && is_array($_SETTING_ADMIN_MENU)) :
    foreach($_SETTING_ADMIN_MENU as $id => $menu_data) :
		if($id != "Main") :
?>
<div class="item">
<figure class="group-rover">
<a href="<?=$menu_data["link"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/index/main_menu_img06.gif" alt="<?=$menu_data["label"]?>" class="rover" /></a>
</figure>
<p class="item-name"><a href="<?=$menu_data["link"]?>"><?=$menu_data["label"]?></a></p>
<p><?=$menu_data["comment"]?></p>
<!-- .item // --></div>
<?php
		endif;
	endforeach;
endif;
?>

<!-- .menu // --></nav>
<!-- .section // --></section>

<section class="section smp">
<p class="name smp"><noscript>JavaScriptを有効にしてくだい。</noscript></p>

<div class="alert memo pie mb30"><span class="icon">情報</span><p class="fl">メニューから目的の操作を選択してください。</p></div>

<nav class="menu smp">
<?php
if(!empty($_SETTING_ADMIN_MENU) && is_array($_SETTING_ADMIN_MENU)) :
    $cnt = 1;
	foreach($_SETTING_ADMIN_MENU as $id => $menu_data) :
		if($id != "Main") :
?>
<dl id="mainExtend0<?=$cnt?>">
<dt><a href="<?=$menu_data["link"]?>"><span><?=$menu_data["label"]?></span></a></dt>
</dl>
<?php
		endif;
		
		$cnt++;
	endforeach;
endif;
?>
<!-- #menu // --></nav>
<!-- .section --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>