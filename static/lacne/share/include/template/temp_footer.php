
<?php
//--------------------------------------------------------
//パスワードの変更アナウンスを行う必要があれば以下を出力する
//--------------------------------------------------------
if(!empty($_GET["login"]) && !empty($login_id)) :
	if($LACNE->library["admin_view"]->is_passchange_alert($login_id)) :
?>
<section id="Modal" class="section passchange-alert">
<?=
//パスワード変更通知
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-passchange");
?>
<div class="alert note pie"><span class="icon">ご案内</span><p class="fl">ログインパスワードは定期的に変更されることをおすすめします。</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="<?=LACNE_PATH?>/setting.php"><span class="pie">変更する</span></a></p>
<p class="btn-type02 pie"><a href="javascript:void(0)" class="modal-close"><span class="pie">変更しない</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<!-- #Modal // --></section>
<script>
$(function(){
	$.library.modalOpen($("<a>").attr("href","#box-passchange"),"#Modal");
});
</script>
<?php
	endif;
endif;
?>

<?php
if(isset($login_id) && $login_id)
{
?>
<p class="pagetop pc"><a href="#Top">ページトップ</a></p>
<?php
}
?>
<!-- #Main // --></div>


<?php

//サイドメニュー表示スマフォ用（$hidden_sidemenu=1としておくと表示されない）
if(isset($login_id) && $login_id && (!isset($hidden_sidemenu) || !$hidden_sidemenu) && $LACNE->library["admin_view"]->device_type() == "Smp") :
    require_once(dirname(__FILE__)."/temp_side_type_b.php");
endif;

?>

<!-- #Content  // --></div><!-- .content-outline // --></div>


<!-- 
//////////////////////////////////////////////////////////////////////////////
Footer
//////////////////////////////////////////////////////////////////////////////
--> 
<footer id="GlobalFooter">
<p class="copyright"><small>Copyright &copy; CMMS All rights reserved.</small></p>
<!-- #GlobalFooter // --></footer>

<!-- #Container // --></div>

</body>
</html>