<?php
    
    $page_setting = array(
        "title" => "パスワード再発行",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css" , LACNE_SHAREDATA_PATH."/css/forget.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>

<script type="text/javascript">
$(document).ready(function(){
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "forget");
?>

<section class="section">
    
<h1 class="head-line01 smp">パスワード再発行</h1>
<p class="load smp"></p>
<?php
if(!isset($success) || !$success) :
?>
<?php
    if(!isset($err) || !$err) :
?>
<div class="alert memo pie pc"><span class="icon">情報</span><p class="fl">ご登録時のIDとメールアドレスを入力し『送信』ボタンを押してください。<br />ご登録メールアドレスへパスワードを再発行いたします。</p></div>
<?php
    else :
    //認証エラー
?>
<div class="alert error pie"><span class="icon">情報</span><p class="fl"><?=$err?></p></div>
<?php
    endif;
?>
<form action="<?=LACNE_APP_ADMIN_PATH?>/forget.php?action=check" method="post">
<div class="input">
<p><span class="label">ログインID：</span><input type="text" name="login_account" value="<?php echo (isset($data["login_account"]))?$data["login_account"]:""?>" /></p>
<p><span class="label">メールアドレス：</span><input type="text" name="email" value="<?php echo (isset($data["email"]))?$data["email"]:""?>" /></p>
<!-- .input // --></div>
<div class="btn">
<p class="btn-type01 pie"><input type="submit" name="" value="送信" class="pie" /></p>
<p class="btn-type02 pie"><a href="<?=LACNE_APP_ADMIN_PATH?>/login.php"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
</form>
<?php

else :
// ------------------------------------------------------------------------
// 再発行完了
// ------------------------------------------------------------------------
?>
<h1 class="head-line01 smp">再発行したパスワードをメールでお送りしました。</h1>
<div class="alert memo pie pc"><span class="icon">情報</span><p class="fl">パスワードの再発行が完了しました。<br />ご登録のメールアドレスへパスワードを送信しましたので、ご確認下さい。</p></div>

<div class="btn set_one">
<p class="btn-type02 pie" style="margin:10px auto 0 auto"><a href="#"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<?php
endif;
?>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>