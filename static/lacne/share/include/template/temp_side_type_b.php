<div id="Side">
<?php
if(isset($login_id) && $login_id) :
?>
<p class="name pc"><?=$LACNE->library["admin_view"]->get_login_name($login_id)?>さん</p>
<?php
endif;
?>

<nav id="GlobalNav">
<?php
include_once(DIR_CONFIG.'admin_menu.php');

if(!empty($_SETTING_ADMIN_MENU) && is_array($_SETTING_ADMIN_MENU)) :
    foreach($_SETTING_ADMIN_MENU as $id => $menu_data) :
?>
<?php
if((empty($menu_data["terms"]) || (!empty($menu_data["terms"]) && $LACNE->library["login"]->IsSuccess(false , $menu_data["terms"])))) :
?>
<dl id="<?=$menu_data["menu_id"]?>">
<dt><a href="#"><span><?=$menu_data["label"]?></span></a></dt>
<dd>
<?php
        if(!empty($menu_data["submenu"]) && is_array($menu_data["submenu"])) :
            foreach($menu_data["submenu"] as $submenu_data) :
                if((empty($submenu_data["terms"]) || (!empty($submenu_data["terms"]) && $LACNE->library["login"]->IsSuccess(false , $submenu_data["terms"]))) &&
                  (empty($submenu_data["option"]) || (!empty($submenu_data["option"]) && class_exists($submenu_data["option"])))) :
?>
<p><a class="<?=$submenu_data["icon"]?>" href="<?=$submenu_data["link"]?>"><span><?=$submenu_data["label"]?></span></a></p>
<?php
                endif;
            endforeach;
        endif;
?>
</dd>
</dl>
<?php
	endif;
?>


<?php
    endforeach;
endif;
?>

<!-- #GlobalNav // --></nav>


<!-- #Side // --></div>