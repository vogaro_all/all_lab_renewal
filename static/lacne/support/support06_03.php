<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>記事詳細 エディタ操作について | 記事作成画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support06_03.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-06',{type:'text'});
	$.library.active('sn-06-03',{type:'text'});
	$('#Main li:odd').addClass('odd');
	$('#Main li:last').addClass('last');
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support06_03/page_ttl.gif" width="660" height="52" alt="記事詳細 エディタ操作について" /></h2>

<div class="grayblock"><div class="grayblock-outline"><div class="grayblock-inline">
<p class="M-align-center M-mb25"><img src="images/support06_03/capture_img.jpg" width="586" height="91" alt="" /></p>

<div class="in-list"><div class="in-list-inline">
<ol>
<li class="num01 first"><strong><img src="images/support06_03/ico_01.gif" width="62" height="25" alt="1" />・・・</strong>
  <div>HTMLソースを直接編集するモードに切り替えます。</div></li>
<li class="num02"><strong><img src="images/support06_03/ico_02.gif" width="97" height="25" alt="2" />・・・</strong>
  <div>文字の装飾を変更します（太字、斜体、下線、取り消し線）</div></li>
<li class="num03"><strong><img src="images/support06_03/ico_03.gif" width="25" height="24" alt="3" />・・・</strong>
  <div>文字の装飾をクリアします。</div></li>
<li class="num04"><strong><img src="images/support06_03/ico_04.gif" width="49" height="24" alt="4" />・・・</strong>
  <div>段落番号または箇条書きを設定します。</div></li>
<li class="num05"><strong><img src="images/support06_03/ico_05.gif" width="97" height="24" alt="5" />・・・</strong>
  <div>文字寄せ（右寄せ、中央寄せ、左寄せ）を行います。</div></li>
<li class="num06"><strong><img src="images/support06_03/ico_06.gif" width="51" height="24" alt="6" />・・・</strong>
  <div>リンクの設定または解除を行います。</div></li>
<li class="num07"><strong><img src="images/support06_03/ico_07.gif" width="25" height="24" alt="7" />・・・</strong>
  <div>画像の挿入を行います。（ <a href="support06_04.html">詳しくはこちら</a> ）</div></li>
<li class="num08"><strong><img src="images/support06_03/ico_08.gif" width="25" height="24" alt="8" />・・・</strong>
  <div>動画ファイルの挿入を行います<p>（ <span class="heighlight">※</span> 動画ファイルオプションを導入されている場合のみ利用可能 ）</p></div></li>
<li class="num09"><strong><img src="images/support06_03/ico_09.gif" width="24" height="24" alt="9" />・・・</strong><div>ブロック（DIV）領域を作成します。</div></li>
<li class="num10"><strong><img src="images/support06_03/ico_10.gif" width="26" height="24" alt="10" />・・・</strong>
  <div>ブロックを可視化します。</div></li>
<li class="num11"><strong><img src="images/support06_03/ico_11.gif" width="89" height="25" alt="11" />・・・</strong>
  <div>あらかじめ設定しておいたクラスによるスタイル指定を行います。</div></li>
<li class="num12"><strong><img src="images/support06_03/ico_12.gif" width="88" height="25" alt="12" />・・・</strong>
  <div>見出しや段落を設定します。</div></li>
  
<li class="num13"><strong><img src="images/support06_03/ico_13.gif" width="74" height="25" alt="13" />・・・</strong>
  <div>文字サイズを変更します。</div></li>
<li class="num14"><strong><img src="images/support06_03/ico_14.gif" width="34" height="25" alt="14" />・・・</strong>
  <div>文字色を変更します。</div></li>
  
  
</ol>
<!-- .in-list-inline // --></div><!-- .in-list // --></div>

<!-- .grayblock-inline // --></div><!-- .grayblock-outline // --></div><!-- .grayblock // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
