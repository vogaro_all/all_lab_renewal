<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>メディアアップロード画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support08.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-08',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support08/page_ttl.gif" width="660" height="52" alt="カテゴリ管理画面" /></h2>
<p class="lead M-pb00">記事内に挿入する画像ファイルのアップロードを行います。<br />
また、すでにアップロードを行ったファイルの確認や、削除を行うことができます。</p>
<p class="lead att M-size-txt"><span class="heighlight">※</span> 動画ファイルオプションを導入されている場合のみ、動画ファイルのアップロードが有効になります。</p>

<div class="capture">
<p class="M-align-center"><img src="images/support08/capture_img.jpg" width="600" height="750" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support08/capture_txt_01.jpg" width="366" height="114" alt="ファイルを選択して、「アップロード」ボタンをクリックすると、ファイルがアップロードされます。※ アップロードするファイルのファイル名は、半角英数字とハイフン、アンダーバーのみ有効です。" /></li>
<li class="popup02"><img src="images/support08/capture_txt_02.jpg" width="403" height="134" alt="アップロード済みのファイル一覧が表示されます。不要なファイルは削除にチェックを入れ、画面一番下の「選択画像を削除」をクリックして下さい。※ 作成された記事内に挿入されている画像ファイルを、この一覧画面上で削除されると、記事内の画像ファイルはリンク切れ状態となります。ご注意下さい。" /></li>
</ul>
<!-- .capture // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
