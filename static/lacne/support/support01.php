<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>管理画面へのログイン | LACNE CMSサポートガイド</title>


<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support01.css" media="all" />
<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-01',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<a id="Top" name="Top"></a>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support01/page_ttl.gif" width="660" height="52" alt="管理画面へのログイン" /></h2>
<p class="lead">発行されたIDとパスワードを入力し、「ログイン」ボタンをクリックして下さい。</p>

<div class="capture">
<p class="M-align-center"><img src="images/support01/capture_img.jpg" width="560" height="389" alt="" /></p>
<ul>
<li class="popup01"><img src="images/support01/capture_txt_01.jpg" width="75" height="49" alt="クリック！" /></li>
</ul>
<!-- .capture // --></div>
<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
