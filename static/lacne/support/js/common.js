/* 
 *
 * library.js v1.0
 * 
 * jQueryとEasingとhashchangeとCookieとMouseWheelが必要です。
 * 
 * Copyright(c) INVOGUE.CO,. Ltd. ALL Rights Reserved.
 * http://www.invogue.co.jp/
 * 
 */


	var common ={
	 init:function(){
		 $.library.pagetop('#PageTop',{positionbottom:28});
		 this.sidenav();
	 },//init
	 sidenav:function(){
		 var target = $('#Side dl');
			target.find('dt:first').addClass('first');
			target.find('dt:last').addClass('last');
			target.find('dt').hover(function(){
				if($(this).hasClass('first')){
					$(this).addClass('first-hover');
				}else if($(this).hasClass('last')){
					$(this).addClass('last-hover');
					target.addClass('hov');
				}else{
					$(this).addClass('hover');
				};
			},function(){
				if($(this).hasClass('first')){
					$(this).removeClass('first-hover');
				}else if($(this).hasClass('last')){
					$(this).removeClass('last-hover');
					if(!$(this).hasClass('active')){
						target.removeClass('hov');
					}
				}else{
					$(this).removeClass('hover');
				};
			});
			target.find('dd li').hover(function(){
				$(this).addClass('hover');
			},function(){
				$(this).removeClass('hover');
			});
			
			if(target.find('.last').hasClass('active')){
				target.addClass('hov');
			}
		
			target.find('dd').hide();
			target.find('dd').each(function(){
				if($(this).find('li').hasClass('active')){
					$(this).show();
				}
			});
			$('.active').next('dd:first').show().addClass('show');
			$('li.active').parent().parent().prev('dt').addClass('show');
			
		} //sidennavi
		
	};//common
