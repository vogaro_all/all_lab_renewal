<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>アカウントの権限タイプについて | アカウント画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support09_02.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-09',{type:'text'});
	$.library.active('sn-09-02',{type:'text'});
	$('#Main li:odd').addClass('odd');
	$('#Main li:last').addClass('last');
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2><img src="images/support09_02/page_ttl.gif" width="660" height="52" alt="アカウントの権限タイプについて" /></h2>
<p class="lead M-pb30">権限タイプには下記の3つがあります。</p>

<div class="list"><div class="list-inline">
<ul>
<li class="first"><strong>マスター・・・</strong><div>記事の公開権限を持ち、管理者アカウントやソーシャルアカウント（Twitter、facebook投稿機能オプションを導入されている場合）の管理など、すべての操作権限を持ちます。</div></li>
<li><strong>管理者・・・</strong><div>記事の公開権限を持っていますが、管理者アカウントやソーシャルアカウント（Twitter、facebook投稿機能オプションを導⼊されている場合）の操作を行うことはできません。</div></li>
<li><strong>投稿者・・・</strong><div>記事の公開権限を持っていません。そのため、記事を作成した場合、その記事は承認待ちとなり、公開するためには、マスター権限もしくは管理者権限を持つアカウントによる承認が必要となります。</div></li>
</ul>
<!-- .in-list-inline // --></div><!-- .in-list // --></div>

<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
