<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<meta http-equiv="content-style-type" content="text/css" />
<meta http-equiv="content-script-type" content="text/javascript" />

<title>採用カテゴリの選択、外部リンク先指定、PDFファイルのアップロード | 記事作成画面 | LACNE CMSサポートガイド</title>

<script type="text/javascript" src="js/jquery1.7_pack.js"></script>
<script type="text/javascript" src="js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="js/library.js"></script>
<script type="text/javascript" src="js/common.js"></script>

<link rel="stylesheet" type="text/css" href="css/global/import.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/global/print.css" media="print" />

<link rel="stylesheet" type="text/css" href="css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="css/support06_04.css" media="all" />

<script type="text/javascript">
$(document).ready(function(){
	$.library.active('sn-06',{type:'text'});
	$.library.active('sn-06-05',{type:'text'});
	common.init();
});
</script>
</head>

<body>
<div id="Container">
<p><a id="Top" name="Top"></a>
</p>
<div class="nonvisual-menu">
<dl><dt>ページ内を移動するためのリンクです。</dt><dd><ul><li><a href="#main-Contents">メインコンテンツへ移動</a></li></ul></dd></dl>
<!-- .nonvisual-menu // --></div>

<?php
include_once("./template/header.inc");
?>


<div id="Main">
<h2 class="bg_title">採用カテゴリの選択(ニュース管理の場合)</h2>
<p class="lead">ニュース記事の管理の場合、採用ページに掲載する採用関連の記事であれば、この「採用カテゴリ」プルダウンで、「新卒採用情報」か「キャリア採用情報」かを選択する必要があります。</p>
<div>
<p class="M-align-center"><img src="images/support06_05/givery01.jpg" alt="" style="padding:3px;border:3px solid #EEE" /></p>
</div>

<h2 class="bg_title M-mt30">外部リンク先指定</h2>
<p class="lead">外部リンク先のURLを登録すると、記事内にサイトURLボタンが表示され、登録したURLにリンクされます。</p>
<div>
<p class="M-align-center"><img src="images/support06_05/givery02.jpg" alt="" style="padding:3px;border:3px solid #EEE" /></p>
</div>

<h2 class="bg_title M-mt30">PDFファイルのアップロード</h2>
<p class="lead">PDFファイルを選択すると、記事登録時にPDFファイルがアップロードされ、記事内にPDFファイルがリンクされます。</p>
<div>
<p class="M-align-center"><img src="images/support06_05/givery03.jpg" alt="" style="padding:3px;border:3px solid #EEE" /></p>
</div><br />
<p class="lead">すでにPDFファイルを登録した記事を編集モードで開いた場合、下図のように、アップされているPDFのファイル名（PDFファイルはアップロード時にリネームされます）が表示され、クリックすると、そのPDFファイルが開きます。また、「削除」ボタンをクリックすると、PDFファイルが削除されます。</p>
<div>
<p class="M-align-center"><img src="images/support06_05/givery03_2.jpg" alt="" style="padding:3px;border:3px solid #EEE" /></p>
<br />
<p class="lead">すでにPDFファイルを登録した記事を編集モードで開いた状態で、さらにファイル選択で別のPDFファイルを選択して、記事を登録した場合、以前のPDFファイルは削除され、新しく選択したPDFファイルが記事にリンクされるようになります。</p>
</div>
<!-- #Main // --></div>

<?php
include_once("./template/side.inc");
?>


<!-- .content-inline // --></div>

<?php
include_once("./template/footer.inc");
?>


<!-- #Container // --></div>
</body>
</html>
