<?php

/**
 *-------------------------------------------------------------------------
 *  オプション： モバイル（スマフォ、ガラケー表示用）
 *  mobile_output.php
 *-------------------------------------------------------------------------
 */

//LACNE OPTION SETTING
define("LACNE_OPTION_MOBILE_OUTPUT" , 1);

//フューチャーフォン向けHTML変換処理
if(file_exists(DIR_VENDORS."mobileHtmlConvert.php"))
{
    require_once(DIR_VENDORS."mobileHtmlConvert.php");
}

class Lib_Mobile_output extends Lib_output 
{
    
    var $imgsize = array(
        //スマフォ用イメージサイズ
        "smph" => array(
            "width" => 270
        ),
        //ガラケー用イメージサイズ
        "mobi" => array(
            "width" => 180
        )
    );
    
    function Lib_Mobile_output($LACNE , $db){
        parent::Lib_output($LACNE , $db);
    }
    
    /*
     * デバイス判別
     * @param void
     * @return mixed ("smph" or "mobi" or false)
    */
    function is_mobile () {

        $smphon_useragents = array(
            'iPhone',         // Apple iPhone
            'iPod',           // Apple iPod touch
            'Android',        // 1.5+ Android
            'dream',          // Pre 1.5 Android
            'CUPCAKE',        // 1.5+ Android
            'blackberry9500', // Storm
            'blackberry9530', // Storm
            'blackberry9520', // Storm v2
            'blackberry9550', // Storm v2
            'blackberry9800', // Torch
            'webOS',          // Palm Pre Experimental
            'incognito',      // Other iPhone browser
            'webmate'         // Other iPhone browser
        );
        $mobile_useragents = array(
            'DoCoMo',
            'UP\.Brower',
            'KDDI-',
            'J-PHONE',
            'Vodafone',
            'SoftBank',
            'emobile',
            'WILLCOM',
            'DDIPOCKET'
        );
        $pattern1 = '/'.implode('|', $smphon_useragents).'/i';
        $pattern2 = '/'.implode('|', $mobile_useragents).'/i';
        if(preg_match($pattern1, $_SERVER['HTTP_USER_AGENT']))
        {
            return "smph";
        }
        else if(preg_match($pattern2, $_SERVER['HTTP_USER_AGENT']))
        {
            return "mobi";
        }

        return "";
    }

    /*
     * 渡されたHTMLタグの中からimgタグを抽出し、指定されたデバイス用に画像をリサイズして
     * 差し替える処理
     * 
     */
    function changedeviceImg($device , $html0)
    {
        $html = fn_output_html_txtarea($html0);
        $html = "<tag>".$html."</tag>";
        
        if(SimpleTag::setof($tag , $html , "tag"))
        {
            $img_tags = $tag->f("in(img)");
            
            if(is_array($img_tags) && count($img_tags))
            {
                for($i=0;$i<count($img_tags);$i++)
                {
                    $src = $tag->f("img[{$i}].param(src)");
                    //画像サイズをデバイス最適化させる
                    $_src = $this->_img_resize($device, $src); 
                    $tag->f("img[{$i}].saveParam(src)" , $_src);
                    $tag->f("img[{$i}].saveParam(style)" , "");
                    $tag->f("img[{$i}].saveParam(width)" , "");
                    $tag->f("img[{$i}].saveParam(height)" , "");
                }
                $return = $tag->get();
                $return = str_replace('<tag>','',$return);
                $return = str_replace('</tag>','',$return);
                return $return;
            }
        }

        return $html0;
        
    }
    
    /*
     * 画像をデバイスに合わせてリサイズする処理
     * (すでにリサイズした同じ画像が存在すればそれを利用する）
     * @param string $device (smph" or "mobi")
     * @param int $resize_w //リサイズ幅サイズ
     * @param int $resize_h //リサイズ縦サイズ
     * @return mixed
     */
    function _img_resize($device , $imgsrc , $resize_w = "" , $resize_h = "")
    {
		$resize_width = !empty($resize_w) ? $resize_w : (!empty($this->imgsize[$device]["width"])?$this->imgsize[$device]["width"] : 0);
		$resize_height = !empty($resize_h) ? $resize_h : (!empty($this->imgsize[$device]["height"])?$this->imgsize[$device]["height"] : 0);
		
        if($resize_width || $resize_height)
        {
            $img_path = $this->LACNE->pathChange_dir_root($imgsrc);
            
            if(file_exists($img_path))
            {

                $resizeImg = new Imgresize($img_path);

				//指定したサイズより大きければリサイズする
				if(($resize_width > 0 && $resizeImg->image_width > $resize_width) || ($resize_height > 0 && $resizeImg->image_height > $resize_height))
				{
					$resize_name = $resizeImg->name()."_".$device;
					
					if(!file_exists($resizeImg->dir."/".$resize_name.".".$resizeImg->ext))
					{
						$resizeImg->name($resize_name);
						if($resize_width)
						{
							$resizeImg->width($resize_width);
						}
						if($resize_height)
						{
							$resizeImg->height($resize_height);
						}
						$resizeImg->save();

					}
					
					return $this->LACNE->get_path_docroot($resizeImg->dir)."/".$resize_name.".".$resizeImg->ext;
				}
				else
				{
					return $imgsrc;
				}
            }
        }
        return $imgsrc;
    }
    
    /**
     * フューチャーフォン向けHTML変換処理
     * Todo 処理が重くないか？
     * @param string $html 
     */
    function mobileHtmlConvert($html , $char_set=STRINGCODE_OUTPUT)
    {
        $mhConvert = new mobileHtmlConvert();
        $html = $mhConvert->singleton($html , "au");
		return mb_convert_kana($html , "ka", $char_set);
    }
    
}



?>