<?php

/**
 *-------------------------------------------------------------------------
 *  オプション： 管理画面スマートフォン対応
 *  Lib_admin_view_smph.php
 * 
 *-------------------------------------------------------------------------
 */

//LACNE OPTION SETTING
define("LACNE_OPTION_ADMIN_SMPH" , 1);

class Lib_admin_view_smph extends Lib_admin_view
{
    //各一覧表示画面の表示数
    //スマフォ対応用はoption側のファイルに
    var $list_num_per_page = array(
        "index_index"   => array(
            "pc"    => 10,
            "smph"  => 10
        ),
        "article_index" => array(
            "pc"    => 10,
            "smph"  => 10
        ),
        "media_index"   => array(
            "pc"    => 10,
            "smph"  => 10
        ),
        "media_list"    => array(
            "pc"    => 20,
            "smph"  => 10
        )
    );
    
    
    function Lib_admin_view_smph($LACNE , $db){
        parent::Lib_admin_view($LACNE , $db);
    }
    
    /*
     * デバイス判別（スマートフォンかどうか）
     * @param void
     * @return boolean
    */
    function is_smph () {

        $smphon_useragents = array(
            'iPhone',         // Apple iPhone
            'iPod',           // Apple iPod touch
            'Android',        // 1.5+ Android
            'iPad',          // Apple iPand
        );
        $pattern1 = '/'.implode('|', $smphon_useragents).'/i';
        if(preg_match($pattern1, $_SERVER['HTTP_USER_AGENT']))
        {
            return true;
        }
        
        return false;
    }
    function device_type() {
        if($this->is_smph()) return "Smp";
        return "Pc";
    }
    function is_Android() {
        if(preg_match('/Android/i', $_SERVER['HTTP_USER_AGENT']))
        {
            return true;
        }
        return false;
    }
    function AndroidEditorError()
    {
        //Android端末でWYSIWYGエディタを利用できないエラー文を出す
        return "Android端末はエディタの機能をご利用いただけません。";
    }
    
    /**
     * 一覧ページの1Pあたりの表示件数
     * @param string $page 
     * @return number
     */
    function get_num_per_page($pagename , $device = "")
    {
        if(!$device && $this->is_smph()) 
        {
            $device = "smph";
        }
        else
        {
            $device = "pc";
        }
        if(isset($this->list_num_per_page[$pagename][$device]))
        {
            return $this->list_num_per_page[$pagename][$device];
        }
        
        return NUM_LIST;
    }
    /**
     * 各デバイス（pc or smp）用に作成されたjsファイルをロード
     * pc : temp_pc.php  smp : temp_smp.php としてファイル設置
     * Templateのrenderを利用しているため、変数を渡しそれを埋め込むことも可能
     *  
     * @param string $path
     * @param string $pagename
     * @param object $vars
     * @return string 
     */
    function load_js_opt_device($path , $pagename , $vars = array())
    {
        $device = "pc";
        if($this->is_smph()) $device = "smp";
        
        $view_dir = $this->LACNE->template->getViewDir();
        $this->LACNE->template->setViewDir($path."/".$pagename);
        $vars = array_merge($vars , array("device"=>$device));
        $js_code = "";    
        if($device == "smp")
        {
            $js_code .= $this->LACNE->render($device , $vars , true , false); //※第４引数をfalseに エスケープをしない
        }
        
        $js_code .= $this->LACNE->render("pc" , $vars , true , false); //temp_pc.phpはPC用jsコードのみでなく共有コードも含むため必ずロードさせる　※第４引数をfalseに エスケープをしない

        $this->LACNE->template->setViewDir($view_dir);
        return $js_code;
    }
}

?>