<?php
use lacne\core\model\Admin;
use lacne\core\model\Post;
//LACNE OPTION SETTING
define("LACNE_OPTION_ACCOUNT" , 1);

/**
// ------------------------------------------------------------------------
 *  account.php
 *  (管理画面）アカウント用
 * @package		LACNE
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 * @copyright	        Copyright (c) 2008 - 2011, In Vogue Inc.
 */
// ------------------------------------------------------------------------

class Lib_account extends Lib_login
{

        /** @var object  $authority  権限設定 */
        var $authority = array(

            "MASTER" => array(
                "name" => "マスター",
                "level" => 1
            ),
            "ADMIN" => array(
                "name" => "管理者",
                "level" => 2
            ),
            "EDITOR" => array(
                "name" => "編集者",
                "level" => 3
            ),
        );

        /**
         * @var object $controll_setting 権限別に許可される操作設定
         * ・manage_account アカウント管理画面での操作,
         * ・manage_setting 設定画面での操作
         * ・manage_category カテゴリ管理画面での操作
         * ・edit_post 記事作成・編集
         * ・publish_post 記事公開
         * ・upload_files ファイルのアップロード
         * ・delete_files ファイルの削除
         *
         */
        var $controll_setting = array(
            "1" => array(
            	"manage"         => 1,
                "manage_account" => 1,
                "manage_setting" => 1,
            	"manage_tag"     => 1,
                "publish_post"   => 1,
                "delete_post"    => 1,
                "upload_files"   => 1,
                "delete_files"   => 1,
            	"manage_ranking"  => 1,
            	"manage_topic" => 1,
            	"manage_banner"  => 1,
            	"manage_effect"  => 1,
                "manage_mv"      => 1,
                "manage_total_ranking"    => 1,
                "manage_keyword"    => 1
            ),

            "2" => array(
            	"manage"         => 0,
            	"manage_account" => 0,
                "manage_setting" => 0,
            	"manage_tag"     => 1,
                "publish_post"   => 1,
                "delete_post"    => 1,
                "upload_files"   => 1,
                "delete_files"   => 1,
            	"manage_ranking"  => 1,
            	"manage_topic" => 1,
            	"manage_banner"  => 1,
            	"manage_effect"  => 1,
                "manage_mv"      => 1,
                "manage_total_ranking"    => 1,
                "manage_keyword"    => 1
            ),

            "3" => array(
            	"manage"         => 0,
                "manage_account" => 0,
                "manage_setting" => 0,
            	"manage_tag"     => 0,
                "publish_post"   => 0,
                "delete_post"    => 0,
                "upload_files"   => 1,
                "delete_files"   => 1,
            	"manage_ranking"  => 0,
            	"manage_topic" => 0,
            	"manage_banner"  => 0,
            	"effect_owned"   => 0,
            	"manage_effect"  => 0,
                "manage_mv"      => 0,
                "manage_total_ranking"    => 0,
                "manage_keyword"    => 0

            ),
        );

        var $app_sendmail_flag = true; //承認待ち、差戻し、承認・公開のタイミングでメール送信するか

        //承認待ち、差戻し、承認・公開時のメール送信設定
        //それぞれarrayを空にすると送信されなくなる
        var $app_sendmail_setting = array(
            "add_wait"     => array("MASTER" , "ADMIN"),
            "wait_publish" => array("EDITOR"),//実際はEDITORの中の記事作成担当者のみに送信
            "wait_back"    => array("EDITOR") //実際はEDITORの中の記事作成担当者のみに送信
        );

	/**
	 *  コンストラクタ
	 *
	 *  @param  $db DBオブジェクト
	 *  @return void
	 */
	function Lib_account($LACNE , $db){
            parent::Lib_login($LACNE , $db);
	}

	/**
	 *  アカウントリストを取得
	 *
	 *  @param  number $num //取得件数
	 *  @return object
	 */
        function get_AccountList($num=0) {

                $data_list = with(new Admin())->fetchAll($num);

                $account_list = array();
                if($data_list && count($data_list))
                {
                    foreach($data_list as $data)
                    {
                        $account_list[$data["id"]] = $data;
                    }
                }

                return $account_list;
	}


        /**
         * 権限設定データを返す（プルダウン選択用にデータを加工して）
         *
         * @return object
         */
        function get_AuthorityData()
        {
            $return_obj = array();
            foreach($this->authority as $key => $val)
            {
                $return_obj[$key] = $val["name"];
            }

            return $return_obj;
        }

        function get_account_name($login_id)
        {
            $data = with(new Admin())->fetchOneByLoginID($login_id);

            if($data && isset($data["user_name"]))
            {
                return $data["user_name"];
            }

            return "不明";
        }

        /**
         * アカウントKEY名を渡してそのアカウントの権限レベルを取得する
         * @return number
         */
        function get_AccountLevel()
        {

            return $this->get_authority("level");

        }

        /**
         * 操作権限があるかをチェック
         * @param string $action
         * @param string $login_id //指定しなければSESSIONから取得
         * @return boolean
         */
        function chk_controll_limit($action , $login_id = "")
        {

            $authority_str = "";
            if($login_id)
            {
                $rs = with(new Admin())->fetchOneByLoginID($login_id);
                if($rs && isset($rs["authority"]))
                {
                    $authority_str = $rs["authority"];
                }
            }

            $level = $this->get_authority("level" , $authority_str);

            if($level)
            {
                if(isset($this->controll_setting[$level][$action]))
                {
                    return $this->controll_setting[$level][$action];
                }
            }

            return 0;
        }

        /**
         * 対象の記事データは自身が作成した記事かどうか
         * @param number $post_id
         * @param string $login_id //指定しなければSESSIONから取得
         */
        function chk_my_edit_post($post_id , $login_id = "")
        {
            if(!$login_id && isset($_SESSION["login_id"])) $login_id = $_SESSION["login_id"];

            $data = with(new Post())->fetchOne($post_id);

            if($data && isset($data["user_id"]) && $data["user_id"] == $login_id)
            {
                return true;
            }

            return false;

        }

        /**
	 *  認証成功の判定（ユーザーの権限をチェックし、操作不可ならリダイレクト）
	 *  @param boolean $redirect (認証エラーの場合そのままリダイレクトするか)
	 *  @param string $controller_name
	 *  @return boolean
	 */
	function IsSuccess($redirect = true , $controller_name = "") {

            if(isset($_SESSION["cert"]) && isset($_SESSION["login_id"]))
            {
		//CERT_STRING,login_id,IPの照合
		if($_SESSION["cert"] == CERT_STRING && $_SESSION["login_id"] && $_SESSION["ip"] == fn_getIP()) {

                    //権限チェック
                    if($controller_name && !$this->chk_controll_limit($controller_name))
                    {
                        if($redirect)
                        {
                            $this->err_authority(); //権限なし main pageにリダイレクト
                        }

                        return false;
                    }

                    return $_SESSION["login_id"];

		}
            }
            if($redirect)
            {
                fn_redirect(LINK_LOGIN_ERR01);
            }
            else
            {
                return false;
            }
	}

        /**
         * 権限エラーでリダイレクト処理
         */
        function err_authority(){ fn_redirect(LACNE_APP_ADMIN_PATH."/index.php"); }

        /**
         * 権限名、もしくは権限レベル値を取得
         * @return string $key (name or level)
         * @param string $$authority_str
         */
        function get_authority($key = "" , $authority_str = "")
        {

            if(!$authority_str && isset($_SESSION["authority"]))
            {
                $authority_str = $_SESSION["authority"];
            }

            if($authority_str && isset($this->authority[$authority_str]))
            {
                if(isset($this->authority[$authority_str][$key]))
                {
                    return $this->authority[$authority_str][$key];
                }
                else
                {
                   return $_SESSION["authority"];
                }
            }

            return "";
        }

        /**
         * 承認待ち、差戻し、承認・公開のそれぞれのタイミングでメール送信する対象者を取得
         * @param string $action
         * @return array
         */
        function get_sendmail_target($action)
        {

            $target = array();
            if($this->app_sendmail_flag) //メール送信を許可しているかどうかをまずチェック
            {

                if(is_array($this->app_sendmail_setting[$action]) && count($this->app_sendmail_setting[$action]))
                {
                    foreach($this->app_sendmail_setting[$action] as $authority)
                    {
                        $result = with(new Admin())->fetchAllbyAuthority($authority);
                        if($result) $target = array_merge($target , $result);
                    }
                }
            }
            return $target;

        }

}

?>