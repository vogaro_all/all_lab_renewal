<?php
use lacne\core\model\Admin;
use lacne\core\model\PostMeta;

//LACNE OPTION SETTING
define("LACNE_OPTION_WORKFLOW" , 1);
use lacne\core\model\Post;

/**
// ------------------------------------------------------------------------
 *  workflow.php
 *  (管理画面）承認・公開フロー処理用
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 * @copyright	        Copyright (c) 2008 - 2011, In Vogue Inc.
 */
// ------------------------------------------------------------------------

class Lib_flow extends Lib_post
{

        var $save_history = false; //履歴を保存するかどうか

        function Lib_flow($LACNE , $db){
            parent::Lib_post($LACNE , $db);
            $this->LACNE->load_library(array('login'));
        }

        /**
         * TODO 承認機能がない場合のことを考えて継承する
         * データのインサートもしくはアップデート処理
         * 公開権限があるユーザーならば通常どおりデータを挿入もしくはアップデート
         * 権限がなければ、仮保存処理
         *
         * @param string $table
         * @param object $data
         * @param string $login_id
         * @param string $key
         */
        function replace_post($data , $data_tags , $data_recommend, $key , $login_id="")
        {
            $tid = "";

            //公開権限があるか
            if($this->LACNE->library["login"]->chk_controll_limit("publish_post" , $login_id))
            {
                //権限ある→そのままインサートもしくはアップデート
                if(isset($data["id"]) && $data["id"])
                {
                    //更新モードならhistoryをアップデート
                    $data["history"] = $this->update_history($data["id"], "-", $data["user_id"], "更新処理" , fn_get_date()); //hisotry
                }
                else
                {
                    //新規記事ならhistoryを作成
                    $data["history"] = $this->create_new_history("" , $data["user_id"], "新規作成" , fn_get_date()); //hisotry作成
                }

                $tid = $this->postModel->replace_ownedmedia($data , $data_tags, $data_recommend, $key);
            }

            //公開権限がない（新規作成ならOK 編集モードなら非公開かつ自分自身が作成した記事かどうかチェックして処理）
            else
            {

                if($data["id"])
                {
                    //更新モードの場合、この記事が公開状態かどうか確認
                    $rs = $this->postModel->fetchOne($data["id"]);
                }
                if(!$data["id"] || ($rs && !$rs["output_flag"] && $this->LACNE->library["login"]->chk_my_edit_post($data["id"] , $login_id)))
                {
                    if(isset($data["id"]) && $data["id"])
                    {
                        //更新モードならhistoryをアップデート
                        $data["history"] = $this->update_history($data["id"], "wait", $data["user_id"], "承認待ち" , fn_get_date()); //hisotry
                    }
                    else
                    {
                        //新規記事ならhistoryを作成
                        $data["history"] = $this->create_new_history("wait" , $data["user_id"], "承認待ち" , fn_get_date());
                    }

                    //DB登録：post_base_id , status,historyを追記して新規挿入
                    $data["status"] = "wait";
                    $data["output_flag"] = 0;
                    $tid = $this->postModel->replace_ownedmedia($data , $data_tags, $data_recommend, $key);

                    //管理者あてに承認待ちの記事が作成された旨をメール送信
                    //メール送信をおこなう管理者アカウントのデータ取得
                    $target_admin_arr = $this->LACNE->library["login"]->get_sendmail_target("add_wait");
                    if($target_admin_arr && count($target_admin_arr))
                    {
                        //メール送信先（管理者宛て）
                        $send_to_arr = array();
                        foreach($target_admin_arr as $target_admin)
                        {
                            if(isset($target_admin["email"]))
                            {
                                $send_to_arr[] = $target_admin["email"];
                            }
                        }

                        if(count($send_to_arr))
                        {
                            //記事作成者
                            $editor_info = with(new Admin())->fetchOneByLoginID($data["user_id"]);

                            if($editor_info)
                            {
                                //メール本文データ
                                $mail_data = array(
                                    "editor_name" => $editor_info["user_name"],
                                    "title" => $data["title"],
//                                 	"id" => $data["id"],
                                	"id" => ($data["id"]) ? $data["id"] : $tid,
                                    "modified" => $data["modified"]
                                );

                                $this->app_action_sendmail($send_to_arr, "app_request_message", "承認待ちの記事があります", $mail_data);
                            }
                        }
                    }
                    //メール送信ここまで
                }
            }

            return $tid; //挿入IDを返す カスタムフィールドデータは（あれば）add.php側で挿入処理される（ここで返却した$tidを使って）
        }

        /**
         * 記事の公開切り替え
         *
         * @param number $id
         * @param srting $user_id //公開、非公開実行者
         */
        function change_publish($id , $user_id)
        {

            $result = 0;

            //公開権限があるか
            if($this->LACNE->library["login"]->chk_controll_limit("publish_post"))
            {
                if(is_numeric($id) && $this->postModel->cnt("id = ?" , array($id)))
                {
                    $rs = $this->postModel->fetchOne($id);
                    if($rs && isset($rs["status"]) && !$rs["status"]) //statusにwaitもbackもない状態なら公開切り替えする
                    {
                        $output_flag = $rs["output_flag"];

                        $updata = array();

                        $action = "";
                        if($output_flag == 1){
                            $updata["output_flag"] = 0;
                            $action = "非公開化";
                        }else{
                            $updata["output_flag"] = 1;
                            $action = "公開化";
                        }

                        $updata["id"] = $id;
                        //historyをアップデート
                        $updata["history"] = $this->update_history($id, "", $user_id, $action , fn_get_date()); //hisotry

                        $result = $this->postModel->replace($updata , "id");

                    }
                }
            }
            return $result;
        }


        /**
         * 承認待ちの記事を公開する
         *
         * @param number $post_id
         * @param srting $user_id //承認実行者
         */
        function app_publish($post_id , $user_id)
        {

            $result = 0;

            //公開権限があるか
            if($this->LACNE->library["login"]->chk_controll_limit("publish_post"))
            {

                $rs = $this->postModel->fetchOne($post_id);

                if($rs && isset($rs["status"]) && $rs["status"])
                {

                    //post_base_idに値があれば（入れ替えパターン。例えば、公開中の記事を別の内容に差し替えたい
                    //場合、新規作成したのち、公開と同時に入れ替え対象の記事と入れ替えをおこなう。とりあえず入れ替えする
                    //処理だけ実装
                    if(isset($rs["post_base_id"]) && $rs["post_base_id"])
                    {

                        $data = $rs;

                        //もとの記事データと入れ替え
                        $data["id"] = $rs["post_base_id"];
                        $data["output_flag"] = 1;
                        $data["status"] = "";
                        $data["post_base_id"] = 0;
                        $data["history"] = $this->update_history($data["id"], "", $user_id, "承認" , fn_get_date());
                        //$data["modified"] = fn_get_date();
                        $result = $this->postModel->replace($data , "id");

                        //入れ替えしたあと、記事データを削除
                        $this->postModel->delete($post_id);

                        //メタデータも入れ替え
                        with(new PostMeta())->delete($rs["post_base_id"]);

                        $meta_updata = array(
                            "post_id" => $rs["post_base_id"]
                        );
                        with(new PostMeta())->change_post_id($post_id , $rs["post_base_id"]);


                    }
                    //通常の承認→公開パターン
                    //output_flag値を1にし、statusを空にするだけ
                    else
                    {
                        $updata = array(
                            "id" => $rs["id"],
                            "output_flag" => 1,
                            "status" => "",
                            "history" => $this->update_history($rs["id"], "", $user_id, "承認" , fn_get_date()),
                            //"modified" => fn_get_date()
                        );

                        $result = $this->postModel->replace($updata , "id");

                        //記事作成者あてに承認された旨をメール送信
                        //メール送信をおこなう設定になっているかチェック
                        $sendmail_check = $this->LACNE->library["login"]->get_sendmail_target("wait_publish");
                        if($sendmail_check)
                        {
                            //メール送信先（記事を作成した投稿者宛て）
                            if(isset($rs["user_id"]) && $rs["user_id"])
                            {
                                $editor_info = with(new Admin())->fetchOneByLoginID($rs["user_id"]);

                                //承認を行った管理者名
                                $admin_user_info = with(new Admin())->fetchOneByLoginID($user_id);

                                if($editor_info)
                                {
                                    //メール本文データ
                                    $mail_data = array(
                                        "editor_name" => $editor_info["user_name"],
                                        "title" => $rs["title"],
                                        "modified" => $rs["modified"],
                                        "admin_user_name" => (isset($admin_user_info["user_name"]))?$admin_user_info["user_name"]:""
                                    );

                                    $this->app_action_sendmail(array($editor_info["email"]), "app_ok_message", "承認待ちの記事が公開されました", $mail_data);
                                }
                            }
                        }
                        //メール送信ここまで

                    }
                }
            }

            return $result;
        }

        /**
         * 承認待ちの記事を差し戻す
         *
         * @param number $post_id
         * @param srting $user_id //差戻し実行者
         * @param string $message
         */
        function app_back($post_id , $user_id , $message)
        {

            $result = 0;

            //公開権限があるか
            if($this->LACNE->library["login"]->chk_controll_limit("publish_post"))
            {

                $rs = $this->postModel->fetchOne($post_id);

                if($rs && isset($rs["status"]) && $rs["status"])
                {
                    $updata = array(
                        "id" => $post_id,
                        "output_flag" => 0,
                        "status" => "back",
                        "history" => $this->update_history($post_id, "back", $user_id, "差戻し：".$message , fn_get_date()),
                        //"modified" => fn_get_date()
                    );

                    $result = $this->postModel->replace($updata , "id");

                    //記事作成者あてに差戻しされた旨をメール送信
                    //メール送信をおこなう設定になっているかチェック
                    $sendmail_check = $this->LACNE->library["login"]->get_sendmail_target("wait_back");
                    if($sendmail_check)
                    {
                        //メール送信先（記事を作成した投稿者宛て）
                        if(isset($rs["user_id"]) && $rs["user_id"])
                        {
                            $editor_info = with(new Admin())->fetchOneByLoginID($rs["user_id"]);

                            //差戻しを行った管理者名
                            $admin_user_info = with(new Admin())->fetchOneByLoginID($user_id);

                            if($editor_info)
                            {
                                //メール本文データ
                                $mail_data = array(
                                    "editor_name" => $editor_info["user_name"],
                                    "title" => $rs["title"],
                                    "modified" => $rs["modified"],
                                    "admin_user_name" => (isset($admin_user_info["user_name"]))?$admin_user_info["user_name"]:"",
                                    "message" => $message
                                );

                                $this->app_action_sendmail(array($editor_info["email"]), "app_ng_message", "承認待ちの記事が差し戻しされました", $mail_data);
                            }
                        }
                    }
                    //メール送信ここまで
                }
            }
            return $result;
        }


        /**
         * historyを抽出（配列形式にする）
         * @param number $post_id
         * @return object
         */
        function get_history($post_id)
        {
            if(!$this->save_history) //historyの記録が無効なら空を返す
            {
                return array();
            }

            $data = $this->postModel->fetchOne($post_id);

            if($data && isset($data["history"]))
            {

                $history = $data["history"];
                //historyはシリアライズされている
                //return unserialize($history);
                return unserialize_base64_decode($history);
            }

            return array();
        }

        /**
         *
         * historyデータを取得・追記して返す
         *
         * @param number $post_id
         * @param string $status
         * @param number $user_id
         * @param string $message(差戻し時)
         * @param datetime $modified
         * @return string
         */
        function update_history($post_id , $status , $user_id , $message , $modified)
        {

            if(!$this->save_history) //historyの記録が無効なら空を返す
            {
                return "";
            }

            $add_history = array(
                "status"  =>$status ,
                "user_id" =>$user_id ,
                "message" =>$message ,
                "modieid" =>$modified
            );

            $history = $this->get_history($post_id);

            if($history)
            {
                array_push($history , $add_history);
            }
            else
            {
                $history = $add_history;
            }

            //return serialize($history);
            return serialize_base64_encode($history);

        }


        /**
         *
         * historyデータを新規作成
         *
         * @param string $status
         * @param number $user_id
         * @param string $message(差戻し時)
         * @param datetime $modified
         * @return string
         */
        function create_new_history($status , $user_id , $message , $modified)
        {
            if($this->save_history) //historyの記録が有効なら
            {
                //return serialize(array(array("status"=>$status , "user_id"=>$user_id, "message"=>$message , "modified"=>$modified))); //hisotry
                return serialize_base64_encode((array(array("status"=>$status , "user_id"=>$user_id, "message"=>$message , "modified"=>$modified)))); //hisotry
            }

            return "";
        }

        /**
         * 一覧中の「公開 / 非公開」または「承認・公開」メニュー表示
         * @param object $post_data
         * @param boolean $smp (スマフォの場合は画像なし)
         */
        function publish_menu($post_data , $smp = false)
        {
            $return = "";

            $menu_str = "";
            $menu_html_str = "";
            $waiting = 0;
            //対象記事が承認待ちもしくは差戻しの状態かどうか
            if(isset($post_data["status"]) && ($post_data["status"] == "wait" || $post_data["status"] == "back"))
            {
                if($post_data["status"] == "wait")
                {
                    $menu_str = "承認待ち";
                }
                else
                {
                    $menu_str = "差戻し";
                }
                $menu_html_str = '<a href="#" class="link_waiting" id="waiting_'.$post_data["id"].'">';
                $menu_html_str_close = '</a>';
                $waiting = 1;
            }
            else if(isset($post_data["output_flag"]))
            {
                if($post_data["output_flag"] == 1)
                {
                    if(!$smp)
                    {
                        $menu_str = '<img src="'.LACNE_SHAREDATA_PATH.'/images/common/table_img_public_on.gif" alt="公開" />';
                    }
                    else
                    {
                        $menu_str = "公開";
                    }
                }
                else
                {
                    if(!$smp)
                    {
                        $menu_str = '<img src="'.LACNE_SHAREDATA_PATH.'/images/common/table_img_public_off.gif" alt="未公開" />';
                    }
                    else
                    {
                        $menu_str = "未公開";
                    }
                }
                $menu_html_str = '<span class="link_publish" id="publish_'.$post_data["id"].'">';
                $menu_html_str_close = '</span>';

            }


            //公開権限があるか
            if($this->LACNE->library["login"]->chk_controll_limit("publish_post"))
            {
                $return = $menu_html_str.$menu_str.$menu_html_str_close;
            }
            else
            {
                $return = $menu_str;
            }


            return $return;

        }


        function app_action_sendmail($send_address_arr , $template_filename , $mail_title , $mail_body , $from = FROM_MAINADDRESS)
        {
            //テンプレートDirを変更
            $view_dir = $this->LACNE->template->getViewDir();
            $this->LACNE->template->setViewDir(dirname(__FILE__)."/mail_template");

            $mail_body = $this->LACNE->render($template_filename , $mail_body , true);

            //メール送信
            $this->LACNE->load_library(array('sendmail'));
            //送信先アドレスを1件取得し、2件目以降はCCにする
            $send_to = array_shift($send_address_arr);
            $send_to_cc = array();
            if(count($send_address_arr))
            {
                foreach($send_address_arr as $send_address)
                {
                    $send_to_cc[] = array($send_address);
                }
            }
            //メール送信
            $this->LACNE->library["sendmail"]->sendmail($send_to ,$mail_title, $mail_body , $from , $send_to_cc);

            //テンプレートdirをもとに戻す
            $this->LACNE->template->setViewDir($view_dir);

            return;
        }
}

?>