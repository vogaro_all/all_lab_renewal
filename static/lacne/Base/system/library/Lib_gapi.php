<?php
use lacne\core\Library;
require_once(DIR_VENDORS."/gapi/src/Google/autoload.php");

/**
// ------------------------------------------------------------------------
 * Lib_gapi
 *
 * Googleアナリティクスリクエスト
 * @package		LACNE
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_gapi extends Library
{

	/** @var object $client */
    var $client;

    /** @var object $profile */
    var $profile;

    /** @var object $analytics */
	var $analytics;

    /** @var string $service_account_email */
    var $service_account_email = GAPI_ACCOUNT_EMAIL;

    /** @var string $key_file_location */
    var $key_file_location = GAPI_KEY_FILE;

    /**
     * Lib_gapi constructor.
     * @param $LACNE
     * @param $db
     */
    function Lib_gapi($LACNE , $db) {
        parent::__construct($LACNE, $db);
    }

    /**
     * get_service
     */
    function get_service()
    {
        $this->client = new Google_Client();
        $this->client->setApplicationName("MyAnalyticsApp");
        $this->analytics = new Google_Service_Analytics($this->client);

        $key = file_get_contents(DIR_VENDORS.'gapi/key/'.$this->key_file_location);
        $cred = new Google_Auth_AssertionCredentials(
          $this->service_account_email,
          array(Google_Service_Analytics::ANALYTICS_READONLY),
          $key
        );
        $this->client->setAssertionCredentials($cred);
        if($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion($cred);
        }
    }

    /**
     * get_service アカウントが可変の場合
     */
    function get_service_multi($target_view)
    {
    	$this->service_account_email = constant('GAPI_ACCOUNT_EMAIL_'.$target_view);
    	$this->key_file_location = constant('GAPI_KEY_FILE_'.$target_view);

    	$this->client = new Google_Client();
    	$this->analytics = new Google_Service_Analytics($this->client);
    	$key = file_get_contents(DIR_VENDORS.'gapi/key/'.$this->key_file_location);
    	$cred = new Google_Auth_AssertionCredentials(
    			$this->service_account_email,
    			array(Google_Service_Analytics::ANALYTICS_READONLY),
    			$key
    	);
    	$this->client->setAssertionCredentials($cred);
    	if($this->client->getAuth()->isAccessTokenExpired()) {
    		$this->client->getAuth()->refreshTokenWithAssertion($cred);
    	}
    	return $this->analytics;
    }

    /**
     * get_first_profile_id
     */
    function get_first_profile_id() {
        $accounts = $this->analytics->management_accounts->listManagementAccounts();

        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();

            $properties = $this->analytics->management_webproperties
            ->listManagementWebproperties($firstAccountId);

            if (count($properties->getItems()) > 0) {
                $items = $properties->getItems();
                $firstPropertyId = $items[0]->getId();

                $profiles = $this->analytics->management_profiles
                ->listManagementProfiles($firstAccountId, $firstPropertyId);

                if (count($profiles->getItems()) > 0) {
                    $items = $profiles->getItems();
                    $this->profile = $items[0]->getId();
                } else {
                    throw new Exception('No views (profiles) found for this user.');
                }
            } else {
                throw new Exception('No properties found for this user.');
            }
        } else {
            throw new Exception('No accounts found for this user.');
        }
    }

//     /**
//      * get_pv_best_data
//      * @return array
//      */
//     function get_pv_best_data()
//     {
//         $this->get_service();
//         $this->get_first_profile_id();

//         $siteOpenDay = '2016-04-15';

//         $result = $this->analytics->data_ga->get(
//             'ga:' . constant('VIEW_ID_OWNED'),
//             $siteOpenDay,
//             'yesterday',
//             'ga:pageviews',
//             array(
//                 'dimensions'  => 'ga:pageTitle,ga:pagePath',
//                 'sort'        => '-ga:pageviews',
//             	'filters'     => 'ga:pagePath=~/detail/(\d);ga:hostname=~library.kumon.ne.jp',
//                 'max-results' => '50'
//             )
//         );
//         return $result['rows'];
//     }

//     /**
//      * get_pv_keyword_data
//      * @return array
//      */
//     function get_pv_keyword_data()
//     {
//         $this->get_service();
//         $this->get_first_profile_id();
//         $result = $this->analytics->data_ga->get(
//             'ga:' . constant('VIEW_ID_OWNED'),
//             '7daysAgo',
//             'yesterday',
//             'ga:pageviews',
//             array(
//                 // カスタムディメンションでタグIDを取得する
//                 'dimensions'  => 'ga:dimension4',
//                 'sort'        => '-ga:pageviews',
//             	'filters'     => 'ga:pagePath=~/detail/(\d);ga:hostname=~library.kumon.ne.jp;ga:dimension4!=/大画/文/大見/SNS/文/SNS/大画/文/大見/SNS/文/SNS/SNS/文/',
//                 'max-results' => '15'
//             )
//         );
//         return $result['rows'];
//     }

    // /**
    //  * get_pv_pickup_data
    //  * @return array
    //  */
    // function get_pv_pickup_data()
    // {
    //     $this->get_service();
    //     $this->get_first_profile_id();

    //     $siteOpenDay = '2016-04-15';

    //     $result = $this->analytics->data_ga->get(
    //         'ga:' . constant('VIEW_ID_OWNED'),
    //         $siteOpenDay,
    //         'yesterday',
    //         'ga:pageviews',
    //         array(
    //             'dimensions'  => 'ga:pageTitle,ga:pagePath',
    //             'sort'        => '-ga:pageviews',
    //             'filters'     => 'ga:pagePath=~/detail/(\d);ga:hostname=~library.kumon.ne.jp',
    //             'max-results' => '50'
    //         )
    //     );
    //     return $result['rows'];
    // }


    // 効果測定　オウンドメディア　コンバージョン　コンテンツ単位
    function get_conversion_contents ($target_view, $search_date_from, $search_date_to, $sort_target, $display_count, $filter) {
    	$results = $this->analytics->data_ga->get(
    			'ga:' . constant('VIEW_ID_'.$target_view),
    			$search_date_from,
    			$search_date_to,
    			'ga:goal1Completions,ga:goal2Completions',
    			array(
    					'dimensions' => 'ga:goalCompletionLocation,ga:dimension2', // 副指標ごと
    					'sort' => $sort_target, // - を付けると降順ソート
    					'max-results' => $display_count, // 取得件数
    					'filters' => $filter // TOPページ除外
    			)
    	);
    	$analytics_data = $results->getRows();
    	return $analytics_data;
    }
    
    // 効果測定　オウンドメディア　コンバージョン　コンテンツ単位　CVR計算用のにPV数取得
    function get_conversion_pv_contents ($target_view, $search_date_from, $search_date_to, $sort_target, $display_count, $filter) {
		$results = $this->analytics->data_ga->get(
				'ga:' . constant('VIEW_ID_'.$target_view),
				$search_date_from,
				$search_date_to,
				'ga:pageviews',
				array(
						'dimensions' => 'ga:pagePath,ga:pageTitle', // 副指標ごと
						'max-results' => 2500, // 取得件数
						'filters' => $filter // TOPページ除外
				)
		);
		$pageview_data = $results->getRows();
    	
    	return $pageview_data;
    }
    
    // 効果測定　オウンドメディア　コンバージョン　テンプレ単位
    function get_conversion_template ($target_view, $search_date_from, $search_date_to, $sort_target, $display_count) {
    	$results = $this->analytics->data_ga->get(
    			'ga:' . constant('VIEW_ID_'.$target_view),
    			$search_date_from,
    			$search_date_to,
    			'ga:goal1Completions,ga:goal2Completions',
    			array(
    					'dimensions' => 'ga:dimension3', // 副指標ごと
    					'sort' => $sort_target,          // - を付けると降順ソート
    					'max-results' => $display_count, // 取得件数
    			)
    	);
    	$analytics_data = $results->getRows();
    	return $analytics_data;
    }
    
    // 効果測定　オウンドメディア　コンバージョン　テンプレ単位　CVR計算用のにPV数取得
    function get_conversion_pv_template ($target_view, $search_date_from, $search_date_to, $sort_target, $display_count) {
		$results = $this->analytics->data_ga->get(
				'ga:' . constant('VIEW_ID_'.$target_view),
				$search_date_from,
				$search_date_to,
				'ga:pageviews',
				array(
						'dimensions' => 'ga:dimension3', // 副指標ごと
						'max-results' => 2500,           // 取得件数
				)
		);
		$pageview_data = $results->getRows();
    	
    	return $pageview_data;
    }
    
    // 効果測定　オウンドメディア　コンバージョン　合計
    function get_sum_conversion_contents ($target_view, $search_date_from, $search_date_to, $display_count, $filter) {
    	$results = $this->analytics->data_ga->get(
    			'ga:' . constant('VIEW_ID_'.$target_view),
    			$search_date_from,
    			$search_date_to,
    			'ga:goal1Completions,ga:goal2Completions',
    			array(
    					'dimensions' => '', // 副指標ごと
    					'max-results' => DISPLAY_COUNT, // 取得件数
    					'filters' => $filter // TOPページ除外
    			)
    	);
    	$analytics_data = $results->getRows();
    	return $analytics_data;
    }

	// 効果測定　オウンドメディア　コンバージョン　合計
	function get_sum_conversion_pv_contents ($target_view, $search_date_from, $search_date_to, $display_count, $filter) {
		$results = $this->analytics->data_ga->get(
			'ga:' . constant('VIEW_ID_'.$target_view),
			$search_date_from,
			$search_date_to,
			'ga:pageviews',
			array(
				'dimensions' => '', // 副指標ごと
				'max-results' => DISPLAY_COUNT, // 取得件数
				'filters' => $filter // TOPページ除外
			)
		);
		$analytics_data = $results->getRows();
		return $analytics_data;
	}

    // 効果測定　ブランドサイト　コンバージョン　
    function get_brand_conversion_contents ($target_view, $search_date_from, $search_date_to, $sort_target, $display_count, $filter) {
    	$results = $this->analytics->data_ga->get(
    			'ga:' . constant('VIEW_ID_'.$target_view),
    			$search_date_from,
    			$search_date_to,
    			'ga:goal2Completions,ga:goal4Completions,ga:goal2ConversionRate,ga:goal4ConversionRate',
    			array(
    					'dimensions' => 'ga:pageTitle,ga:pagePath', // 副指標ごと
    					'sort' => $sort_target,          // - を付けると降順ソート
    					'max-results' => $display_count, // 取得件数
    			)
    	);
    	$pageview_data = $results->getRows();
    	return $pageview_data;
    }

}