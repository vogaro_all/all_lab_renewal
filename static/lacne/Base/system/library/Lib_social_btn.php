<?php
use lacne\core\Library;
/**
// ------------------------------------------------------------------------
 * Lib_social_btn.php
 * ソーシャルボタン設置用
 * @package		Lacne
 * @author		In Vogue Inc. 2008 -
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Lib_social_btn extends Library
{

	/**
	 * Lib_social_btn constructor.
	 * @param $LACNE
	 * @param $db
	 */
	function Lib_social_btn($LACNE , $db) {
		parent::__construct($LACNE, $db);
	}
	
}

?>