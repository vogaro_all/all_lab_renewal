<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_app_connect.php
 * app_connect(アプリ接続ユーザー管理）用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class AppConnect extends Model
{

    /**
     * Model_app_connect constructor.
     */
    public function __construct(){ parent::__construct(); }

    /**
     * データ1件取得
     * @param number $id
     * @return object
     */
    function fetchOne($id)
    {
        if(is_numeric($id))
        {
            $sql = "SELECT * FROM ".TABLE_APP_CONNECT." WHERE id = ?";
            return $this->_fetchOne($sql, array($id));
        }

        return;
    }

    /**
     * ログインアカウントデータ1件取得
     * @param string $login_id
     * @return object
     */
    function fetchOneByLoginID($login_id)
    {

        $sql = "SELECT * FROM ".TABLE_APP_CONNECT." WHERE login_id = ?";
        return $this->_fetchOne($sql, array($login_id));

    }

    /**
     * ログインアカウントデータ1件取得
     * @param string $login_id
     * @return object
     */
    function fetchOneByConnectionID($connection_id)
    {

        $sql = "SELECT * FROM ".TABLE_APP_CONNECT." WHERE connection_id = ?";
        return $this->_fetchOne($sql, array($connection_id));

    }

    /**
     * データのインサートもしくはアップデート処理
     *
     * @param object $data
     * @param string $key
     * @return number
     */
    function replace($data , $key)
    {
        return $this->adodb_replace(TABLE_APP_CONNECT , $data , $key);
    }

    /**
     * DELETE
     * @param string $where
     * @param array $param
     * @return boolean
     */
    function delete($where , $param)
    {

        $sql = "DELETE FROM ".TABLE_APP_CONNECT." WHERE ".$where;
        return $this->_execute($sql, array($param));

    }


}