<?php namespace lacne\core\model;
use lacne\core\Model;

/**
// ------------------------------------------------------------------------
 * model_parts.php
 * パーツデータ用モデル
 * @package		Lacne
 * @author		In Vogue Inc.
 * @link		http://lacne.jp
 */
// ------------------------------------------------------------------------

class Parts extends Model
{


	/**
	 *  コンストラクタ
	 *
	 *  @param  $session Class_Sessionオブジェクト
	 *  @return void
	 */
	function Model_parts($LACNE , $db) {
        parent::Class_Model($LACNE , $db);
	}

        /**
         * 指定したpost_idのパーツデータ取得
         * @param string $post_id
         * @return object
         */
        function findByPostID($post_id , $format = '' , $has_value = false)
        {

            $param = array();
            $sql = "SELECT * FROM ".$this->getTableName("parts")." as parts";

            if($has_value) {
                $sql .= " LEFT JOIN ".$this->getTableName("parts_value")." as parts_value ON parts.id = parts_value.ownedmedia_parts_id	";
            }

            $param[] = $post_id;
            $sql .= " WHERE parts.ownedmedia_posts_id = ?";

            if($format) {
                $sql .= " AND parts.format = ?";
                $param[] = $format;
            }

            $sql .= " ORDER BY parts.sort_no ASC";

            return $this->_fetchAll($sql, $param);

        }

        function save($parts_config , $post_id , $data = array()) {
            //-----------------------------------
            //トランザクション開始
            $this->conn->StartTrans();
            //-----------------------------------

            //現在登録されているpartsデータを取得してくる
            $registered_parts_data = $this->findByPostID($post_id);

            $cnt = 1;

            if(!empty($data)) {
                foreach($data as $section_data) {
                    $format = key($section_data);
                    if(!isset($parts_config[$format])) {
                        continue;
                    }

                    $save_parts_data = array(
                        'ownedmedia_posts_id'   => $post_id,
                        'format'    => $format,
                        'abbr'      => $parts_config[$format]['abbr'],
                        'sort_no'   => $cnt,
                        'created'   => fn_get_date(),
                        'modified'  => fn_get_date()
                    );

                    //すでに登録されているpartsデータがあれば、そのIDをつかって上書き
                    if(!empty($registered_parts_data)) {
                      $registered_parts = array_shift($registered_parts_data);

                      $parts_id = $registered_parts['id'];

                      $save_parts_data['id'] = $parts_id;

                      //このpartsデータに紐付いているvalueテーブルのデータは一旦削除
                      $this->delete_parts_value('ownedmedia_parts_id = ?' , $parts_id);
                    }

                    $save_parts_id = $this->replace($save_parts_data , "id");

                    if($save_parts_id) {
                        //valueテーブルにデータ挿入
                        foreach($section_data[$format] as $item_key => $item_value) {
                            if(isset($parts_config[$format]['items'][$item_key])) {
                                $save_parts_value_data = array(
                                    'ownedmedia_parts_id'  => $save_parts_id,
                                    'data_key'  => $item_key,
                                    'data_value' => preg_replace('/[\xF0-\xF7][\x80-\xBF][\x80-\xBF][\x80-\xBF]/', '', $item_value)
                                );
                                $this->replace_parts_value($save_parts_value_data , "id");
                            }
                        }
                    }

                    $cnt++;
                }
            }
            //すでに登録されていたpartsデータが余っていれば、削除対象となる
            //-------------------------------------------------
            if(!empty($registered_parts_data)) {
                foreach($registered_parts_data as $registered_parts) {
                    $this->delete('id = ?' , array($registered_parts['id']));
                    $this->delete_parts_value('ownedmedia_parts_id = ?' , array($registered_parts['id']));
                }
            }

            //-----------------------------------
            //コミット、またはロールバック
            //StartTrans()で開始されたトランザクションをここで完了。エラーがなければコミット(true)、エラーが発生していればロールバックされる(false)
            $db_trans_result = $this->conn->CompleteTrans(true);
            //-----------------------------------

            return $db_trans_result;

        }

        /**
         *
         * データのインサートもしくはアップデート処理
         *
         * @param object $data
         * @param string $key
         * @param boolean $auto_quote
         */
        function replace($data , $key , $auto_quote = true)
        {
            $tid = $this->adodb_replace($this->getTableName("parts") , $data , $key);

            return $tid;
        }
        function replace_parts_value($data , $key , $auto_quote = true)
        {
            $tid = $this->adodb_replace($this->getTableName("parts_value") , $data , $key);

            return $tid;
        }

        function delete($where , $param)
        {

            $sql = "DELETE FROM ".$this->getTableName("parts")." WHERE ".$where;
            $this->_execute($sql, $param);

            return;
        }
        function delete_parts_value($where , $param)
        {

            $sql = "DELETE FROM ".$this->getTableName("parts_value")." WHERE ".$where;
            $this->_execute($sql, $param);

            return;
        }


        //DBから取得したpartsデータを表示用に組み替える
        function convert_parts_data($db_parts_data, $wantAbbr = false) {
            /*
                [0] => Array
        (
            [id] => 25
            [post_id] => 2
            [format] => head2
            [abbr] =>
            [sort_no] => 1
            [created] => 2016-02-29 11:54:10
            [modified] => 2016-02-29 11:54:10
            [parts_id] => 7
            [data_key] => content
            [data_value] => AAAAA
        )

    [1] => Array
        (
            [id] => 26
            [post_id] => 2
            [format] => text2
            [abbr] =>
            [sort_no] => 2
            [created] => 2016-02-29 11:54:10
            [modified] => 2016-02-29 11:54:10
            [parts_id] => 12
            [data_key] => content
            [data_value] => CCCCCCC
        )

    [2] => Array
        (
            [id] => 28
            [post_id] => 2
            [format] => profile2
            [abbr] =>
            [sort_no] => 3
            [created] => 2016-02-29 11:54:10
            [modified] => 2016-02-29 11:54:10
            [parts_id] => 13
            [data_key] => simei
            [data_value] => ああああ
        )

    [3] => Array
        (
            [id] => 27
            [post_id] => 2
            [format] => profile2
            [abbr] =>
            [sort_no] => 3
            [created] => 2016-02-29 11:54:10
            [modified] => 2016-02-29 11:54:10
            [parts_id] => 13
            [data_key] => img
            [data_value] => /lacne/news_test/upload/img0004.jpg
        )

    [4] => Array
        (
            [id] => 29
            [post_id] => 2
            [format] => profile2
            [abbr] =>
            [sort_no] => 3
            [created] => 2016-02-29 11:54:10
            [modified] => 2016-02-29 11:54:10
            [parts_id] => 13
            [data_key] => soshiki
            [data_value] => ううううう
        )
        */

            $return_data = array();

            if(!empty($db_parts_data)) {
                foreach($db_parts_data as $data) {
                    if(!isset($return_data[$data['sort_no']])) {
                        $return_data[$data['sort_no']] = array(
                            $data['format'] => array()
                        );
                    }

                    $return_data[$data['sort_no']][$data['format']][$data['data_key']] = $data['data_value'];
                    if($wantAbbr)
                    {
                    	$return_data[$data['sort_no']]['abbr'] = $data['abbr'];
                    }
                }
            }

            return $return_data;
        }
}

?>