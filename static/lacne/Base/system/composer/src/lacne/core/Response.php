<?php namespace lacne\core;

class Response
{

    var $http_headers = array();
    var $status_code = 200;
    var $status_text = 'OK';

    /**
     * Response constructor.
     */
    public function __construct(){}

    /**
     * レスポンス送信
     */
    function output($content)
    {

        header('HTTP/1.1 ' . $this->status_code . ' ' . $this->status_text);

        foreach ($this->http_headers as $name => $value) {
            header($name . ': ' . $value);
        }

        echo $content;
        exit;
    }


    /**
     * ステータスコードを設定
     *
     * @param integer $status_code
     * @param string $status_code
     */
    function setStatusCode($status_code, $status_text = '')
    {
        $this->status_code = $status_code;
        $this->status_text = $status_text;
    }

    /**
     * HTTPレスポンスヘッダ設定
     *
     * @param string $name
     * @param mixed $value
     */
    function setHttpHeader($name, $value)
    {
        $this->http_headers[$name] = $value;
    }
}
