<?php namespace lacne\core;

include_once(DIR_VENDORS."adodb/adodb.inc.php");
$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

/**
 * Class AdoDB
 * @package lacne\core
 */
class AdoDB
{
    /**
     * HACK protectedにして外部から参照できないように調整する必要がある
     * @var
     */
    public $conn;

    /**
     * 特になし
     * AdoDB constructor.
     */
    public function __construct(){}

    /**
     * DB接続処理
     * @param array $params //接続情報上書き変更用
     */
    public function connect($params = array())
    {
        $db_info = array(
            "type" => DB_TYPE,
            "host" => DB_HOST,
            "user" => DB_USER,
            "pass" => DB_PASS,
            "database" => DB_NAME
        );

        if(defined("RUN_PHP_UNIT_TEST") && RUN_PHP_UNIT_TEST) $db_info["database"] .= "_test";

        $db_info = array_merge($db_info, $params);

        $conn = &NewADOConnection($db_info["type"]);
        $conn->Connect($db_info["host"],$db_info["user"],$db_info["pass"],$db_info["database"]);

        if(!$conn){
            die("Database Connect Error");
        }

        $conn->execute("SET NAMES utf8");
        $conn->execute("SET @@session.sql_mode = '';");
        $this->conn = $conn;

        //デバッグ設定
        if(DEBUG_MODE_DB){
            $this->conn->debug = true;
        }
    }

    /**
     * @return mixed
     */
    public function get_connection(){ return $this->conn; }

    /**
     * エスケープ
     * @param $str
     * @return mixed
     */
    public function esc($str){ return $this->conn->qstr($str); }

}