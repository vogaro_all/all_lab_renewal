<?php if (!empty($entity->related_url)) {?>
          
          <div class="related related--house">
            <a href="<?php echo( $entity->related_url ); ?>" class="related__inner">
              <div class="related__label">関連記事</div>
              <?php if (!empty($entity->related_img)) {?><div class="related__image" style="background-image: url(<?php echo( $entity->related_img ); ?>);"></div><?php }?>
              <div class="related__content">
                <?php if (!empty($entity->related_title)) {?><p class="title"><?php echo( $entity->related_title ); ?></p><?php }?>
                <?php if (!empty($entity->content) || !empty($entity->related_description)) {?>
                  <?php if (!empty($entity->content)) {?>
                <p class="text"><?php echo(htmlspecialchars_decode($entity->content));?></p>
                	<?php } elseif (!empty($entity->related_description)) {?>
                <p class="text"><?php echo(htmlspecialchars_decode($entity->related_description));?></p>
                	<?php }?>
                <?php }?>
              </div>
            </a>
          </div>

<?php } ?>