<?php if (isset($entity['amp_flg']) && !empty($entity['amp_flg'])) {?>
          <section class="section section--grid-reverse">
            <div class="section__inner">
              <?php if (!empty($entity->title_name)) {?>
              <h2 class="heading2"><?php echo( $entity->title_name ); ?></h2>
              <?php }?>
              <p class="text"><?php echo(htmlspecialchars_decode($entity->content));?></p>
            </div>
            <div class="image"><amp-img src="<?php echo( $entity->getImg() ); ?>" alt=""  width="325" height="211" layout="responsive"></amp-img></div>
          </section>
<?php } else { ?>
          <section class="section section--grid-reverse">
            <div class="section__inner">
              <?php if (!empty($entity->title_name)) {?>
              <h2 class="heading2"><?php echo( $entity->title_name ); ?></h2>
              <?php }?>
              <p class="text"><?php echo(htmlspecialchars_decode($entity->content));?></p>
            </div>
            <div class="image"><img src="<?php echo( $entity->getImg() ); ?>" alt=""></div>
          </section>
<?php } ?>
