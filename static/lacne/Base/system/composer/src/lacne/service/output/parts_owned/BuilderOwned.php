<?php namespace lacne\service\builder;

use lacne\domain\OwnedMedia\Entity;
use lacne\domain\OwnedMedia\entity\MediaEntity;
use lacne\domain\News\entity\NewsEntity;
use lacne\core\Template as LACNE_TEMPLATE;
use lacne\service\output\OwnedMediaOtherAPI;

class BuilderOwned
{
    protected $templates = array();

    public function __construct()
    {
        $this->templates = array(
            _Header1::getKey()  => _Header1::getKlass(),
            _Header2::getKey()  => _Header2::getKlass(),
            _Header3::getKey()  => _Header3::getKlass(),
            _IMG1::getKey()     => _IMG1::getKlass(),
            _IMG2::getKey()     => _IMG2::getKlass(),
        	_IMG3::getKey()     => _IMG3::getKlass(),
        	_IMG4::getKey()     => _IMG4::getKlass(),
        	_IMG5::getKey()     => _IMG5::getKlass(),
            _IMG6::getKey()     => _IMG6::getKlass(),
            _Text1::getKey()    => _Text1::getKlass(),
            _Text2::getKey()    => _Text2::getKlass(),
            _Text3::getKey()    => _Text3::getKlass(),
            _Text4::getKey()    => _Text4::getKlass(),
            _Text5::getKey()    => _Text5::getKlass(),
            _Text6::getKey()    => _Text6::getKlass(),
            _Text7::getKey()    => _Text7::getKlass(),
            _Text8::getKey()    => _Text8::getKlass(),
            _Text9::getKey()    => _Text9::getKlass(),
            _Text10::getKey()    => _Text10::getKlass(),
            _Text11::getKey()    => _Text11::getKlass(),
            _Text12::getKey()    => _Text12::getKlass(),
            _Cv1::getKey()      => _Cv1::getKlass(),
            _Cv2::getKey()      => _Cv2::getKlass(),
            _Profile1::getKey() => _Profile1::getKlass(),
            _Profile2::getKey() => _Profile2::getKlass(),
        );

        // 連続表示に対応するセクションパーツのキー配列
        $this->successiveTemplates =array(
            _Text4::getKey()     => _Text4::getKlass(),
        	_Text9::getKey()     => _Text9::getKlass(),
//             _IMG2::getKey()     => _IMG2::getKlass(),
//             _Profile2::getKey() => _Profile2::getKlass()
        );
        
        // 連続表示の繰り返し回数を設定
        $this->maxContinue =array(
//         	_IMG2::getKey()     => 2,
        );
    }

    protected static function GET_SECTION_KEY_AND_VALUE($sectionItem)
    {
        if( is_null($sectionItem) ){
            return array("", "");
        }

        foreach($sectionItem as $key => $value){
            return array($key, $value);
        }
    }

    /**
     * @param MediaEntity $entity
     */
    public function doit(MediaEntity $entity)
    {
        $sectionIterator = new \ArrayIterator( $entity->section );
        foreach(  $sectionIterator as $_ => $items) {
            foreach( $items as $key => $item ){

            	// sort_noを出力させる為に設定
            	$item['_sort_no'] = $_;
            	
                if( array_key_exists($key, $this->templates) ){
                    
                    $className = $this->templates[$key];
                    $class = new $className($item);

                    if ($key == 'text10') {
                        $related_posts_id = $class->title_name;
                        $ownedMediaOtherAPI = new OwnedMediaOtherAPI();
                        $related_posts_data = $ownedMediaOtherAPI->get_post_data($related_posts_id);
                        
                        if (isset($related_posts_data) && !empty($related_posts_data) ){
                            $class->related_title = $related_posts_data['title'];
                            $class->related_url = '/contents/'.$related_posts_id;
                            $class->related_img = $related_posts_data['thumb_image'];
                            $class->related_description = $related_posts_data['description'];
                        }
                    }
                    
                    
                    // 繰り返し処理のrange幅の設定
                    $range = 1000;
                    if (array_key_exists($key, $this->maxContinue)) {
                    	// 1以下はループ回数が意図しない回数になるので除外
                    	if ($this->maxContinue[$key]>1) {
                    		$range = $this->maxContinue[$key]-1;
                    	}
                    }
                    
                    // 拡張 同じパーツが連続する場合、呼んでくる機能
                    foreach(range(1, $range) as $_ ){
                        if( $sectionIterator->valid() ){
                            $position = $sectionIterator->key();
                            $sectionIterator->next();

                            $next_key_and_value = self::GET_SECTION_KEY_AND_VALUE( $sectionIterator->current() );
                            $sectionIterator->seek($position-1);
                        }
                        
                        if( $key == $next_key_and_value[0] && array_key_exists($key, $this->successiveTemplates)){
                        	
                        	// sort_noを出力させる為に設定
                        	$next_item = $next_key_and_value[1];
                        	$next_item['_sort_inno'] = $item['_sort_no']+$_;
                        	
                            $class->setComposeItem( new $className($next_item) );
                            $sectionIterator->next();
                        }else{
                            break;
                        }
                    }
                        
                    $class->render();
                }
            }
        }
    }

    /**
     * @param MediaEntity $entity
     */
    public function doitAmp(MediaEntity $entity)
    {
        $sectionIterator = new \ArrayIterator( $entity->section );
        foreach(  $sectionIterator as $_ => $items) {

            foreach( $items as $key => $item ){
                
                // AMPの画像タグ変更用フラグ
                if (strpos($key,'img') !== false) {
                    $item['amp_flg'] = 1;
                }
                
                // AMPの画像タグ<amp-img>へ置換
                if ($key == 'text2') {

                    // 閉じタグのスラッシュ「/>」削除
                    $pattern = '/<img ([^>]+)\/>/i';
                    $replace = '<img $1>';
                    $content = preg_replace($pattern, $replace, htmlspecialchars_decode($item['content']));
                    
                    $pattern = '/<img ([^>]+)>/i';
                    $replace = '<amp-img $1 layout="responsive"></amp-img>';
                    $content = preg_replace($pattern, $replace, $content);

                    // サイト外からの画像は layout="fixed" にする
                    $pattern = '/<amp-img ([^>]*src="https.*"[^>]*)+layout="responsive">/i';
                    $replace = '<amp-img $1 layout="fixed"></amp-img>';
                    $content = preg_replace($pattern, $replace, $content);

                    // アフェリエイトのサムネイルのサイズ固定
                    $pattern = '/<amp-img ([^>]*src="https:\/\/thumbnail.*"[^>]*)+>/i';
                    $replace = '<amp-img $1 width="128" height="128"></amp-img>';
                    $content = preg_replace($pattern, $replace, $content);
                    
                    $item['content'] = $content;
                }
                
                // sort_noを出力させる為に設定
                $item['_sort_no'] = $_;
                
                if( array_key_exists($key, $this->templates) ){
                    $className = $this->templates[$key];
                    $class = new $className($item);
                    
                    // 繰り返し処理のrange幅の設定
                    $range = 1000;
                    if (array_key_exists($key, $this->maxContinue)) {
                        // 1以下はループ回数が意図しない回数になるので除外
                        if ($this->maxContinue[$key]>1) {
                            $range = $this->maxContinue[$key]-1;
                        }
                    }
                    
                    // 拡張 同じパーツが連続する場合、呼んでくる機能
                    foreach(range(1, $range) as $_ ){
                        if( $sectionIterator->valid() ){
                            $position = $sectionIterator->key();
                            $sectionIterator->next();
                            
                            $next_key_and_value = self::GET_SECTION_KEY_AND_VALUE( $sectionIterator->current() );
                            $sectionIterator->seek($position-1);
                        }
                        
                        if( $key == $next_key_and_value[0] && array_key_exists($key, $this->successiveTemplates)){
                            
                            // sort_noを出力させる為に設定
                            $next_item = $next_key_and_value[1];
                            $next_item['_sort_inno'] = $item['_sort_no']+$_;
                            
                            $class->setComposeItem( new $className($next_item) );
                            $sectionIterator->next();
                        }else{
                            break;
                        }
                    }
                    
                    $class->render();
                }
            }
        }
    }
    
    /**
     * @param NewsEntity $entity
     */
    public function doit_news(NewsEntity $entity)
    {
        $sectionIterator = new \ArrayIterator( $entity->section );
        foreach(  $sectionIterator as $_ => $items) {
            foreach( $items as $key => $item ){
                
                // sort_noを出力させる為に設定
                $item['_sort_no'] = $_;
                
                if( array_key_exists($key, $this->templates) ){
                    $className = $this->templates[$key];
                    $class = new $className($item);
                    
                    // 繰り返し処理のrange幅の設定
                    $range = 1000;
                    if (array_key_exists($key, $this->maxContinue)) {
                        // 1以下はループ回数が意図しない回数になるので除外
                        if ($this->maxContinue[$key]>1) {
                            $range = $this->maxContinue[$key]-1;
                        }
                    }
                    
                    // 拡張 同じパーツが連続する場合、呼んでくる機能
                    foreach(range(1, $range) as $_ ){
                        if( $sectionIterator->valid() ){
                            $position = $sectionIterator->key();
                            $sectionIterator->next();
                            
                            $next_key_and_value = self::GET_SECTION_KEY_AND_VALUE( $sectionIterator->current() );
                            $sectionIterator->seek($position-1);
                        }
                        
                        if( $key == $next_key_and_value[0] && array_key_exists($key, $this->successiveTemplates)){
                            
                            // sort_noを出力させる為に設定
                            $next_item = $next_key_and_value[1];
                            $next_item['_sort_inno'] = $item['_sort_no']+$_;
                            
                            $class->setComposeItem( new $className($next_item) );
                            $sectionIterator->next();
                        }else{
                            break;
                        }
                    }
                    
                    $class->render();
                }
            }
        }
    }
}

/**
 * Interface ITemplate
 * @package lacne\service\builder
 */
interface ITemplate
{
    public function __construct($data);

    /**
     * @return mixed
     */
    public static function getKey();

    /**
     * @return mixed
     */
    public static function getKlass();
}

abstract class _ABTemplate extends Entity implements ITemplate
{
    protected $composeItem = array();

    /**
     * @return string
     */
    public static function getKlass(){ return get_class(); }

    public function setComposeItem($composeItem)
    {
        array_push($this->composeItem, $composeItem);
    }

    public function render()
    {
        $t = new LACNE_TEMPLATE( dirname(__FILE__) . "/html_resource" );
        echo( $t->render( $this->getKey(), array("entity" => $this, "composeItem" => $this->composeItem), false, false) );
    }
}

class _Header1 extends _ABTemplate
{
    public static function getKey(){ return "head1"; }

    public static function getKlass(){ return get_class(); }
}

class _Header2 extends _ABTemplate
{
    public static function getKey(){ return "head2"; }

    public static function getKlass(){ return get_class(); }
}

class _Header3 extends _ABTemplate
{
    public static function getKey(){ return "head3"; }

    public static function getKlass(){ return get_class(); }
}


class _IMG1 extends _ABTemplate
{
    public static function getKey(){ return "img1"; }

    public static function getKlass(){ return get_class(); }

    /**
     * リサイズされた画像が存在する場合は
     * リサイズ画像を取得
     * @return boolean
     */
    public function getImg()
    {
        if(!empty($this->img)) {
            $path_parts = pathinfo($this->img);
            if(!empty($path_parts)){
                $filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
                $fullFilename = LACNE_DIR . "/.." . $filename;

                if(file_exists($fullFilename))
                {
                    return  $filename;
                }
            }
            return $this->img;
        }
    }

}

class _IMG2 extends _ABTemplate
{
    public static function getKey(){ return "img2"; }

    public static function getKlass(){ return get_class(); }

    /**
     * リサイズされた画像が存在する場合は
     * リサイズ画像を取得
     * @return boolean
     */
    public function getImg()
    {
        if(!empty($this->img)) {
            $path_parts = pathinfo($this->img);
            if(!empty($path_parts)){
                $filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
                $fullFilename = LACNE_DIR . "/.." . $filename;

                if(file_exists($fullFilename))
                {
                    return  $filename;
                }
            }
            return $this->img;
        }
    }
    public function getImg2()
    {
    	if(!empty($this->img2)) {
    		$path_parts = pathinfo($this->img2);
    		if(!empty($path_parts)){
    			$filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
    			$fullFilename = LACNE_DIR . "/.." . $filename;
    
    			if(file_exists($fullFilename))
    			{
    				return  $filename;
    			}
    		}
    		return $this->img2;
    	}
    }
}

class _IMG3 extends _ABTemplate
{
	public static function getKey(){ return "img3"; }

	public static function getKlass(){ return get_class(); }

	/**
	 * リサイズされた画像が存在する場合は
	 * リサイズ画像を取得
	 * @return boolean
	 */
	public function getImg()
	{
		if(!empty($this->img)) {
			$path_parts = pathinfo($this->img);
			if(!empty($path_parts)){
				$filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
				$fullFilename = LACNE_DIR . "/.." . $filename;

				if(file_exists($fullFilename))
				{
					return  $filename;
				}
			}
			return $this->img;
		}
	}
	public function getImg2()
	{
		if(!empty($this->img2)) {
			$path_parts = pathinfo($this->img2);
			if(!empty($path_parts)){
				$filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
				$fullFilename = LACNE_DIR . "/.." . $filename;

				if(file_exists($fullFilename))
				{
					return  $filename;
				}
			}
			return $this->img2;
		}
	}
	public function getImg3()
	{
		if(!empty($this->img3)) {
			$path_parts = pathinfo($this->img3);
			if(!empty($path_parts)){
				$filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
				$fullFilename = LACNE_DIR . "/.." . $filename;
	
				if(file_exists($fullFilename))
				{
					return  $filename;
				}
			}
			return $this->img3;
		}
	}
}

class _IMG4 extends _ABTemplate
{
	public static function getKey(){ return "img4"; }

	public static function getKlass(){ return get_class(); }

	/**
	 * リサイズされた画像が存在する場合は
	 * リサイズ画像を取得
	 * @return boolean
	 */
	public function getImg()
	{
		if(!empty($this->img)) {
			$path_parts = pathinfo($this->img);
			if(!empty($path_parts)){
				$filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
				$fullFilename = LACNE_DIR . "/.." . $filename;

				if(file_exists($fullFilename))
				{
					return  $filename;
				}
			}
			return $this->img;
		}
	}
}

class _IMG5 extends _ABTemplate
{
	public static function getKey(){ return "img5"; }

	public static function getKlass(){ return get_class(); }

	/**
	 * リサイズされた画像が存在する場合は
	 * リサイズ画像を取得
	 * @return boolean
	 */
	public function getImg()
	{
		if(!empty($this->img)) {
			$path_parts = pathinfo($this->img);
			if(!empty($path_parts)){
				$filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
				$fullFilename = LACNE_DIR . "/.." . $filename;

				if(file_exists($fullFilename))
				{
					return  $filename;
				}
			}
			return $this->img;
		}
	}
}

class _IMG6 extends _ABTemplate
{
    public static function getKey(){ return "img6"; }
    
    public static function getKlass(){ return get_class(); }
    
    /**
     * リサイズされた画像が存在する場合は
     * リサイズ画像を取得
     * @return boolean
     */
    public function getImg()
    {
        if(!empty($this->img)) {
            $path_parts = pathinfo($this->img);
            if(!empty($path_parts)){
                $filename = $path_parts['dirname']."/".$path_parts['filename']."_dtil.".$path_parts['extension'];
                $fullFilename = LACNE_DIR . "/.." . $filename;
                
                if(file_exists($fullFilename))
                {
                    return  $filename;
                }
            }
            return $this->img;
        }
    }
}

class _Text1 extends _ABTemplate
{
    public static function getKey(){ return "text1"; }

    public static function getKlass(){ return get_class(); }
}
class _Text11 extends _ABTemplate
{
    public static function getKey(){ return "text11"; }
    
    public static function getKlass(){ return get_class(); }
}
class _Text12 extends _ABTemplate
{
    public static function getKey(){ return "text12"; }
    
    public static function getKlass(){ return get_class(); }
}

class _Text2 extends _ABTemplate
{
    public static function getKey(){ return "text2"; }

    public static function getKlass(){ return get_class(); }
}


class _Text3 extends _ABTemplate
{
    public static function getKey(){ return "text3"; }

    public static function getKlass(){ return get_class(); }
}

class _Text4 extends _ABTemplate
{
    public static function getKey(){ return "text4"; }

    public static function getKlass(){ return get_class(); }
}

class _Text5 extends _ABTemplate
{
    public static function getKey(){ return "text5"; }

    public static function getKlass(){ return get_class(); }
}

class _Text6 extends _ABTemplate
{
    public static function getKey(){ return "text6"; }

    public static function getKlass(){ return get_class(); }

    /**
     * 画像の登録があるかどうか確認する
     * @return boolean
     */
    public function hasImg()
    {
        return !empty($this->img) ? true : false;
    }

    /**
     * 画像表示用のclass名を返す
     * @return boolean
     */
    public function getImgClass()
    {
        return $this->hasImg() == true ? "box-content--split" : "";
    }

}

class _Text7 extends _ABTemplate
{
    public static function getKey(){ return "text7"; }

    public static function getKlass(){ return get_class(); }
}

class _Text8 extends _ABTemplate
{
    public static function getKey(){ return "text8"; }

    public static function getKlass(){ return get_class(); }
}

class _Text9 extends _ABTemplate
{
    public static function getKey(){ return "text9"; }

    public static function getKlass(){ return get_class(); }

    /**
     * @return string snsの種類(class名)
     */
    public function getSnsClassLabel()
    {
        return mb_strtolower( $this->sns );
    }
}

class _Text10 extends _ABTemplate
{
    public static function getKey(){ return "text10"; }
    
    public static function getKlass(){ return get_class(); }
}

class _Cv1 extends _ABTemplate
{
    public static function getKey(){ return "cv1"; }

    public static function getKlass(){ return get_class(); }

    /**
     * テキストの登録があるかどうか確認する
     * @return boolean
     */
    public function hasText()
    {
        return !empty($this->text_name) ? true : false;
    }

    /**
     * テキスト（枠内）の登録があるかどうか確認する
     * @return boolean
     */
    public function hasTextInside()
    {
        return !empty($this->text_inside_name) ? true : false;
    }

}

class _Cv2 extends _ABTemplate
{
    public static function getKey(){ return "cv2"; }

    public static function getKlass(){ return get_class(); }

    /**
     * テキストの登録があるかどうか確認する
     * @return boolean
     */
    public function hasText()
    {
        return !empty($this->text_name) ? true : false;
    }

    /**
     * 2枚目のバナー画像の登録があるかどうか確認する
     * @return boolean
     */
    public function hasImg2()
    {
        return !empty($this->img_name2) ? true : false;
    }

    /**
     * 2枚目のバナー画像リンク先の登録があるかどうか確認する
     * @return boolean
     */
    public function hasLink2()
    {
        return !empty($this->link_name2) ? true : false;
    }

}

class _Profile1 extends _ABTemplate
{
    public static function getKey(){ return "profile1"; }

    public static function getKlass(){ return get_class(); }

    /**
     * プロフィール画像の登録があるかどうか確認する
     * @return boolean
     */
    public function hasProfileImg()
    {
        return !empty($this->profile_img_name) ? true : false;
    }

    /**
     * 組織・役職の登録があるかどうか確認する
     * @return boolean
     */
    public function hasSoshiki()
    {
        return !empty($this->soshiki_name) ? true : false;
    }

    /**
     * 紹介文の登録があるかどうか確認する
     * @return boolean
     */
    public function hasIntro()
    {
        return !empty($this->intoro_name) ? true : false;
    }

    /**
     * ブログリンク先文言の登録があるかどうか確認する
     * @return boolean
     */
    public function hasBlogLinkTitle()
    {
        return !empty($this->blog_link_title_name) ? true : false;
    }

    /**
     * ブログリンク先の登録があるかどうか確認する
     * @return boolean
     */
    public function hasBlogLink()
    {
        return !empty($this->blog_link_name) ? true : false;
    }

}

class _Profile2 extends _ABTemplate
{
    public static function getKey(){ return "profile2"; }

    public static function getKlass(){ return get_class(); }

    /**
     * 組織・役職の登録があるかどうか確認する
     * @return boolean
     */
    public function hasSoshiki()
    {
        return !empty($this->soshiki_name) ? true : false;
    }

    /**
     * リサイズされた画像が存在する場合は
     * リサイズ画像を取得
     * @return boolean
     */
    public function getImg()
    {
        if(!empty($this->img_name)) {
            $path_parts = pathinfo($this->img_name);
            if(!empty($path_parts)){
                $filename = $path_parts['dirname']."/".$path_parts['filename']."_thumb.".$path_parts['extension'];
                $fullFilename = LACNE_DIR . "/.." . $filename;

                if(file_exists($fullFilename))
                {
                    return  $filename;
                }
            }
            return $this->img_name;
        }
    }



}