<?php namespace lacne\service\command;

/**
 * コマンド処理失敗例外
 * Class CommandFailException
 * @package lacne\service\command
 */
class CommandFailException extends \Exception
{
    public function __construct($message, $code, \Exception $previous)
    {
        parent::__construct($message, $code, $previous);
    }
}