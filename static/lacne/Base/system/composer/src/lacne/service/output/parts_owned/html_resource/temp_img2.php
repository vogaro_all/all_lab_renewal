<!--　画像 2カラム -->
            <div class="sec-images">
              <div class="sec-images__column">
                <div class="figure figure--small">
                  <div class="figure__image" style="background-image: url('<?php echo( $entity->getImg() ); ?>');"></div>
                  <p class="figure__caption"><?php echo( $entity->caption_name ); ?></p>
                </div>
                <div class="figure figure--small">
                  <div class="figure__image" style="background-image: url('<?php echo( $entity->getImg2() ); ?>');"></div>
                  <p class="figure__caption"><?php echo( $entity->caption2_name ); ?></p>
                </div>
              </div>
            </div>
<!-- end -->