<?php namespace lacne\service\output;

use lacne\domain\OwnedMedia\MediaEntity;
use lacne\service\output\OwnedMediaHelper;
use lacne\domain\OwnedMedia\specification\OwnedMediaShowConditionSpec;

/**
 * 記事情報のトップ・一覧それぞれ掲載するために扱うAPIクラス
 * Class OwnedMediaAPI
 * @package lacne\service\output
 */
class OwnedMediaTopAPI extends OwnedMediaBaseAPI
{

    /**
     * トップページの情報を取得する
     * @return array|string
     */
    public function loadTopArticleListMain()
    {
    	$spec = new OwnedMediaShowConditionSpec();
    	$notIds = $spec
//     	->setNotInId( $this->loadTopArticleListPickup() )
    	->getNotIds();

    	return $this->lists( array('num'=>10), $this->getSearchParam( array(), $notIds ) );
    }
    
    /**
     * トップページのピックアップ情報を取得する
     * @return array|string
     */
    public function loadTopArticleListPickup()
    {
    	$spec = new OwnedMediaShowConditionSpec();
    	$notIds = $spec
    	->getNotIds();
    
    	return $this->lists( $this->getParam($limit = 5), $this->getSearchParam( array("post.pickup = " => 1), $notIds ) );
    }
    
}