<?php 
if (isset($_GET['category']) && is_numeric($_GET['category'])) {
	$add_pager_link = '&category='.$_GET['category'];
}

if (isset($_GET['key']) && !empty($_GET['key'])) {
	if (is_array($_GET['key'])) {
		foreach ($_GET['key'] as $key => $value) {
			$add_pager_link .= '&key%5B%5D='.$value;
		}
	} else {
		$add_pager_link .= '&key='.$_GET['key'];
	}
}

if (isset($_GET['keywords']) && !empty($_GET['keywords'])) {
    if (is_array($_GET['keywords'])) {
        foreach ($_GET['keywords'] as $keywords => $value) {
            $add_pager_link .= '&keywords%5B%5D='.$value;
        }
    } else {
        $add_pager_link .= '&keywords='.$_GET['keywords'];
    }
}

?>
<?php if( $pager->hasPages() == true ) : ?>

<div class="c-pager">
		<?php $pager->onePrev(function($pager, $prevID, $prevTxt){ ?>
		<p class="c-pager__prev"><a href="?page=<?php echo($prevID); ?>"><span class="u-sr-only">前へ</span></a></p>
		<?php }, function($pager){ ?>
		<p class="c-pager__prev"><a><span class="u-sr-only">前へ</span></a></p>
		<?php }
	,'',''); ?>
	
	<ul class="c-pager__pagination">
	<?php $pager->getPages(function($i, $active, $pager, $isPrevEllipsis, $isNextEllipsis, $add_pager_link){ ?>
	
			<?php if($isNextEllipsis && $i == $pager->getLastPage()): ?>
			<li class="ellipsis">...</li>
			<?php endif; ?>

			<?php if( $active == $i ): ?>
			<li class="page"><a><?php echo($i);?></a></li>
			<?php else: ?>
			<li class="page"><a href="?page=<?php echo($i.$add_pager_link); ?>"><?php echo($i); ?></a></li>
			<?php endif; ?>

			<?php if($isPrevEllipsis && $i == 1): ?>
			<li class="ellipsis">...</li>
			<?php endif; ?>

	<?php }, 3, $add_pager_link); ?>
	</ul>
	
	<?php $pager->oneNext(function($pager, $nextID, $nextTxt){ ?>
	<p class="c-pager__next"><a href="?page=<?php echo($nextID); ?>"><span class="u-sr-only">次へ</span></a></p>
	<?php },function($pager){ ?>
	<p class="c-pager__next"><a><span class="u-sr-only">次へ</span></a></p>
	<?php }, '', ''); ?>
	
</div>

<?php endif; ?>
