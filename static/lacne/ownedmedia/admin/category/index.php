<?php
use lacne\core\model\Category;
use lacne\core\model\Post;

return function($request, $response, $service, $app) {

    // ------------------------------------------------------------------------
    // セットアップ
    // ------------------------------------------------------------------------
    $app->lacne->load_library(array('login', 'category')); //library load
//     $app->lacne->session->sessionStart(); //Session start
//     $login_id = $app->lacne->library["login"]->IsSuccess(true, "manage_category"); //認証チェック
    $login_id = $app->login_id;
    
    //Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにcategory以下にあるため）
    $app->lacne->template->setViewDir($app->lacne->template->getViewDir() . "/category");
    //render_data : Template側に渡すデータ変数
    $render_data = array(
        "login_id" => $login_id
    );

    // ------------------------------------------------------------------------
    // ここから削除の処理
    // ------------------------------------------------------------------------
    if ($app->lacne->post('/delete')) {
        //CSRF TOKENチェック
        $csrf_check = false;
        if (isset($_POST["token"]) && $_POST["token"]) {
            $csrf_check = $app->lacne->request->csrf_check($_POST["token"]);
        }

        //送信されてきたパラメータ取得(カテゴリ1は削除不可)
        if ($csrf_check && isset($_POST["id"]) && is_numeric($_POST["id"]) && $_POST["id"] != 1) {
            $tid = $_POST["id"];
            with(new Category())->delete($tid);

            //そのカテゴリに属する記事があれば、その記事のカテゴリを1にする
            if (with(new Post())->cnt("category = ?", array($tid))) {
                with(new Post())->post_category_change($tid, 1);
            }

            echo "1";
            exit;
        }
        echo "0";
        exit;
    }

	// ------------------------------------------------------------------------
	// カテゴリ名問い合わせ（削除時にajaxで問い合わせされる）
	// ------------------------------------------------------------------------
	if(isset($_GET["cat_name"]) && is_numeric($_GET["cat_name"]))
	{
		$return = "";
	    if(isset($_GET["cat_name"]) && is_numeric($_GET["cat_name"]))
	    {
	        $result = with(new Category())->fetchOne($_GET["cat_name"]);
	        if($result && isset($result["category_name"]))
	        {
	            $return = $result["category_name"];
	        }
	    }

	    echo $return;
	    exit;
	}


    //登録されているカテゴリデータを取得
	$category_list_data = with(new Category())->fetchAll();

    $category_data_cnt = 0;
    if ($category_list_data) $category_data_cnt = count($category_list_data);

    //記事データがカテゴリごとに何件あるか
    $cnt_category_by = with(new Post())->getnum_category_by();

    $render_data = array_merge($render_data, array(
        "category_list_cnt" => $category_data_cnt,
        "category_list" => $category_list_data,
        "cnt_category_by" => $cnt_category_by,
        //CSRF TOKEN
        "csrf_token" => $app->lacne->request->csrf_token_generate()
    ));


    return $app->lacne->render("index", $render_data, true);
};
