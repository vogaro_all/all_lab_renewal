<?php
use lacne\core\model\Category;
return function($request, $response, $service, $app) {
    $app->lacne->load_library(array('login', 'category', 'validation')); //library load
//     $app->lacne->session->sessionStart(); //Session start
//     $login_id = $app->lacne->library["login"]->IsSuccess(true, "manage_category"); //認証チェック
    $login_id = $app->login_id;

    //Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにcategory以下にあるため）
    $app->lacne->template->setViewDir($app->lacne->template->getViewDir() . "/category");
    //render_data : Template側に渡すデータ変数
    $render_data = array(
        "login_id" => $login_id,
        //CSRF TOKEN
        "csrf_token" => $app->lacne->request->csrf_token_generate()
    );

    // ------------------------------------------------------------------------
    // ここから確認画面の処理
    // ------------------------------------------------------------------------
    if ($app->lacne->action("/check")) {

        //送信されてきたパラメータ取得
        $data_list = fn_get_form_param($_POST);

        //入力チェック
        //詳細ページ対応非対応タイプであれば、linkが必須
        $err_check_arr = array(
            "category_name" => array("name" => "カテゴリ名", "type" => array("null", "len"), "length" => 255),
        );
        //バリデーション実行
        $err = $app->lacne->library["validation"]->check($data_list, $err_check_arr);

        //CSRF TOKENチェック
        $csrf_check = false;
        if (isset($data_list["token"]) && $data_list["token"]) {
            $csrf_check = $app->lacne->request->csrf_check($data_list["token"]);
        }
        if (!$csrf_check) $err["csrf_error"] = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";

        $render_data = array_merge($render_data, array(
            "data_list" => $data_list,
            "hidden_data" => serialize_base64_encode($data_list)
        ));

        if (!$err) {
            //確認画面 Render
            $app->lacne->render("confirm", $render_data);
        } else {
            $render_data["err"] = $err;
            //エラー画面Render
            $app->lacne->render("regist", $render_data);
        }
    }

    // ------------------------------------------------------------------------
    // DB登録の処理
    // ------------------------------------------------------------------------
    if ($app->lacne->post("/complete")) {

        //送信されてきたパラメータ取得
        $param = fn_get_form_param($_POST);
        //CSRF TOKENチェック
        $csrf_check = false;
        if (isset($param["token"]) && $param["token"]) {
            $csrf_check = $app->lacne->request->csrf_check($param["token"]);
        }

        if ($csrf_check) {
            //デコード
            //Todo シリアライズ/デコード処理まわりをきちんとクラス化してまとめたい
            //Requestあたり？
            $data_list = unserialize_base64_decode($param["hidden"]);

            //編集モードならば渡されたIDをセット
            if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
                if (with(new Category())->cnt("id =?", $_GET["id"])) {
                    $data_list["id"] = $_GET["id"];
                    $data_list["modified"] = fn_get_date();
                }
            } //新規登録モードならば、ID以外をセット
            else {
                $data_list["sort_no"] = 0;
                $data_list["created"] = fn_get_date();
                $data_list["modified"] = fn_get_date();
            }

            //unset data
            if (isset($data_list["token"])) unset($data_list["token"]);

            //DB保存
            with(new Category())->replace($data_list, "id");

            //完了画面へ
            fn_redirect(LACNE_APP_ADMIN_PATH . "/category/register.php?action=comp");

        } else {
            $render_data["err"] = array(
                "csrf_error" => "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。"
            );
            //エラー画面Render
            $app->lacne->render("regist", $render_data);
        }
    }

    // ------------------------------------------------------------------------
    // 完了画面描画
    // ------------------------------------------------------------------------
    if ($app->lacne->action("/comp")) {

        //Render
        $app->lacne->render("complete", $render_data);

    }

    // ------------------------------------------------------------------------
    // ここから最初の画面の処理
    // ------------------------------------------------------------------------
    if ($app->lacne->action("/")) {

        //確認画面からの修正もどり
        if ($app->lacne->post("/back") && $app->lacne->post("/hidden")) {
            $param = fn_get_form_param($_POST);

            //CSRF TOKENチェック
            $csrf_check = false;
            if (isset($param["token"]) && $param["token"]) {
                $csrf_check = $app->lacne->request->csrf_check($param["token"]);
            }
            if (!$csrf_check) {
                $render_data["err"]["csrf_error"] = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
                $data_list = array();
            } else {
                //デコード
                $data_list = unserialize_base64_decode($param["hidden"]);
            }

            //ID指定がある編集モードの場合
        } else if (isset($_GET["id"]) && is_numeric($_GET["id"])) {
            $tid = $_GET["id"];
            $data_list = with(new Category())->fetchOne($tid);
        }

        $render_data["data_list"] = $data_list;
        //Render
        return $app->lacne->render("regist", $render_data, true);

    }
};