<?php
use lacne\core\model\Tag;
use lacne\core\model\Post;
use lacne\core\model\PostTag;
return function($request, $response, $service, $app) {

	$app->lacne->load_library(array('login' , 'tag')); //library load
	$app->lacne->session->sessionStart(); //Session start
	$login_id = $app->lacne->library["login"]->IsSuccess(true , "manage_tag"); //認証チェック

	//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにtag以下にあるため）
	$app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/tag");
	//render_data : Template側に渡すデータ変数
	$render_data = array(
	    "login_id" => $login_id
	);

	// ------------------------------------------------------------------------
	// ここから削除の処理
	// ------------------------------------------------------------------------
	if($app->lacne->post('/delete'))
	{
	    //CSRF TOKENチェック
	    $csrf_check = false;
	    if(isset($_POST["token"]) && $_POST["token"])
	    {
	        $csrf_check = $app->lacne->request->csrf_check($_POST["token"]);
	    }

	    //送信されてきたパラメータ取得(カテゴリ1は削除不可)
	    if($csrf_check && isset($_POST["id"]) && is_numeric($_POST["id"]))
	    {
	        $tid = $_POST["id"];
	        with(new Tag())->delete($tid);
	        with(new PostTag())->delete($tid);

	        echo "1";
	        exit;
	    }
	    echo "0";
	    exit;
	}

	// ------------------------------------------------------------------------
	// カテゴリ名問い合わせ（削除時にajaxで問い合わせされる）
	// ------------------------------------------------------------------------
	if(isset($_GET["cat_name"]) && is_numeric($_GET["cat_name"]))
	{
		$return = "";
	    if(isset($_GET["cat_name"]) && is_numeric($_GET["cat_name"]))
	    {
	        $result = with(new Tag())->fetchOne($_GET["cat_name"]);
	        if($result && isset($result["tag_name"]))
	        {
	            $return = $result["tag_name"];
	        }
	    }

	    echo $return;
	    exit;
	}


	//登録されているカテゴリデータを取得
	$tag_list_data = with(new Tag())->fetchAll();
	$tag_data_cnt = 0;
	if($tag_list_data) $tag_data_cnt = count($tag_list_data);

	//記事データがカテゴリごとに何件あるか
	$cnt_tag_by = with(new Post())->getnum_tag_by();

	$render_data = array_merge($render_data , array(
	    "tag_list_cnt" => $tag_data_cnt,
	    "tag_list" => $tag_list_data,
	    "cnt_tag_by" => $cnt_tag_by,
	    //CSRF TOKEN
	    "csrf_token" => $app->lacne->request->csrf_token_generate()
	));


	//Render
	return $app->lacne->render("index" , $render_data, true);
};