<?php

use lacne\core\model\Post;
use lacne\core\model\Parts;
use lacne\core\model\Tag;
// use lacne\core\model\Doctor;

return function($request, $response, $service, $app) {
	global $_OWNEDMEDIA_PARTS_CONFIG;
// 	global $_RECOMMEND_CNT;

	// ------------------------------------------------------------------------
	// セットアップ
	// ------------------------------------------------------------------------
    $app->lacne->load_library(array('login', 'post', 'media', 'validation')); //library load
    $app->lacne->session->sessionStart(); //Session start
    $login_id = $app->lacne->library["login"]->IsSuccess(); //認証チェック


	
	$render_data = array_merge($render_data ,array(

	    "post_authority"		=> $post_authority,
	    "data_list"				=> $data_list,
	    "category_list" 		=> $app->lacne->library['post']->get_category_list(), //登録されているカテゴリデータを取得
// 	    "sub_category_list" 		=> $app->lacne->library['post']->get_sub_category_list(), //登録されているカテゴリデータを取得
		"tag_list"				=> $app->lacne->library['post']->get_tag_list(), //登録されているタグデータを取得
	    "csrf_token" 			=> $app->lacne->request->csrf_token_generate(),
	));

	//Render
	return $app->lacne->render("edit" , $render_data, true);


};