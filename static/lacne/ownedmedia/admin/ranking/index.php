<?php
use lacne\core\model\Media;
use lacne\core\model\Ranking;

return function($request, $response, $service, $app) {
	$app->lacne->load_library(array('login' , 'ranking', 'gapi')); //library load
	$app->lacne->session->sessionStart(); //Session start
	$login_id = $app->lacne->library["login"]->IsSuccess(); //認証チェック

	//Templateファイルのフォルダ指定(通常読み込むtemplateフォルダの中のさらにmedia以下にあるため）
	$app->lacne->template->setViewDir($app->lacne->template->getViewDir()."/ranking");
	//render_data : Template側に渡すデータ変数
	$render_data = array(
	    "login_id" => $login_id
	);

	// ------------------------------------------------------------------------
	// 登録されているデータをCSV形式でダウンロード
	// ------------------------------------------------------------------------
	if($app->lacne->post('/csv')){
		//送信されてきたPOSTデータ取得
		$param = fn_get_form_param($_POST);
		
		//ランキングデータの取得
		$filename = 'rankingCSV_'.date('YmdHis');
		$ranking_data = $app->lacne->library['ranking']->get_data();
		$result = $app->lacne->library['ranking']->download_csv($filename, $ranking_data);
		if(!$result){
			$err = $result;
		}

		//CSRF TOKENチェック
		if(!$err)
		{
			$csrf_check = false;
			if(isset($param["token"]) && $param["token"])
			{
				$csrf_check = $app->lacne->request->csrf_check($param["token"]);
			}
			if(!$csrf_check) $err["csrf_check"] = "データの受け渡しで問題が発生しました。もう一度操作をやり直して下さい。";
		}
	}

	// ------------------------------------------------------------------------
	// Googleアナリティクスアクセスデータ取得
	// ------------------------------------------------------------------------
	// if($app->lacne->post('/get')){
	// 	//送信されてきたPOSTデータ取得
	// 	$param = fn_get_form_param($_POST);
	// 	$pv_data = $app->lacne->library["pickup"]->get_pv_pickup();
	// 	if(!empty($pv_data)){
	// 		$insert = $app->lacne->library["pickup"]->insert_ga_pickup_data($pv_data);
	// 		if($insert){
	// 			$render_data["get_message"] = 'Googleアナリティクスから取得したデータを反映しました。';
	// 		}else{
	// 			$render_data["get_err"] = '取得データの登録に失敗しました。';
	// 			$render_data["get_message"] = '取得データの登録に失敗しました。';
	// 		}
	// 	}else{
	// 		$render_data["get_err"] = '取得データがありません。';
	// 		$render_data["get_message"] = '取得データがありません。';
	// 	}
	// }

	// ------------------------------------------------------------------------
	// ここからアップロードの処理
	// ------------------------------------------------------------------------
	if($app->lacne->post('/upload')){

	    //送信されてきたPOSTデータ取得
	    $param = fn_get_form_param($_POST);

	    $file_num = 1;
	    $message = "";
	    $errors = array();

	    if(isset($param['file']) && isset($param['filename'])){
	        $data_list['file'] = $param['file'];
	        $data_list['filename'] = $param['filename'];
	    }

		//アップロードされたメディアファイル取得
		for($i=0; $i<$file_num; $i++)
	    {
	    	//アップロードされたメディアファイル取得
	        $media_file = array();
	        if (isset($param['file']) && isset($param['file'][$i]) && preg_match('/^data:(.*?);base64,(.*)/', $param['file'][$i], $matches)) {
	            $binary = base64_decode(strtr($matches[2], '-_,', '+/='));
	            $media_file = array(
	                'size' => strlen($binary),
	                'name' => $data_list["filename"][$i],
	                'binary' => $binary
	            );

	        } elseif(!empty($_FILES["file"]) && is_array($_FILES["file"]) ) {
	            foreach($_FILES["file"] as $key=>$file){
	                $media_file[$key] = $file[$i];
	            }
	        }

			$result = $app->lacne->library["ranking"]->upload_process(array(
				'post' => $param,
				'media_file' => $media_file,
				'csrf_check' => true
			), false);

			if(!empty($result['status']) && $result['status'] == 'success' && !empty($result['message']))
			{
				//upload OK
				$message = $result['message'];
			}
			else if(!empty($result['status']) && ($result['status'] == 'error'))
			{
				$errors[$i] = $result['err'];
			}
			else
			{
				$errors[$i] = 'アップロード時にエラーが発生しました。';
			}
		}

		if(!$errors){
			$render_data["message"] = $message;
		}
		else
		{
			$render_data["err"] = $errors;
			$render_data["message"] = $errors;
		}
	}

	$render_data = array_merge($render_data , array(
		"upload_href" => "index.php",
		"csrf_token" => $app->lacne->request->csrf_token_generate()
	));

	//Render
	return $app->lacne->render("index" , $render_data, true);
}
?>