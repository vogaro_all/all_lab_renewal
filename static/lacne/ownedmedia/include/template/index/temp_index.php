<?php

    $page_setting = array(
        "title" => "インデックス",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/index.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .main');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index");
?>

<section class="section pc">
<h1 class="head-line01"><?=LACNE_APP_ADMIN_PAGENAME?> インデックス</h1>
<p class="load">メニューから目的の操作を選択してください。</p>
<nav class="menu">
<?php
include_once(DIR_CONFIG.'admin_menu.php');
$app_dirname_0 = !empty($APP_DIRNAME)?str_replace("/","",$APP_DIRNAME):"";
if(!empty($app_dirname_0) && !empty($_SETTING_ADMIN_MENU[$app_dirname_0]["submenu"]) && is_array($_SETTING_ADMIN_MENU[$app_dirname_0]["submenu"])) :
    foreach($_SETTING_ADMIN_MENU[$app_dirname_0]["submenu"] as $submenu_data) :
		 if( $submenu_data["icon"] != "main" && (empty($submenu_data["terms"]) || (!empty($submenu_data["terms"]) && $LACNE->library["login"]->IsSuccess(false , $submenu_data["terms"]))) && 
                  (empty($submenu_data["option"]) || (!empty($submenu_data["option"]) && class_exists($submenu_data["option"])))) :
	
?>
<figure class="group-rover">
<a href="<?=$submenu_data["link"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/index/<?=(!empty($_SETTING_INDEXMENU_IMAGES[$submenu_data["icon"]]))?$_SETTING_INDEXMENU_IMAGES[$submenu_data["icon"]]:"main_menu_img06.gif"?>" alt="<?=(!empty($submenu_data["comment"]) && $submenu_data["comment"])?sprintf($submenu_data["comment"] , KEYWORD_KIJI):""?>" class="rover tip" /></a>
<figcaption><a href="<?=$submenu_data["link"]?>" class="tip"><?=$submenu_data["label"]?></a></figcaption>
</figure>
<?php
		endif;
	endforeach;
endif;	
?>
<!-- .menu // --></nav>
<!-- .section // --></section>

<section class="section">
<p class="name smp"><noscript>JavaScriptを有効にしてくだい。</noscript></p>

<?php
//承認待ちデータの案内（承認フローオプションが有効の場合のみ表示）
if(method_exists($LACNE->model["post"], "data_cnt_waiting") && isset($cnt_wait) && $cnt_wait) :
?>
<div class="alert note pie mb30"><span class="icon">情報</span><p class="fl">承認待ちの<?=KEYWORD_KIJI?>が<a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php?wait=1"><?=$cnt_wait?>件</a>あります。</p><p class="fr pc"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php?wait=1">承認待ち一覧</a></p></div>
<?php endif; ?>

<!-- .section --></section>

<article class="section">
<div class="list-haed pie">
<h2>最新の<?=KEYWORD_KIJI?></h2>
<p class="pc"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php"><?=KEYWORD_KIJI?>一覧</a></p>
<p class="smp"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php">一覧</a></p>
<!-- .table-haed // --></div>
<ul class="list-type01 pie">
<?php
if(isset($data_list) && count($data_list)) :
    foreach($data_list as $post) :
?>
<li><a href="<?=LACNE_APP_ADMIN_PATH?>/article/edit.php?id=<?=$post["id"]?>" class="location"><?=$post["title"]?></a><time datetime="<?=fn_dateFormat($post["output_date"])?>"><?=fn_dateFormat($post["output_date"])?></time><span><?=$post["category_name"]?></span></li>
<?php
    endforeach;
else:
?>
<li class="no-data">登録がありません。</li>
<?php
endif;
?>
</ul>
<!-- .section // --></article>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>