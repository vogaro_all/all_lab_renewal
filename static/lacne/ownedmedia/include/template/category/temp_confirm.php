<?php

$page_setting = array(
    "title" => "カテゴリ管理",
    "js" => array(),
    "css" => array(LACNE_SHAREDATA_PATH."/css/common/global_iframe.css",LACNE_SHAREDATA_PATH."/css/category/confirm.css")
);

//include common header template
include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .category');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "confirm");
?>

<section class="section">
<h1 class="head-line01 smp">カテゴリ登録</h1>
<p class="load smp"></p>
<div class="alert memo pie"><span class="icon">情報</span><p class="fl">下記内容で登録します。よろしければ登録ボタンを押してください。</p></div>
<form action="<?=LACNE_APP_ADMIN_PATH?>/category/register.php<?=fn_set_urlparam($_GET , array('id'))?>" method="POST">
<div class="input">
<p><span class="label">カテゴリ名：</span><?=$data_list["category_name"]?></p>
<p><span class="label">バナーリンクURL：</span><?=$data_list["link_url"]?></p>
<p><span class="label">バナーリンクURL：</span><?php if (!empty($data_list["image"])) { ?><img src="<?php echo($data_list["image"]);?>" width="200px"><?php } ?></p>
<!-- .input // --></div>
<div class="btn">
<p class="btn-type01 pie"><input type="submit" name="complete" value="登録" class="pie" /></p>
<p class="btn-type02 pie"><input type="submit" name="back" value="修正" class="pie" /></p>
<!-- .btn // --></div>
<input type="hidden" name="hidden" value="<?=$hidden_data?>" />
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>