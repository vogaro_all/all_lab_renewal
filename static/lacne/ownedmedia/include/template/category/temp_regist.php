<?php

    $page_setting = array(
        "title" => "カテゴリ管理",
        "js" => array(LACNE_SHAREDATA_PATH."/js/medialist.js",),
        "css" => array(LACNE_SHAREDATA_PATH."/css/article/edit.css",LACNE_SHAREDATA_PATH."/css/common/global_iframe.css",LACNE_SHAREDATA_PATH."/css/category/regist.css")
    );
    
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .category');
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "regist");
?>

<section class="section">
<h1 class="head-line01 smp">カテゴリ登録</h1>
<p class="load smp">カテゴリの登録・編集を行います。</p>
<?php
if(!isset($err) || !$err) :
?>
<div class="alert memo pie pc"><span class="icon">情報</span><p class="fl">下記の項目を入力のうえ、確認ボタンを押してください。</p></div>
<?php
else:
?>
<div class="alert error pie"><span class="icon">情報</span><p class="fl"><?=fn_output_errtxt($err)?></p></div>
<?php
endif;
?>
<form action="<?=LACNE_APP_ADMIN_PATH?>/category/register.php?action=check&<?=fn_set_urlparam($_GET , array('id') , false)?>" method="POST">
<div class="input">
<p><span class="label">カテゴリ名：</span><?=$data_list["category_name"]?><input type="hidden" name="category_name" value="<?php echo (isset($data_list["category_name"]))?$data_list["category_name"]:""?>" /></p>
<p><span class="label">バナーリンクURL：</span><input type="text" name="link_url" maxlength="255" value="<?php echo (isset($data_list["link_url"]))?$data_list["link_url"]:""?>" /></p>

<div class="toggle_show_img_iteml">
<p><span class="label">バナー画像：</span>
<div class="btn img_area">
<p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_1" /></p>
<p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_1" /></p>
</div>
<input type="hidden" name="image" value="<?=(isset($data_list["image"]))?$data_list["image"]:""?>" class="img_hidden_value" id="img_hidden_1" />
<p id="img_prev_1" class="img_preview"></p>
</p>
</div>


<!-- .input // --></div>
<div class="btn" style="width: 380px; margin-bottom: 130px;">
<p class="btn-type01 pie"><input type="submit" name="conf" value="確認" class="pie" /></p>
<p class="btn-type02 pie"><a href="javascript:void(0)"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>