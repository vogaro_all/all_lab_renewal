<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){

    $.library.tooltip({margin_x:-25});
    $.library.css3('#Pc #Main p.toolTip span.text','<?=LACNE_SHAREDATA_PATH?>/');
    $('a[href=#box-regist]').click(function(){$('#box-regist iframe').attr('src','<?=LACNE_APP_ADMIN_PATH?>/category/register.php');});
    $('#Pc #Main .section.list ul li').hover(function(){
            $(this).addClass('highlight');
    },function(){
            $(this).removeClass('highlight');
    });

    //編集ボタン
    $('.list .edit_link').click(function(){
        var tid = $(this).attr("id");
        tid = tid.replace("edit_" , "");
        $('#box-regist iframe').attr('src','<?=LACNE_APP_ADMIN_PATH?>/category/register.php?id='+tid);
        $.library.modalOpen($("<a>").attr("href","#box-regist"),"#Modal");
    });


});



<?php
endif;
?>

// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){

    stripe_list();

    //削除ボタン
    $('.list .del').click(function(){
        var tid = $(this).attr("id");
        tid = tid.replace("del_" , "");
        //idから削除対象とするカテゴリ名をajaxで取得してモーダルアラートに表示させる
        $.ajax({
            type:'GET',
            url : '<?=LACNE_APP_ADMIN_PATH?>/category/index.php',
            data:'cat_name='+tid,
            success : function(result)
            {
                if(result)
                {
                    $('#box-delete a#delete_link').attr('href',tid);
                    $('#delete_cat_name').html(result);
                    $.library.modalOpen($("<a>").attr("href","#box-delete"),"#Modal");
                }
            }
        });
    });
    $('#box-delete a#delete_link').click(function(){
        var tid = $(this).attr("href");
        $.ajax({
            url:'<?=LACNE_APP_ADMIN_PATH?>/category/index.php',
            type:"POST",
            data:"delete=1&id="+$(this).attr("href")+"&token=<?=$csrf_token?>",
            success:function(result)
            {
                $.library.modalClose();
                if(result == "1")
                {
                    $("#cat_"+tid).fadeOut(500, function(){
                        //削除して、一覧表示部分を切り替え
                        $(this).remove();
                        //ストライプを一旦消して再描画させる
                        $('#Main .section.list ul li').removeClass('odd');
                        $('#Main .section.list ul li').removeClass('last');
                        stripe_list();
                    })
                }
            }
        })
        return false;
    });
});

function stripe_list()
{
    //しましま表示
    $('#Main .section.list ul li:nth-child(even)').addClass('odd');
    $('#Main .section.list ul li:last').addClass('last');
}

// -----------------------------------------------
</script>
