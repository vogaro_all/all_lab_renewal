<?php

    $page_setting = array(
        "title" => "KEYWORD管理",
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/category/index.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");

?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .tag');

});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index" , array("csrf_token" =>$csrf_token));
?>

<section class="section">
<!--
タイトル
-->
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a>KEYWORD管理</h1>
<p class="load">KEYWORDの新規登録、削除を行います。</p>
<!-- .section // --></section>

<section class="section edit">
<!--
新規登録エリア
-->
<h2 class="head-line02">KEYWORD新規登録</h2>
<div class="form section-inside">
<p class="btn-type01 pie section-inside pc"><a href="#box-regist" class="modal-open"><span class="pie">新規登録</span></a></p>
<p class="btn-type01 pie section-inside smp"><a href="<?=LACNE_APP_ADMIN_PATH?>/tag/register.php"><span class="pie">新規登録</span></a></p>
<!-- .form // --></div>

<!-- .section // --></section>

<section class="section list">
<!--
一覧エリア
-->
<h2 class="head-line02">登録KEYWORD一覧</h2>
<ul id="cat_list">
<?php
//---------------------------------------------
//KEYWORD一覧表示部分
//---------------------------------------------
if(isset($tag_list) && $tag_list) :
    foreach($tag_list as $tag) :
        $num = 0;
        if(isset($tag["id"]) && isset($cnt_tag_by[$tag["id"]]))
        {
            $num = $cnt_tag_by[$tag["id"]];
        }
?>
<li id="cat_<?=$tag["id"]?>"><span class="name"><?=$tag["tag_name"]?> (<?=$num?>)</span>
    <span class="edit"><a href="javascript:void(0)" class="edit_link" id="edit_<?=$tag["id"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_edit.gif" alt="編集" class="tip" /></a>
    <a href="javascript:void(0)" class="del" id="del_<?=$tag["id"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除" class="tip" /></a>
    </span>
</li>
<?php
    endforeach;
else:
?>
<li class="no-data">登録がありません。</li>
<?php
endif;
?>
</ul>

<!-- .section // --></section>


<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//モーダル画面用
//--------------------------------------------------------
?>
<?=
//登録画面（iframe）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-regist");
?>
<iframe src="<?=LACNE_APP_ADMIN_PATH?>/tag/register.php" width="500" frameborder="0" scrolling="no"></iframe>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//KEYWORD削除（個別削除）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-delete");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl">KEYWORD「<span id="delete_cat_name"></span>」を削除してもよろしいですか？<br />このKEYWORDに属する<?=KEYWORD_KIJI?>は別KEYWORDに移動されます。ご注意下さい。</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="javascript:void(0)" id="delete_link"><span class="pie">削除</span></a></p>
<p class="btn-type02 pie"><a href="javascript:void(0)" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>


<!-- #Modal // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>