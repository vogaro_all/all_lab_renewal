<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $.library.css3('#Pc #Main p.toolTip span.text','<?=LACNE_SHAREDATA_PATH?>/');
    $('figcaption a.tip').hover(function(){
            $('#Main .toolTip .text').text($(this).closest('figure').find('img').attr('alt'));
    },function(){
            $('#Main .toolTip .text').text('');
    });

    $('.tip').powerTip({
        placement:'s',
        fadeInTime:100
    });

    MediaDrop.init({
        'filetype' : <?php echo "['".implode("','", $LACNE->library['ranking']->filetype)."']"; ?>,
        'maxfilesize' : <?php echo $LACNE->library['ranking']->maxfilesize; ?>,
        'maxfilenum'  : 1
    });
});
function template(){
    return "form.pc table.upload-ui tbody tr";
}
function template_delete(){
    return "tr";
}

<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    // アップロードするファイルの削除
    $("a.CancelFile").click(function(){
        if($(this).closest('#image-preview').length){
            $(this).closest(template_delete()).remove();
        }else{
            var file_num = $(template()).size();
            if(1 < file_num){
                $(this).closest(template_delete()).remove();
                $("a#AddFile").show();
            }
        }
        return false;
    });

    //ダウンロード
    $('#get_csv').click(function(){
        $('form[name=get_csv_form]').submit();
        return false;
    });

    //PV数取得モーダル
    $('#btn-get').click(function(){
        $.library.modalOpen($("<a>").attr("href","#box-get"),"#Modal");
        return false;
    });
    $('#run-get').click(function(){
        $('form[name=get_form]').submit();
        return false;
    });

    //タグをクリックすると検索フォームに入力される処理
    $('.taglist a.tag').click(function(){
        $('input[name=search_tag]').val($(this).html());
        return false;
    });


    //Output Error モーダルで表示
    <?php
    if((isset($err) && $err) || (isset($message) && $message) || (isset($get_err) && $get_err) || (isset($get_message) && $get_message)) {
    ?> 
            $.library.do_slideDown_message("#comp_message");
    <?php
    }
    ?>

});
// -----------------------------------------------
</script>
