<?php

    $page_setting = array(
        "title" => "WEEKLYランキング機能",
        "js" => array(
            LACNE_SHAREDATA_PATH."/js/jquery.powertip/jquery.powertip-1.1.0.min.js",
            LACNE_SHAREDATA_PATH."/js/fancybox/jquery.fancybox-1.3.4.js",
            LACNE_SHAREDATA_PATH."/js/mediadrop.js"
        ),
        "css" => array(
            LACNE_SHAREDATA_PATH."/css/media/index.css",
            LACNE_SHAREDATA_PATH."/css/media/drop_upload.css",
            LACNE_SHAREDATA_PATH."/css/common/pager.css",
            LACNE_SHAREDATA_PATH."/js/jquery.powertip/jquery.powertip.css",
            LACNE_SHAREDATA_PATH."/js/fancybox/jquery.fancybox-1.3.4.css",
        )
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .ranking');
    $(".modalinfo").click(function() {
        $.fancybox({
            'title': $('span',this).text(),
            'href' : this.href,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
        return false;
    });
    
    $('.pager a').each(function(){
        var href = $(this).attr("href")+"#mediaList";
        $(this).attr("href",href);
    });
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index" , array("err"=>$err , "message"=>$message , "get_err"=>$get_err , "get_message"=>$get_message , "message_delete"=>$message_delete));
?>


<section class="section">
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a>WEEKLYランキング機能</h1>
<p class="load">WEEKLYランキングの反映・調整を行います。</p>

<?php 
if(isset($get_err) && $get_err) : 
?>
<div class="alert error pie" id="comp_message" style="display:none"><span class="icon">エラー</span><p class="fl">
<span style="display:block;color:#343c47;"><?=$get_message?></span>
</p></div>
<?php 
elseif(isset($get_message) && $get_message) : 
?>
<div class="alert comp pie" id="comp_message" style="display:none"><span class="icon">完了</span><p class="fl"><?=$get_message?></p></div>
<?php 
endif; 
?>


<div class="btn-erea">

<?php
//--------------------------------------------------------
//Googleアナリティクスからデータを取得
//--------------------------------------------------------
?>
<!-- <div class="ga-btn">
<p>過去7日間のPVデータを<br>Googleアナリティクスから取得し反映させます。</p>
<p class="btn-type01 pie section-inside pc"><a href="#box-get" class="modal-open"><span class="pie">データ反映</span></a></p>
<form name="get_form" action="index.php" method="post" style="display: none">
<input type="hidden" name="token" value="<?=$csrf_token?>" />
<input type="hidden" name="get" value="1" />
</form>
</div> -->

<?php
//--------------------------------------------------------
//登録されているデータをCSV形式でダウンロード
//--------------------------------------------------------
?>
<div class="csv-btn">
<p>現在登録されているWEEKLYランキングデータをを<br>CSV形式でダウンロードします。</p>
<p class="btn-type01 pie section-inside pc"><a href="#" id="get_csv"><span class="pie">ダウンロード</span></a></p>
<form name="get_csv_form" action="index.php" method="post" style="display: none">
<input type="hidden" name="token" value="<?=$csrf_token?>" />
<input type="hidden" name="csv" value="1" />
</form>
<!-- .csv-erea // --></div>

<!-- .btn-erea // --></div>
<!-- .section // --></section>


<?php
//--------------------------------------------------------
//アップロード画面部分をインクルード
//--------------------------------------------------------
include_once(dirname(__FILE__)."/temp_common_upload.php");
?>
<!-- .section // --></section>

<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//モーダル画面用
//--------------------------------------------------------
?>

<?=
//登録画面（iframe）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-get");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl">WEEKLYランキングを作成し、即時反映します。</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="#" id="run-get"><span class="pie">反映</span></a></p>
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//エラー表示用
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-error");
?>
<div class="alert error pie"><span class="icon">注意</span><p class="fl" id="error_message"></p></div>
<div class="btn btn-one">
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<!-- #Modal // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>