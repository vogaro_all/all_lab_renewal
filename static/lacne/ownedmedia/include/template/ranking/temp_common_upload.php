<?php
/**
 * アップロード部分のテンプレート
 */
?>

<section class="section">
<h2 class="head-line02" style="margin-bottom:15px">CSVデータのアップロード</h2>
<p class="note">※<strong><?=$LACNE->library["ranking"]->get_maxfilesize()?>キロバイトまで</strong>のファイルをアップロードできます。<br>
※アップロードできるファイルは <strong><?= implode(" , " , $LACNE->library["ranking"]->filetype)?></strong>になります。
</p>

<div class="alert note pie"><span class="icon">注意</span><p class="fl">アップロードをしたデータは即時反映されます。</p></div>

<?php 
//エラーもしくは完了メッセージ
if(isset($err) && $err) : 
$err_num = count($err);
?>
<div class="alert error pie" id="comp_message" style="display:none"><span class="icon">エラー</span><p class="fl">
<?php if(1 < $file_num):?>
<span style="display:block;color:#343c47;"><?=$file_num?>件中<?=$err_num?>件のファイルがアップロードできませんでした。</span>
<?php endif; $idx = 1;
foreach($err as $i=>$e) :
if(1 < $file_num && 0 < $err_num){ echo $idx++ . "件目：";} ?>
<?=$e?><br />
<?php endforeach;?>
</p></div>
<?php 
elseif(isset($message) && $message) : 
?>
<div class="alert comp pie" id="comp_message" style="display:none"><span class="icon">完了</span><p class="fl"><?=$message?></p></div>
<?php 
endif; 
?>

<?php
//権限チェック
if($LACNE->library["login"]->chk_controll_limit("upload_files")) :
?>

<!-- PCフォーム（通常のファイルアップロード -->
<div id="legacy-upload">
<form action="<?=!empty($upload_href)?$upload_href:''?>" method="post" enctype="multipart/form-data" class="section-inside pc">
<table class="upload-ui table-list pc pie">
<thead>
<tr><th>ファイル選択</th><th>ファイル名変更</th><th>タグ・情報</th><th></th></tr>
</thead>
<tbody>
<?php if(!empty($err) && $err):foreach($err as $i=>$e):?>
<tr>
<td><input type="file" name="file[]" value="" /></td>
<td><input type="text" name="rename[]" value="<?=!empty($data_list["rename"][$i])?$data_list["rename"][$i]:''?>" maxlength="100" size="20" /></td>
<td><input type="text" name="tag[]" value="<?=!empty($data_list["tag"][$i])?$data_list["tag"][$i]:''?>" maxlength="100" size="20" /></td>
<td class="upload-cancel"><a href="#" class="CancelFile"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除"></a></td>
</tr>
<?php endforeach; else :?>
<tr>
<td><input type="file" name="file[]" value="" /></td>
<td><input type="text" name="rename[]" value="" maxlength="100" size="20" /></td>
<td><input type="text" name="tag[]" value="" maxlength="100" size="20" /></td>
<td class="upload-cancel"><a href="#" class="CancelFile"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除"></a></td>
</tr>
<?php endif;?>
</tbody>
</table>
<a href="#" id="AddFile">アップロードするファイルを追加</a>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
<p class="btn-type01 pie"><input type="submit" name="upload" value="アップロード" class="pie" /></p>
</form>
<!-- .legacy-upload // --></div>


<!-- PCフォーム（ドラッグドロップのアップロード -->
<div id="drop-upload">
<form action="<?=!empty($upload_href)?$upload_href:''?>" method="post" enctype="multipart/form-data" class="section-inside pc">
<div id="image-drop">ここにファイルをドラッグ<br>またはクリックしてファイルを選択</div>
<table class="upload-ui table-list pc pie" id="image-preview">
<thead>
<tr><th width="200">ファイル選択</th><th width="200">ファイル名</th><th width="30"></th></tr>
</thead>
<tbody>
<?php if(!empty($err) && $err):foreach($err as $i=>$e):if(!empty($data_list["rename"])):?>
<tr>
<td class="image"><input type="hidden" name="file[]" value="<?=!empty($data_list["file"][$i])?$data_list["file"][$i]:''?>"></td>
<td class="filename"><input type="text" name="rename[]" value="<?=!empty($data_list["rename"][$i])?$data_list["rename"][$i]:''?>" maxlength="100" size="20" /><input type="hidden" name="filename[]" value="<?=!empty($data_list["filename"][$i])?$data_list["filename"][$i]:''?>"></td>
<td class="upload-cancel"><a href="#" class="CancelFile"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除"></a></td>
</tr>
<?php endif; endforeach; endif;?>
</tbody>
</table>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
<p class="btn-type01 pie"><input type="submit" name="upload" value="アップロード" class="pie" /></p>
</form>
<table id="image-preview-tpl">
<tr>
<td class="image"><img src="" alt=""><input type="hidden" name="file[]" value=""></td>
<td class="filename"><input type="text" name="rename[]" value="" maxlength="100" size="20"><input type="hidden" name="filename[]" value=""></td>
<td class="upload-cancel"><a href="#" class="CancelFile"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除"></a></td>
</tr>
</table>
<input type="file" name="file[]" multiple="multiple" accept="text/csv" id="image-select">
<!-- .drop-upload // --></div>


<!-- スマホフォーム -->
<form action="<?=!empty($upload_href)?$upload_href:''?>" method="post" enctype="multipart/form-data" class="section-inside smp">
<table class="upload-ui table-list pie">
<tbody>
<?php if(!empty($err) && $err):foreach($err as $i=>$e):?>
<tr>
<th>ファイル選択</th>
<td><input type="file" name="file[]" value="" /></td>
</tr>
<tr>
<th>ファイル名変更</th>
<td><input type="text" name="rename[]" value="<?=!empty($data_list["rename"][$i])?$data_list["rename"][$i]:''?>" maxlength="100" size="20" /></td>
</tr>
<tr>
<th>タグ・情報</th>
<td><input type="text" name="tag[]" value="<?=!empty($data_list["tag"][$i])?$data_list["tag"][$i]:''?>" maxlength="100" size="20" /></td>
<td class="upload-cancel"><a href="#" class="CancelFile"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除"></a></td>
</tr>
<?php endforeach; else :?>
<tr>
<th>ファイル選択</th>
<td><input type="file" name="file[]" value="" /></td>
</tr>
<tr>
<th>ファイル名変更</th>
<td><input type="text" name="rename[]" value="" maxlength="100" size="20" /></td>
</tr>
<tr>
<th>タグ・情報</th>
<td><input type="text" name="tag[]" value="" maxlength="100" size="20" /></td>
<td class="upload-cancel"><a href="#" class="CancelFile"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除"></a></td>
</tr>
<?php endif;?>
</tbody>
</table>
<a href="#" id="AddFile">アップロードするファイルを追加</a>
<input type="hidden" name="token" value="<?=$csrf_token?>" />
<p class="btn-type01 pie"><input type="submit" name="upload" value="アップロード" class="pie" /></p>
</form>

<?php
else:
?>
<p class="note">アップロード権限がありません</p>
<?php
endif;
?>

<!-- .section // --></section>