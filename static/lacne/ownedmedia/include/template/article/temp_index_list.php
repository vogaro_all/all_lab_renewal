<?php
// ------------------------------------------------------------------------
// 
// PC用一覧表示テンプレートここから
// 
// ------------------------------------------------------------------------
?>
<form action="<?=$form_delete_url?>" name="del_form" method="POST">
<table cellpadding="0" cellspacing="0" border="0" summary="<?=KEYWORD_KIJI?>一覧" class="table-list pc pie">
<thead>
<tr>
<td width="3%" class="lt"><input type="checkbox" name="" value="" class="master" /></td>
<th width="16%">公開日時<a href="#" class="sort" id="sort_date"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_sort.png" width="7" height="17" /></a></th>
<th width="16%">カテゴリ<a href="#" class="sort" id="sort_cat"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_sort.png" width="7" height="17" /></a></th>
<th width="40%">タイトル</th>
<?php
//アカウント管理、承認フローのオプションが有効なら
if(isset($option_account) && $option_account) :
?>
<?php endif; ?>
<th width="12%">公開</th>
<th width="13%" class="rt">編集</th>
</tr>
</thead>
<tbody>
	
<?php
// ------------------------------------------------------------------------
// ここからループ
// ※エスケープさせずに埋め込む変数値は$_post_data[$i][keyname]で取り出す
// ------------------------------------------------------------------------
if(isset($post_data) && $post_data) :
    foreach($post_data as $i => $data) :
?>
<tr<?=$_post_data[$i]["sub_data"]?> id="post_<?=$data["id"]?>">
<td>
<?php
//権限チェック(削除権限があるか、もしくは自分自身が作成した記事で公開されていないかどうかのチェック）
if($data["delete"]) :
?>
<input type="checkbox" name="del_posts[]" class="delete_check" value="<?=$data["id"]?>" />
<?php endif; ?>
</td>
<?php
//--------------------------------------------------------
// 表示日時
//--------------------------------------------------------
?>
<td class="date"><?=$_post_data[$i]["output_date"]?></td>
<?php
//--------------------------------------------------------
// カテゴリ名
//--------------------------------------------------------
?>
<td class="category"><?=$data["category"]?></td>
<?php
//--------------------------------------------------------
// タイトル
//--------------------------------------------------------
?>
<td class="title">
<a href="<?=$data["edit_link"]?>"<?=$_post_data[$i]["app_icon_class"]?>><?=$data["title"]?></a><?php if($data["memo"]):?><br><p style="padding: 0 0 0 15px;"><?=nl2br($data["memo"])?></p><?php endif; ?>
</td>
<?php
//--------------------------------------------------------
// 公開、非公開
//--------------------------------------------------------
?>
<td class="public"><?=$_post_data[$i]["publish_menu"]?></td>
<?php
//--------------------------------------------------------
// 各種操作
//--------------------------------------------------------
?>
<td class="edit">
<a href="<?=$data["edit_link"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_edit.gif" alt="編集"  class="tip" /></a>
<a href="<?=$data["preview_url"]?>" target="_blank"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_preview.gif" alt="プレビュー" class="tip" /></a>
<?php
//権限チェック(削除権限があるか、もしくは自分自身が作成した記事で公開されていないかどうかのチェック）
if($data["delete"]) :
?>
<span class="btn_del" id="del_<?=$data["id"]?>"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/ico_delete.gif" alt="削除" class="tip" /></span>
<?php endif; ?>
</td>
</tr>
<?php
    endforeach;
// ------------------------------------------------------------------------
// ループここまで
// ------------------------------------------------------------------------
endif;
?>

</tbody>
</table>
<input type="hidden" name="delete" value="1" />
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>



<?php
// ------------------------------------------------------------------------
// 
// スマフォ用一覧表示テンプレートここから
// 
// ------------------------------------------------------------------------
?>
<form action="<?=$form_delete_url?>" name="del_form_smp" method="POST">
<ul class="list smp">
<?php
// ------------------------------------------------------------------------
// ここからループ
// ※エスケープさせずに埋め込む変数値は$_post_data[$i][keyname]で取り出す
// ------------------------------------------------------------------------
if(isset($post_data) && $post_data) :
    foreach($post_data as $i => $data) :
?>
<li<?=$_post_data[$i]["sub_data"]?> id="post_<?=$data["id"]?>">
<p class="check">
<?php
//権限チェック(削除権限があるか、もしくは自分自身が作成した記事で公開されていないかどうかのチェック）
if($data["delete"]) :
?>
<input type="checkbox" name="del_posts[]" class="delete_check_smp" value="<?=$data["id"]?>" />
<?php endif; ?>
</p>
<div class="text">
<p class="title"><a href="<?=$data["edit_link"]?>" class="location"><?=$data["title"].$data["app_device_txt"]?></a></p>
<p class="date"><?=$_post_data[$i]["output_date"]?></p>
<p class="category"><?=$data["category"]?></p>
<?php
//アカウント名（承認フローのオプションが有効なら）
if($option_account) :
?>
<p class="account"><?=$data["account_name"]?></p>
<?php
endif;
if(strstr($_post_data[$i]["publish_menu_smp"],"未公開")){
	$addClass = "closed";
}
?>
<p class="public <?=$addClass?>"><?=$_post_data[$i]["publish_menu_smp"]?></p>
</div>
</li>
<?php

    endforeach;
// ------------------------------------------------------------------------
// ループここまで
// ------------------------------------------------------------------------
endif;
?>
</ul>
<input type="hidden" name="delete" value="1" />
<input type="hidden" name="token" value="<?=$csrf_token?>" />
</form>
