<?php

    $title = KEYWORD_KIJI."一覧";
    if(isset($_GET["wait"]) && $_GET["wait"]) $title = "承認待ち一覧";
    
    $page_setting = array(
        "title" => $title,
        "js" => array(),
        "css" => array(LACNE_SHAREDATA_PATH."/css/article/index.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>

<script type="text/javascript">
$(document).ready(function(){
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .list');
});
var PAGE = '<?=$page?>';
var PAGE_NUM = '<?=$page_num?>';
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "index" , array("order"=>$order,"csrf_token"=>$csrf_token));
?>


<section class="section">
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a><?=$title?></h1>
<p class="load">登録されている<?=KEYWORD_KIJI?>一覧や、<?=KEYWORD_KIJI?>のステータスを確認できます。</p>

<?php
//承認待ちデータの案内（承認フローオプションが有効の場合のみ表示）
if(method_exists($LACNE->model["post"], "data_cnt_waiting") && isset($cnt_wait) && $cnt_wait) :
    if(!isset($_GET["wait"])) :
?>
<div class="alert note pie"><span class="icon">情報</span><p class="fl">承認待ちの<?=KEYWORD_KIJI?>が<a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php?wait=1"><?=$cnt_wait?>件</a>あります。</p><p class="fr"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php?wait=1">承認待ち一覧</a></p></div>
<?php else: ?>
<div class="alert memo pie"><span class="icon">情報</span><p class="fl">承認待ちの<?=KEYWORD_KIJI?>のみを表示しています。</p><p class="fr"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php">すべて表示</a></p></div>
<?php
    endif;
endif; 
?>
<?php
//カテゴリで絞りこまれているか
if($category_id && isset($category_list[$category_id])) :
?>
<div class="alert comp pie"><span class="icon">情報</span><p class="fl">カテゴリ「<?=$category_list[$category_id]?>」の<?=KEYWORD_KIJI?>のみを表示しています。</p><p class="fr pc"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php">すべて表示</a></p></div>
<?php endif; ?>
<?php
//記事が０件なら
if(!isset($cnt) || !$cnt) :
?>
<div class="alert error pie"><span class="icon">注意</span><p class="fl">
<?php
	 if(!isset($_GET["wait"])) :
?>
表示するデータがありません。
<?php
	else:	
?>
承認待ちのデータはありません。<a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php">すべて表示</a>
<?php
	endif;	
?>
</p></div>
<?php endif; ?>
<!-- .section // --></section>

<section class="section">

<div class="ctrl smp">
<p><select id="smp_action">
   <option value="publish"><?=KEYWORD_KIJI?>を公開/非公開</option>
   <?php //承認オプションが有効かどうか
   if(method_exists($LACNE->library["post"], "app_publish")) :
   ?>
   <option value="app"><?=KEYWORD_KIJI?>を承認/差戻し</option>
   <?php endif; ?>
   <option value="delete"><?=KEYWORD_KIJI?>を削除</option>
   </select>
</p>
<p class="btn-type01 pie" id="btn_go"><span class="pie btn_action">GO</span></p>
<!-- .ctrl // --></div>

<div class="pager-head">
<p class="count pc" id="pager_head_area"><?=$_html_pager_head?></p>
<div class="pulldown pie pc mb10">
<p class="text pie">
<?php
//------------------------------------------------------------
//カテゴリのプルダウン選択
//------------------------------------------------------------
if(isset($category_list[$category_id])) echo $category_list[$category_id];
else echo "すべてのカテゴリ";
?>
</p>
<p class="btn"><a href="javascript:void(0);"><img src="<?=LACNE_SHAREDATA_PATH?>/images/common/pulldown_img.gif" width="24" height="28" alt="" /></a></p>
<ul class="list">
<?php if(isset($_GET["category"])) : ?>
<li><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php">すべてのカテゴリ</a></li>
<?php endif; ?>
<?php
if(isset($category_list) && is_array($category_list)) :
    foreach($category_list as $key => $category) :
?>    
<li><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php?category=<?=$key?>&<?=fn_set_urlparam($_GET, array('wait'),false)?>"><?=$category?>（<?=(isset($cnt_category_by[$key]))?$cnt_category_by[$key]:0?>）</a></li>
<?php
    endforeach;
endif;
?>
<!-- .list // --></ul>
<!-- .pulldown // --></div>
<select name="category" class="smp" id="smp_select_cat">
<option value="">すべて表示</option>
<?=fn_output_html_select($category_list,$category_id)?>
</select>
<p class="label smp">カテゴリ</p>
<!-- .pager-head // --></div>

<?=
//一覧表示部分（別ファイルに一覧用のテンプレートを用意 temp_index_list.php）
//-----------------------------------------------------------------
$_list_html
?>
<br><br><br><br>
<div class="pager-foot">
<p class="btn-type03 pc"><strong><a href="#" class="pie" id="btn_dels"><span>選択<?=KEYWORD_KIJI?>の削除</span></a></strong></p>
<div class="pager" id="pager_area">
<?=$_html_pager_str?>
<!-- .pager // --></div>
<!-- .pager-info // --></div>

<!-- .section // --></section>


<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//ここからモーダル画面用
//--------------------------------------------------------
?>
<?=
//記事削除（個別削除）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-delete");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl"><?=KEYWORD_KIJI?>「<span id="delete_post"></span>」を削除してもよろしいですか？</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="#" id="delete_link"><span class="pie">削除</span></a></p>
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//記事削除（複数選択削除）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-delete02");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl">選択した<?=KEYWORD_KIJI?>を削除してもよろしいですか？</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="#" id="run-delete"><span class="pie">削除</span></a></p>
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//公開、承認、SNS画面（iframe）
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-public");
?>
<iframe src="" width="500" frameborder="0" scrolling="no"></iframe>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<?=
//エラー表示用
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-error");
?>
<div class="alert error pie"><span class="icon">注意</span><p class="fl" id="error_message"></p></div>
<div class="btn btn-one">
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<!-- #Modal // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>