<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $("#Pc #Side").remove();
    $('#Main').width($('#Content').width());

    $parent = $(window.parent.document);
    $('#Content').css({width:$parent.find('#box-public iframe').width()});
    $parent.find('#box-public iframe').css({height:$('#Content').height()});
    $parent.find('#box-approval iframe').css({height:$('#Content').height()});

    $("#Main .section .btn-type02 a").click(function(){
            window.parent.location.href = "<?=$_cancel_page?>";
            return false;
    });


});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    


});
// -----------------------------------------------
</script>
