var lacne_component = lacne_component || {};

(function($){

    function get_img(path){

        if(path){
            return '<img src="' + path + '" width="100px" />';
        }

        return '';
    }

    /**
     * プロフィール大
     * @constructor
     */
    lacne_component.Profile1 = function(){};

    lacne_component.Profile1.STATE = 'profile1';

    lacne_component.Profile1.prototype = {
        vm: function(){
            return Vue.extend({
                created: function(){
                    var index = $('input').length + 1;
                    var data  = this.$parent.$data.init_data.profile1;

                    this.$data.key = 'section_img_img' + ( index );
                    this.$data.img_value = data.img_name;
                    this.$data.img_preview = get_img(this.$data.img_value);

                    var nextindex = index + 1;
                    this.$data.profile_key = 'section_img_img' + ( nextindex );
                    this.$data.profile_img_value = data.profile_img_name;
                    this.$data.profile_img_preview = get_img(this.$data.profile_img_value);

                    this.$data.simei_value           = data.simei_name;
                    this.$data.soshiki_value         = data.soshiki_name;
                    this.$data.intoro_value          = data.intoro_name;
                    this.$data.blog_link_title_value = data.blog_link_title_name;
                    this.$data.blog_link_value       = data.blog_link_name;
                },
                template: $('#select-profile-component-profile1').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Profile1.STATE );

                    return  {

                        img_name: name + '[img_name]',
                        img_value: '',
                        img_preview: '',
                        key: '',

                        profile_img_name: name + '[profile_img_name]',
                        profile_img_value: '',
                        profile_img_preview: '',
                        profile_key: '',

                        simei_name: name + '[simei_name]',
                        simei_value: '',

                        soshiki_name: name + '[soshiki_name]',
                        soshiki_value: '',

                        intoro_name: name + '[intoro_name]',
                        intoro_value: '',

                        blog_link_title_name: name + '[blog_link_title_name]',
                        blog_link_title_value: '',

                        blog_link_name: name + '[blog_link_name]',
                        blog_link_value: ''

                    }
                }
            });
        }
    };

    /**
     * プロフィール小
     * @constructor
     */
    lacne_component.Profile2 = function(){};

    lacne_component.Profile2.STATE = 'profile2';

    lacne_component.Profile2.prototype = {
        vm: function(){
            return Vue.extend({
                created: function(){
                    var index = $('input').length + 1;
                    this.$data.key = 'section_profile_img' + index;

                    var data                 = this.$parent.$data.init_data.profile2;
                    this.$data.img_value     = data.img_name;
                    this.$data.preview_img   = get_img(this.$data.img_value);
                    this.$data.simei_value   = data.simei_name;
                    this.$data.soshiki_value = data.soshiki_name;
                    this.$data.intoro_value          = data.intoro_name;
                },
                template: $('#select-profile-component-profile2').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.Profile2.STATE );

                    return  {

                        img_name: name + '[img_name]',
                        img_value: '',
                        preview_img: '',
                        key: '',

                        simei_name: name + '[simei_name]',
                        simei_value: '',

                        soshiki_name: name + '[soshiki_name]',
                        soshiki_value: '',

                        intoro_name: name + '[intoro_name]',
                        intoro_value: ''
                    }
                }
            });
        }
    };

}(jQuery));
