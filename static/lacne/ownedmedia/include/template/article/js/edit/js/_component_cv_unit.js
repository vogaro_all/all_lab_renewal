var lacne_component = lacne_component || {};

(function($){

    function get_img(path){

        if(path){
            return '<img src="' + path + '" width="100px" />';
        }

        return '';
    }

    /**
     * テキストパターン
     * @constructor
     */
    lacne_component.cv1 = function(){};

    lacne_component.cv1.STATE = 'cv1';

    lacne_component.cv1.prototype = {
        vm: function(){
            return Vue.extend({
                created: function(){
                	this.$data.key = 'cv-text' + lacne_component.Base.getUnique();

                    var data = this.$parent.$data.init_data.cv1;

                    this.$data.title_value           = data.title_name;
                    this.$data.text_value            = data.text_name;
                    this.$data.text_inside_value     = data.text_inside_name;
                    this.$data.link_title_value      = data.link_title_name;
                    this.$data.link_value            = data.link_name;
                },
                template: $('#select-cv-component-cv1').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.cv1.STATE );

                    return  {

                        title_name: name + '[title_name]',
                        title_value: '',

                        text_name: name + '[text_name]',
                        text_value: '',

                        text_inside_name: name + '[text_inside_name]',
                        text_inside_value: '',

                        link_title_name: name + '[link_title_name]',
                        link_title_value: '',

                        link_name: name + '[link_name]',
                        link_value: ''

                    }
                }
            });
        }
    };

    /**
     * バナーパターン
     * @constructor
     */
    lacne_component.cv2 = function(){};

    lacne_component.cv2.STATE = 'cv2';

    lacne_component.cv2.prototype = {
        vm: function(){
            return Vue.extend({
                created: function(){
                    var index = $('input').length + 1;
                    this.$data.key  = 'section_cv_img' + index;
                    this.$data.key2 = 'section_cv_img' + index+1;

                    var data                = this.$parent.$data.init_data.cv2;

                    this.$data.title_value  = data.title_name;
                    this.$data.text_value   = data.text_name;
                    this.$data.img_value1   = data.img_name1;
                    this.$data.preview_img1 = get_img(this.$data.img_value1);
                    this.$data.link_value1  = data.link_name1;
                    this.$data.img_value2   = data.img_name2;
                    this.$data.preview_img2 = get_img(this.$data.img_value2);
                    this.$data.link_value2  = data.link_name2;
                },
                template: $('#select-cv-component-cv2').html(),
                data: function(){
                    var name = lacne_component.Base.getName( lacne_component.cv2.STATE );

                    return  {

                        title_name: name + '[title_name]',
                        title_value: '',

                        text_name: name + '[text_name]',
                        text_value: '',

                        img_name1: name + '[img_name1]',
                        img_value1: '',
                        preview_img1: '',
                        key: '',

                        link_name1: name + '[link_name1]',
                        link_value1: '',

                        img_name2: name + '[img_name2]',
                        img_value2: '',
                        preview_img2: '',
                        key2: '',

                        link_name2: name + '[link_name2]',
                        link_value2: ''
                   }
                }
            });
        }
    };

}(jQuery));
