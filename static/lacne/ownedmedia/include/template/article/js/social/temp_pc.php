<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $("#Pc #Side").remove();
    $('#Main').width($('#Content').width());

    $parent = $(window.parent.document);
    $('#Content').css({width:$parent.find('#box-public iframe').width()});
    $parent.find('#box-public iframe').css({height:$('#Content').height()});
    window.parent.$.library.modalAdjust();


});
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    $.prettyLoader();
    $("#btn_sns_submit").click(function(){
        $.prettyLoader.show();
    })

});
// -----------------------------------------------
</script>
