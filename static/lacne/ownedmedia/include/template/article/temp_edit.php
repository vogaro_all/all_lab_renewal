<?php
use lacne\core\model\SnsReserved;

    $page_setting = array(
        "title" => KEYWORD_KIJI."作成",
        "js" => array(
            LACNE_SHAREDATA_PATH."/js/jquery-ui-1.8.13.custom.min.js" ,
            LACNE_SHAREDATA_PATH."/js/ckeditor/ckeditor.js",
            LACNE_SHAREDATA_PATH."/js/timepicker/timepicker.js",
            LACNE_SHAREDATA_PATH."/js/medialist.js",
            LACNE_SHAREDATA_PATH."/js/vue.js",
        ),
        "css" => array(
            LACNE_SHAREDATA_PATH."/css/article/edit.css",
            LACNE_SHAREDATA_PATH."/js/smoothness/jquery-ui-1.8.13.custom.css",
            LACNE_SHAREDATA_PATH."/js/timepicker/style.css"
        )
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");

?>

<script type="text/javascript">
$().ready(function() {
    //Navigation
    $.library.sideActive('<?=LACNE_APP_ADMIN_NAVI_ID?> .news.article');
});


var Setting_toolbar = [];
var LACNE_WIDTH_DIALOG = 450;
var INSERT_MOVIE = 0;
<?php if(isset($movie_panel) && $movie_panel) : ?>
INSERT_MOVIE = 1;
<?php endif; ?>
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "edit" , array("err"=>$err , "message"=>$message , "data_list"=>$data_list , "_data_list" => $_data_list));
?>

<section class="section">
<h1 class="head-line01"><a href="<?=LACNE_APP_INDEXPAGE_URL?>" class="btn-back smp">戻る</a><?=KEYWORD_KIJI?>作成</h1>
<p class="load"><?=KEYWORD_KIJI?>の作成、編集を行います。（<span class="txt-r"> 必須 の付いている項目は必須項目です</span>）</p>
<?php
//--------------------------------------------------------
// エラー or メッセージ表示
//--------------------------------------------------------
?>
<?php if(isset($err) && $err) : ?>
<div class="alert error pie" id="comp_message" style="display:none"><span class="icon">エラー</span><p class="fl"><?=fn_output_errtxt($_err)?></p></div>
<?php elseif(isset($message) && $message) : ?>
<div id="comp_message" style="display:none">
<div class="alert comp pie"><span class="icon">完了</span><p class="fl"><?=$_message?></p></div>
<div class="btn btn-backlist">
<p class="btn-type01 pie"><a href="<?=LACNE_APP_ADMIN_PATH?>/article/index.php"><span class="pie"><?=KEYWORD_KIJI?>一覧へ</span></a></p>
</div>
</div>
<?php endif; ?>
<?php if(!$post_authority): ?>
<div class="alert note pie" id="comp_message"><span class="icon">エラー</span><p class="fl">権限がないか、または公開済みのため、編集を行うことができません。</p></div>
<?php endif; ?>
<!-- .section // --></section>

<section class="section">
<!--
<h2 class="head-line02">URL</h2>
<dl>
	<dt>
		公開用
	</dt>
	<dd>
		<a href="<?php echo("http://".$_SERVER['SERVER_NAME']."/detail/?id=".$data_list["id"]); ?>" target="_blank"><?php echo("http://".$_SERVER['SERVER_NAME']."/detail/?id=".$data_list["id"]); ?></a>
	</dd>
	<dt>
		プレビュー用
	</dt>
	<dd>
		<a href="<?php echo("http://".$_SERVER['SERVER_NAME']."/detail/?id=".$data_list["id"]."&preview=1"); ?>" target="_blank" ><?php echo("http://".$_SERVER['SERVER_NAME']."/detail/?id=".$data_list["id"]."&preview=1"); ?></a>
	</dd>
</dl>
-->

<h2 class="head-line02">基本情報</h2>
<form action="/lacne/ownedmedia/admin/article/edit.php?action=submit&<?=fn_set_urlparam($_GET , array('id') , false)?>" method="POST">
<dl>

<?php /*
<dt class="disp_encyclopedia">中カテゴリ：<span class="req">必須</span></dt>
<dd class="disp_encyclopedia"><?=fn_output_html_checkbox('sub_category', $sub_category_list,$data_list["sub_category"])?>
</dd>
*/ ?>

<?php
//--------------------------------------------------------
// タイトル
//--------------------------------------------------------
?>
<dt>タイトル：<span class="req">必須</span></dt>
<dd><input type="text" class="text" name="title" maxlength="255" value="<?=(isset($data_list["title"]))?$data_list["title"]:""?>" /></dd>
<?php /*
<dt class="disp_encyclopedia">ふりがな：<span class="req">必須</span></dt>
<dd class="disp_encyclopedia"><input type="text" class="text" name="kana" maxlength="255" value="<?=(isset($data_list["kana"]))?$data_list["kana"]:""?>" /></dd>
*/ ?>
<?php
//--------------------------------------------------------
// 表示日時
//--------------------------------------------------------
?>
<dt>公開日時：<span class="req">必須</span></dt>
<dd><input type="text" name="output_date" id="datetime" class="date" maxlength="19" value="<?=(isset($data_list["output_date"]))?$data_list["output_date"]:""?>" />
<p class="ex">※YYYY-mm-dd HH:ii 形式、もしくはYYYY/mm/dd HH:ii 形式で入力して下さい。<br />（例：2012-12-22 10:00）<br />※時間は省略できます(0:00にセットされます）</p></dd>

<dt>カテゴリ：<span class="req">必須</span></dt>
<dd><select name="category" id="category"><option value="">--</option><?=fn_output_html_select($category_list,$data_list["category"])?></select>
</dd>

<?php
//--------------------------------------------------------
// メインビジュアルフラグ
//--------------------------------------------------------
?>
<dt>MVフラグ：</dt>
<dd>
<input type="checkbox" name="mv_flag" value="1" <?php if(isset($data_list["mv_flag"]) && $data_list["mv_flag"] == 1) echo "checked=\"checked\""; ?> /> メインビジュアルに表示する</dd>


<?php
//--------------------------------------------------------
// リード文
//--------------------------------------------------------
?>
<?php /*
<dt>一覧用リード文：</dt>
<dd><input type="text" class="text" name="lead" maxlength="255" value="<?=(isset($data_list["lead"]))?$data_list["lead"]:""?>" /></dd>
*/ ?>
<?php
//--------------------------------------------------------
// KEYWORD選択
//--------------------------------------------------------
?>
<!-- JSで変数設定用のダミーセレクト -->
<select id="tag_default" style="display: none"><?=fn_output_html_select($tag_list,$data_list["category"])?></select>
<dt>KEYWORD：</dt>
<dd>
<?php if(!empty($tag_list)) : ?>
	<span id="tag-add-button" class="button">KEYWORDを追加</span>
	<div style="display:none">
	</div>
    
	<ul class="tag_list">

		<?php
			$newCreate = false;
			if(empty($data_list['tags']))
			{
				//連想配列から配列に
				$ary = array_values($tag_list);

				//配列の形式をあわせる
				$i=0;
				foreach($ary as $index => $tmp)
				{
					$list[$i]['name'] = $tmp;
					$i++;
				}

				//フラグ
				$newCreate = true;
			}
			else
			{
				$list = $data_list['tags'];
			}

			foreach($list as $index => $item) :
		?>

		<li>
		    <input type="text" name="search_tag[]"  class="targetText" value="" style="width: 172" placeholder="検索文字を入力してください"/>
			<?php echo createSelectBoxTag($tag_list, $item['tag_name'])?>
			<span class="delete button btn-info">×</span>
		</li>

		<?php
				//新規作成ならタグ選択ボックスを一つ出して終了
				if($newCreate) break;
			endforeach;
		?>
	</ul>
<?php else: ?>
    <p>KEYWORDの登録がありません。KEYWORD管理からタグを登録してください。</p>
<?php endif; ?>
</dd>

<?php
//--------------------------------------------------------
// 画像
//--------------------------------------------------------
?>
<div class="toggle_show_img_iteml">
<dt>画像：<span class="req">必須</span><?php //画像選択フィールド ?></dt>
<dd>
<div class="btn img_area">
<p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_1" /></p>
<p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_1" /></p>
</div>
<input type="hidden" name="thumb_image" value="<?=(isset($data_list["thumb_image"]))?$data_list["thumb_image"]:""?>" class="img_hidden_value" id="img_hidden_1" />
<p id="img_prev_1" class="img_preview"></p>
</dd>
</div>
<?php /*
<dt>ピックアップ：</dt>             
<dd><input type="checkbox" name="pickup" value="1" <?php if(isset($data_list["pickup"]) && $data_list["pickup"] == 1) echo "checked=\"checked\""; ?> />※チェックを入れるとPICKUPエリアへ表示します。</dd>
*/?>
</dl>

<h2 class="head-line02">メタ設定</h2>
<dl>
<?php
//--------------------------------------------------------
// METAキーワード
//--------------------------------------------------------
?>
<?php /*
<dt>キーワード：<span>メタ情報</span></dt>
<dd><input type="text" class="text" name="keyword" maxlength="255" value="<?=(isset($data_list["keyword"]))?$data_list["keyword"]:""?>" /></dd>
*/?>

<?php
//--------------------------------------------------------
// METAディスクリプション
//--------------------------------------------------------
?>
<dt>説明：<span>メタ情報</span></dt>
<dd><input type="text" class="text" name="description" maxlength="255" value="<?=(isset($data_list["description"]))?$data_list["description"]:""?>" /></dd>

<?php
//--------------------------------------------------------
// サムネイル画像
//--------------------------------------------------------
?>
<dt>og:image：<?php //画像選択フィールド ?></dt>
<dd>
<div class="btn img_area">
<p class="btn-type01 pie img_btn"><input type="button" value="画像選択" id="img_select_2" /></p>
<p class="btn-type02 pie img_delete_btn"><input type="button" value="削除" id="img_delete_2" /></p>
</div>
<input type="hidden" name="ogp_image" value="<?=(isset($data_list["ogp_image"]))?$data_list["ogp_image"]:""?>" class="img_hidden_value" id="img_hidden_2" />
<p id="img_prev_2" class="img_preview"></p>
</dd>

</dl>

<div class="contents">
<h2 class="head-line02">コンテンツ内容</h2>

<?php
/**==========================
 * アプリケーション生成場所
 *==========================*/
?>
<section id="app"></section>
<!-- .contents --></div>

<dl>


<section style="padding-top: 100px"></section>
<div class="alert note pie pc"><span class="icon">注意</span><p class="fl">編集画面での表示とプレビュー画面での表示は異なる場合があります。</p></div>
<div class="btn ownedmedia">
<?php if($post_authority): ?>
<p class="btn-type01 pie ownedmedia"><span class="pie" id="submit">登録</span></p>
<?php endif; ?>
<p class="btn-type02 pie pc ownedmedia"><span class="pie" id="preview">プレビュー</span></p>
<p class="btn-type02 pie ownedmedia"><span class="pie" id="cancel">キャンセル</span></p>
</div>
<input type="hidden" name="modified" value="<?=(isset($data_list["modified"]))?$data_list["modified"]:""?>" />
<input type="hidden" name="token" value="<?=$csrf_token?>" />
<input type="hidden" name="id" value="<?=(isset($data_list["id"]))?$data_list["id"]:""?>" />
<div id="preview_area"></div>
</form>
<!-- .section // --></section>


<section id="Modal" class="section">
<?php
//--------------------------------------------------------
//モーダル画面用
//--------------------------------------------------------
?>
<?=
//詳細本文をクリア
//------------------------------
$LACNE->library["admin_view"]->html_modal_open("box-clear");
?>
<div class="alert note pie"><span class="icon">注意</span><p class="fl"><?=KEYWORD_KIJI?>詳細の内容をクリアします。よろしいですか？</p></div>
<div class="btn">
<p class="btn-type01 pie"><a href="#" id="delete_link"><span class="pie">クリア</span></a></p>
<p class="btn-type02 pie"><a href="#" class="modal-close"><span class="pie">キャンセル</span></a></p>
<!-- .btn // --></div>
<?=$LACNE->library["admin_view"]->html_modal_close();?>

<!-- #Modal // --></section>

<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>
<?php require_once( dirname(__FILE__) . '/js/edit/js/component_defined.php' ); ?>
