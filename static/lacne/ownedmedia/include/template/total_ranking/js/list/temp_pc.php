<script type="text/javascript">
<?php
if($device == "pc") :
// -----------------------------------------------
// PCの場合のみロード
// -----------------------------------------------
?>
$(function(){
    
    $("#Pc #Side").remove();
    
    MediaDrop.init({
        'filetype' : <?php echo "['".implode("','", $LACNE->library['total_ranking']->filetype)."']"; ?>,
        'maxfilesize' : <?php echo $LACNE->library['total_ranking']->maxfilesize; ?>,
    });
});
function template(){
    return "form.pc table.upload-ui tbody tr";
}
function template_delete(){
    return "tr";
}
<?php
endif;
?>
    
// -----------------------------------------------
// 以下は共通ロード
// -----------------------------------------------
$(function(){
    
    // アップロードするファイルの追加
    $("a#AddFile").click(function(){
        var $template = $(template());
        var file_num = $template.size();
        if(file_num < 10){
            $template = $template.filter(":last");
            $template
                .clone(true)
                .find('input').val("").end()
                .insertAfter($template);
            if(file_num == 9){
                $(this).hide();
            }
        }
        return false;
    });

    // アップロードするファイルの削除
    $("a.CancelFile").click(function(){
        if($(this).closest('#image-preview').length){
            $(this).closest(template_delete()).remove();
        }else{
            var file_num = $(template()).size();
            if(1 < file_num){
                $(this).closest(template_delete()).remove();
                $("a#AddFile").show();
            }
        }
        return false;
    });

    $("#btn_close").click(function(){
        window.close();
    });

    //タグをクリックすると検索フォームに入力される処理
    $('.taglist a.tag').click(function(){
        $('input[name=search_tag]').val($(this).html());
        return false;
    });

    //タグ検索 すべて表示ボタン
    $('#btnAllView').click(function(){
        $('input[name=search_tag]').val("");
        $('#tagSearchForm').submit();
    });
	
    //Output Error モーダルで表示
    <?php
    if((isset($err) && $err) || (isset($message) && $message)) {
    ?>
            $.library.do_slideDown_message("#comp_message");
    <?php
    }
    ?>
});
// -----------------------------------------------
</script>
