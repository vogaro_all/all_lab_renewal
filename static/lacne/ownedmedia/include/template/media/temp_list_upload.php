<?php

    $page_setting = array(
        "title" => "ファイルアップロード",
        "js" => array(LACNE_SHAREDATA_PATH."/js/medialist.js", LACNE_SHAREDATA_PATH."/js/mediadrop.js"),
        "css" => array(LACNE_SHAREDATA_PATH."/css/media/list.css", LACNE_SHAREDATA_PATH."/css/media/drop_upload.css", LACNE_SHAREDATA_PATH."/css/common/pager.css")
    );

    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_header.php");
    
?>
<script type="text/javascript">
$(document).ready(function(){
});
</script>

<?=
//--------------------------------------------------------
//デバイス（PC or Smph）用に最適化されたjsファイルをロード
//--------------------------------------------------------
$LACNE->library["admin_view"]->load_js_opt_device(dirname(__FILE__)."/js" , "list" , array("err"=>!empty($err)?$err:'' , "message"=>!empty($message)?$message:''));
?>


<section class="section">
	
<ul class="tab">
<li class="css3"><a href="list.php?<?=$search_param?>" class="css3">ファイル一覧</a></li>
<li class="css3 on"><a href="list.php?upload=on&<?=$search_param?>" class="css3">アップロード</a></li>
<!-- .tab // --></ul>	

<p class="section-inside"><?=KEYWORD_KIJI?>内に掲載するメディアファイルのアップロードを行います。</p>

<?php
//--------------------------------------------------------
//アップロード画面部分をインクルード
//--------------------------------------------------------
include_once(dirname(__FILE__)."/temp_common_upload.php");
?>

<div class="btn btn-one">
<p class="btn-type02 pie close_btn" id="btn_close"><a href="#"><span class="pie">閉じる</span></a></p>
<!-- .btn // --></div>
<!-- .section // --></section>


<?php
    //include common header template
    include_once(LACNE_SHARE_TEMPLATE_DIR."temp_footer.php");
?>