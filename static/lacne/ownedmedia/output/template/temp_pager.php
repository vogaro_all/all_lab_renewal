<?php
$prev_page_txt = "前へ";
$next_page_txt = "次へ";

$linkparam = "";
if($param){
        $linkparam .= "?".$param."&page=";
}else{
        $linkparam .= "?page=";
}
?>

<ul>

<?php
if($page > 0) :

    if($page == 1) :
?>
<li class="prev"><?=$prev_page_txt?></li>
<?php
    else :
?>
<li class="prev"><a href="<?=$linkparam.($page-1)?>"><?=$prev_page_txt?></a></li>
<?php
    endif;
?>

<?php
    for($i=1;$i<=$page_num;$i++) :
        if($i == $page) :
?>
<li class="count active"><span><?=$i?></span></li>
<?php
        else:
?>
<li class="count"><a href="<?=$linkparam.$i?>"><?=$i?></a></li>
<?php
        endif;
    endfor;
?>

<?php
    if($page == $page_num) :
?>
<li class="next"><?=$next_page_txt?></li>
<?php
    else :
?>
<li class="next"><a href="<?=$linkparam.($page+1)?>"><?=$next_page_txt?></a></li>
<?php
    endif;
    
endif;
?>

</ul>