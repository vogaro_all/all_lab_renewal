<?php
require_once("./lacne/ownedmedia/output/post.php");
use lacne\service\output\OwnedMediaOtherAPI;
use lacne\service\output\OwnedMediaHelper;
use lacne\core\Pager;
$ownedMediaOtherAPI = new OwnedMediaOtherAPI();

$params = array(
    'limit' => 5,
    'mv_flag' => 1,
);
$mvDataList = $ownedMediaOtherAPI->load("loadArticleList", $params);

$limit 	= 12;
$params = array(
    'limit' => $limit,
//     'mv_flag' => 0,
);
$dataList = $ownedMediaOtherAPI->load("loadArticleList", $params);

//ページャ条件設定
$page 	= $_GET['page'];
$length = $dataList['ret']['cnt'];
$pages = (OwnedMediaHelper::pager( new Pager($page, $length, $limit) ));

// side用
$params = array(
    'limit' => 5,
);
$rankingWeeklyDataList = $ownedMediaOtherAPI->load("loadRankingWeekly", $params);
$rankingTotalDataList = $ownedMediaOtherAPI->load("loadRankingTotal", $params);
$keywordList = $ownedMediaOtherAPI->load("loadKeywordList", $params);

include_once('./index_template.html');
?>