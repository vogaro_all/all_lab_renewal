<?php
$lacne_path = dirname(__FILE__). "/../lacne";

require_once($lacne_path . '/Base/system/composer/vendor/autoload.php');
require_once($lacne_path."/ownedmedia/include/setup.php");

use lacne\service\output\OwnedMediaOtherAPI;
use lacne\domain\OwnedMedia\entity\MediaEntity;

$ownedMediaOtherAPI = new OwnedMediaOtherAPI();

// 記事＆セクション情報取得
$_post_data = $ownedMediaOtherAPI->details();

$MediaEntity = new MediaEntity( fn_esc($_post_data) );

$keys = array();
foreach ($MediaEntity->getAllTags() as $tagEntity) {
    $keys[] = $tagEntity->getId();
}

$page_data = $_post_data['page_data'] ;

$limit 	= 4;
$params = array(
    'limit' => $limit,
    //     'category' => $MediaEntity->getCategory(),
    'keys' => $keys,
    'not_id' => $MediaEntity->getId(),
);
$relationDataList = $ownedMediaOtherAPI->load("loadArticleList", $params);

// side用
$params = array(
    'limit' => 5,
);
$rankingWeeklyDataList = $ownedMediaOtherAPI->load("loadRankingWeekly", $params);
$rankingTotalDataList = $ownedMediaOtherAPI->load("loadRankingTotal", $params);
$keywordList = $ownedMediaOtherAPI->load("loadKeywordList", $params);

include_once('./detail_template.html');

?>