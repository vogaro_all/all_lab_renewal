<header class="l-header">
  <div class="l-header__top">
    <p class="l-header__logo">
      <a href="/" class="logo"><img src="/assets/images/pages/common/logo.png" alt="" width="107.5" height="42"></a>
      <span class="text">自分らしく<br class="u-d-md-none">楽しめる暮らし</span>
    </p>
    <div class="l-header__search" id="js-header-search">
      <svg width="20" height="20" class="u-d-md-none"><use xlink:href="/assets/images/sprite.symbol.svg#search"></use></svg>
      <form class="search-inner" action="/search/" method="get">
        <input type="text" name="keywords" placeholder="キーワードを入力してください">
        <button class="submit" type="submit"><svg width="20" height="20"><use xlink:href="/assets/images/sprite.symbol.svg#search"></use></svg></button>
      </form>
    </div>
    <button class="l-header__toggler" id="js-menu-trigger">
      <span class="line"></span>
      <span class="line"></span>
      <span class="line"></span>
    </button>
  </div><!-- .l-header__top -->

  <div class="l-header__category">
    <div class="swiper-container" id="js-header-category">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <a href="/house/" class="link">
            <span class="link__inner">
              <img src="/assets/images/pages/common/icon_category01.svg" alt="" class="link__icon">
              <span class="link__text">建てる</span>
            </span>
          </a>
        </div>
        <div class="swiper-slide">
          <a href="/renovation/" class="link">
            <span class="link__inner">
              <img src="/assets/images/pages/common/icon_category02.svg" alt="" class="link__icon">
              <span class="link__text">リノベする</span>
            </span>
          </a>
        </div>
        <div class="swiper-slide">
          <a href="/interior/" class="link">
            <span class="link__inner">
              <img src="/assets/images/pages/common/icon_category03.svg" alt="" class="link__icon">
              <span class="link__text">インテリア</span>
            </span>
          </a>
        </div>
        <div class="swiper-slide">
          <a href="/life/" class="link">
            <span class="link__inner">
              <img src="/assets/images/pages/common/icon_category04.svg" alt="" class="link__icon">
              <span class="link__text">暮らしのQ&A</span>
            </span>
          </a>
        </div>
        <div class="swiper-slide">
          <a href="/hiroshima/" class="link">
            <span class="link__inner">
              <img src="/assets/images/pages/common/icon_category05.svg" alt="" class="link__icon">
              <span class="link__text">広島の街</span>
            </span>
          </a>
        </div>
        <div class="swiper-slide">
          <a href="/lifestyle/" class="link">
            <span class="link__inner">
              <img src="/assets/images/pages/common/icon_category06.svg" alt="" class="link__icon">
              <span class="link__text">ライフスタイル</span>
            </span>
          </a>
        </div>
        <div class="swiper-slide u-d-md-none"></div>
      </div>
      <button class="swiper-button-prev"></button>
      <button class="swiper-button-next"></button>
    </div>
  </div>
</header>

<div class="l-header__menu" id="js-menu-target">
  <div class="menu">
    <button class="close" id="js-menu-close">
      <span class="line"></span>
      <span class="line"></span>
    </button>

    <div class="inner">
      <ul class="category-list">
        <li class="category-list__item">
          <a href="/house/" class="link">
            <span class="link__icon"><img src="/assets/images/pages/common/icon_category01.svg" alt=""></span>
            <span class="link__text">建てる</span>
          </a>
        </li>
        <li class="category-list__item">
          <a href="/renovation/" class="link">
            <span class="link__icon"><img src="/assets/images/pages/common/icon_category02.svg" alt=""></span>
            <span class="link__text">リノベする</span>
          </a>
        </li>
        <li class="category-list__item">
          <a href="/interior/" class="link">
            <span class="link__icon"><img src="/assets/images/pages/common/icon_category03.svg" alt=""></span>
            <span class="link__text">インテリア</span>
          </a>
        </li>
        <li class="category-list__item">
          <a href="/life/" class="link">
            <span class="link__icon"><img src="/assets/images/pages/common/icon_category04.svg" alt=""></span>
            <span class="link__text">暮らしのQ&A</span>
          </a>
        </li>
        <li class="category-list__item">
          <a href="/hiroshima/" class="link">
            <span class="link__icon"><img src="/assets/images/pages/common/icon_category05.svg" alt=""></span>
            <span class="link__text">広島の街</span>
          </a>
        </li>
        <li class="category-list__item">
          <a href="/lifestyle/" class="link">
            <span class="link__icon"><img src="/assets/images/pages/common/icon_category06.svg" alt=""></span>
            <span class="link__text">ライフスタイル</span>
          </a>
        </li>
      </ul>

      <div class="link-wrap">
        <ul class="nav-list">
          <li class="nav-list__item"><a href="/" class="text">TOP</a></li>
          <li class="nav-list__item"><a href="/about/" class="text">ABOUT</a></li>
          <li class="nav-list__item"><a href="https://www.allhouse.co.jp/contact/" target="_blank" class="text">CONTACT</a></li>
          <li class="nav-list__item"><a href="/privacypolicy/" class="text">PRIVACY</a></li>
        </ul>
        <ul class="link-list">
          <li class="link-list__item"><a href="https://www.allhouse.co.jp/" target="_blank">コーポレートサイト</a></li>
          <li class="link-list__item"><a href="https://house.allhouse.co.jp/" target="_blank">注文住宅</a></li>
          <li class="link-list__item"><a href="https://reform.allhouse.co.jp/" target="_blank">リノベーション</a></li>
          <li class="link-list__item"><a href="https://www.all-house.net/" target="_blank">お部屋さがし</a></li>
          <li class="link-list__item"><a href="https://hiroshima-stylo.com/" target="_blank">廣島スタイロ</a></li>
        </ul>
      </div>

      <ul class="sns-list">
        <li class="sns-list__item"><a href="https://www.instagram.com/allhouse_architecture/?hl=ja" target="_blank"><img src="/assets/images/pages/common/sns_instagram.png" alt="" width="44" height="44"></a></li>
        <li class="sns-list__item"><a href="https://www.facebook.com/orulab/" target="_blank"><img src="/assets/images/pages/common/sns_facebook.png" alt="" width="44" height="44"></a></li>
        <li class="sns-list__item"><a href="https://twitter.com/orulab" target="_blank"><img src="/assets/images/pages/common/sns_twitter.png" alt="" width="44" height="44"></a></li>
      </ul>
    </div>
  </div>
</div>
