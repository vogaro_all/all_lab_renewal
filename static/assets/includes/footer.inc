<footer class="l-footer">
  <div class="l-footer__inner">
    <ul class="link-list">
      <li class="link-list__item"><a href="/" class="text">TOP</a></li>
      <li class="link-list__item"><a href="/about/" class="text">ABOUT</a></li>
      <li class="link-list__item"><a href="https://www.allhouse.co.jp/contact/" target="_blank" class="text">CONTACT</a></li>
      <li class="link-list__item"><a href="/privacypolicy/" class="text">PRIVACY</a></li>
      <li class="link-list__item">
        <span class="text">カテゴリ</span>
        <ul class="sub-list">
          <li class="sub-list__item"><a href="/house/">建てる</a></li>
          <li class="sub-list__item"><a href="/renovation/">リノベする</a></li>
          <li class="sub-list__item"><a href="/interior/">インテリア</a></li>
          <li class="sub-list__item"><a href="/life/">暮らしのQ&A</a></li>
          <li class="sub-list__item"><a href="/hiroshima/">広島の街</a></li>
          <li class="sub-list__item"><a href="/lifestyle/">ライフスタイル</a></li>
        </ul>
      </li>
      <li class="link-list__item">
        <span class="text">グループサイト</span>
        <ul class="sub-list">
          <li class="sub-list__item"><a href="https://www.allhouse.co.jp/" target="_blank">コーポレートサイト</a></li>
          <li class="sub-list__item"><a href="https://house.allhouse.co.jp/" target="_blank">注文住宅</a></li>
          <li class="sub-list__item"><a href="https://reform.allhouse.co.jp/" target="_blank">リノベーション</a></li>
          <li class="sub-list__item"><a href="https://www.all-house.net/" target="_blank">お部屋さがし</a></li>
          <li class="sub-list__item"><a href="https://hiroshima-stylo.com/" target="_blank">廣島スタイロ</a></li>
        </ul>
      </li>
    </ul>

    <div class="information">
      <p class="information__logo"><a href="https://www.allhouse.co.jp/" target="_blank"><img src="/assets/images/pages/common/allhouse_logo.png" alt="" width="184" height="41"></a></p>
      <p class="address">
        〒735-0012<br>
        広島県安芸郡府中町八幡1丁目4-23<br>
        TEL 082-890-1002 / FAX 082-890-1003
      </p>
      <ul class="sns-list">
        <li class="sns-list__item"><a href="https://www.instagram.com/allhouse_architecture/?hl=ja" target="_blank"><img src="/assets/images/pages/common/sns_instagram.png" alt="" width="44" height="44"></a></li>
        <li class="sns-list__item"><a href="https://www.facebook.com/orulab/" target="_blank"><img src="/assets/images/pages/common/sns_facebook.png" alt="" width="44" height="44"></a></li>
        <li class="sns-list__item"><a href="https://twitter.com/orulab" target="_blank"><img src="/assets/images/pages/common/sns_twitter.png" alt="" width="44" height="44"></a></li>
      </ul>
    </div>
    <p class="l-footer__copyright"><small class="text">Copyright© allhouse. All Rights Reserved.</small></p>
  </div><!-- .l-footer__inner -->
  <button class="l-footer__page-top" data-scroll></button>
</footer>
