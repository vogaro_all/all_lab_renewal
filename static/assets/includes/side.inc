<aside class="l-side" id="js-side">
  <?php if( call_user_func( $rankingWeeklyDataList["has"]) || call_user_func( $rankingTotalDataList["has"]) ):?>
  <section class="sec-ranking">
    <h2 class="sec-ranking__heading"><img src="/assets/images/pages/common/ranking_heading.svg" alt="RANKING" width="113.5" height="83.5"></h2>
    <div class="sec-ranking__tab">
      <button class="button is-active" data-tab-trigger="weekly"><img src="/assets/images/pages/common/ranking_weekly.svg" alt="WEEKLY" width="57.5" height="7.5"></button>
      <button class="button" data-tab-trigger="total"><img src="/assets/images/pages/common/ranking_total.svg" alt="TOTAL" width="44" height="8"></button>
    </div>
    <div class="sec-ranking__inner">
      <div class="sec-ranking__content is-active" data-tab="weekly">
        <ul class="card-list">
          <?php call_user_func( $rankingWeeklyDataList["done"], function($item, $cnt, $page, $helper, $index){ ?>
          <li class="card-list__item">
            <div class="card">
              <a <?php echo($item->getLinkParam()); ?> class="card__link"></a>
              <p class="card__rank"><?php echo($index); ?></p>
              <div class="card__image"><img src="<?php echo($item->getThumbImage()); ?>" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date"><?php echo($item->getOutputDate('Y.m.d')); ?></p>
                <p class="category"><a href="/<?php echo($item->getCategoryPath()); ?>/"><?php echo(htmlspecialchars_decode($item->getCategoryName(), ENT_QUOTES)); ?></a></p>
                <p class="text"><?php echo($item->title); ?></p>
              </div>
            </div>
          </li>
          <?php }, false); ?>
<!--
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">2</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">3</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">4</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">5</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
-->
        </ul>
      </div>

      <div class="sec-ranking__content" data-tab="total">
        <ul class="card-list">
          <?php call_user_func( $rankingTotalDataList["done"], function($item, $cnt, $page, $helper, $index){ ?>
          <li class="card-list__item">
            <div class="card">
              <a <?php echo($item->getLinkParam()); ?> class="card__link"></a>
              <p class="card__rank"><?php echo($index); ?></p>
              <div class="card__image"><img src="<?php echo($item->getThumbImage()); ?>" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date"><?php echo($item->getOutputDate('Y.m.d')); ?></p>
                <p class="category"><a href="/<?php echo($item->getCategoryPath()); ?>/"><?php echo(htmlspecialchars_decode($item->getCategoryName(), ENT_QUOTES)); ?></a></p>
                <p class="text"><?php echo($item->title); ?></p>
              </div>
            </div>
          </li>
          <?php }, false); ?>
<!--
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">2</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">3</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">4</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
          <li class="card-list__item">
            <div class="card">
              <a href="#" class="card__link"></a>
              <p class="card__rank">5</p>
              <div class="card__image"><img src="/assets/images/pages/dummy/dummy01.jpg" alt="" data-object-fit></div>
              <div class="card__content">
                <p class="date">2020.04.10</p>
                <p class="category"><a href="#">建てる</a></p>
                <p class="text">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れてい</p>
              </div>
            </div>
          </li>
-->
        </ul>
      </div>
    </div>
  </section>
  <?php endif; ?>

  <?php if( call_user_func( $keywordList["has"]) ): ?>
  <section class="sec-keyword">
    <h2 class="sec-keyword__heading"><img src="/assets/images/pages/common/keyword_heading.svg" alt="" width="126.5" height="83.5"></h2>
    <ul class="tag-list">
      <?php call_user_func( $keywordList["tagdone"], function($item, $cnt, $page, $helper, $index){ ?>
      <li class="tag-list__item"><a href="/search/?key=<?php echo($item->id); ?>">#<?php echo $item->getName(); ?></a></li>
      <?php }, false); ?>
    </ul>
  </section>
  <?php endif; ?>

  <ul class="banner-list">
    <li class="banner-list__item"><a href="https://www.allhouse.co.jp/" target="_blank"><img src="/assets/images/pages/common/side_banner01.png" alt=""></a></li>
    <li class="banner-list__item"><a href="https://house.allhouse.co.jp/" target="_blank"><img src="/assets/images/pages/common/side_banner02.png" alt=""></a></li>
    <li class="banner-list__item"><a href="https://reform.allhouse.co.jp/" target="_blank"><img src="/assets/images/pages/common/side_banner03.png" alt=""></a></li>
  </ul>
  <div class="banner">
    <ins class="adsbygoogle" style="display:block;" data-ad-client="ca-pub-6194676272576947" data-ad-slot="9629451547" data-ad-format="auto" data-full-width-responsive="true"></ins>
    <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
  </div>
</aside>
